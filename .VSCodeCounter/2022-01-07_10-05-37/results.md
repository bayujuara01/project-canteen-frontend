# Summary

Date : 2022-01-07 10:05:37

Directory c:\Users\NEXSOFT\Documents\Koding\React\project-kantin-frontend

Total : 89 files,  4743 codes, 96 comments, 503 blanks, all 5342 lines

[details](details.md)

## Languages
| language | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| JavaScript React | 44 | 3,512 | 3 | 316 | 3,831 |
| JavaScript | 33 | 933 | 70 | 123 | 1,126 |
| CSS | 7 | 150 | 0 | 28 | 178 |
| JSON | 2 | 81 | 0 | 2 | 83 |
| Markdown | 1 | 37 | 0 | 32 | 69 |
| HTML | 1 | 20 | 23 | 1 | 44 |
| XML | 1 | 10 | 0 | 1 | 11 |

## Directories
| path | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| . | 89 | 4,743 | 96 | 503 | 5,342 |
| public | 2 | 45 | 23 | 2 | 70 |
| src | 85 | 4,605 | 73 | 468 | 5,146 |
| src\app | 4 | 84 | 0 | 13 | 97 |
| src\components | 38 | 2,392 | 2 | 236 | 2,630 |
| src\components\atomic | 18 | 414 | 0 | 76 | 490 |
| src\components\atomic\BarChart | 1 | 58 | 0 | 7 | 65 |
| src\components\atomic\Card | 3 | 45 | 0 | 9 | 54 |
| src\components\atomic\CustomButton | 1 | 8 | 0 | 2 | 10 |
| src\components\atomic\DatePicker | 2 | 118 | 0 | 26 | 144 |
| src\components\atomic\Divider | 2 | 11 | 0 | 4 | 15 |
| src\components\atomic\NavItem | 2 | 41 | 0 | 6 | 47 |
| src\components\atomic\PieChart | 1 | 49 | 0 | 7 | 56 |
| src\components\atomic\TextInfo | 2 | 23 | 0 | 5 | 28 |
| src\components\molecules | 6 | 121 | 0 | 12 | 133 |
| src\components\molecules\InputField | 1 | 35 | 0 | 3 | 38 |
| src\components\molecules\Pagination | 2 | 9 | 0 | 3 | 12 |
| src\components\molecules\PasswordField | 1 | 29 | 0 | 3 | 32 |
| src\components\molecules\StatCard | 1 | 38 | 0 | 2 | 40 |
| src\components\organism | 11 | 1,816 | 0 | 144 | 1,960 |
| src\components\organism\forms | 3 | 405 | 0 | 47 | 452 |
| src\components\organism\survey | 1 | 88 | 0 | 10 | 98 |
| src\components\organism\tables | 7 | 1,323 | 0 | 87 | 1,410 |
| src\components\templates | 3 | 41 | 2 | 4 | 47 |
| src\components\templates\NotFoundLayout | 2 | 37 | 2 | 3 | 42 |
| src\features | 16 | 561 | 11 | 66 | 638 |
| src\features\alergic | 2 | 86 | 0 | 8 | 94 |
| src\features\auth | 2 | 51 | 0 | 6 | 57 |
| src\features\dashboard | 2 | 39 | 0 | 5 | 44 |
| src\features\position | 2 | 93 | 0 | 8 | 101 |
| src\features\report | 4 | 129 | 3 | 17 | 149 |
| src\features\survey | 2 | 59 | 8 | 7 | 74 |
| src\features\user | 2 | 104 | 0 | 15 | 119 |
| src\pages | 16 | 1,306 | 1 | 111 | 1,418 |
| src\utils | 1 | 0 | 0 | 1 | 1 |

[details](details.md)