# Summary

Date : 2022-02-15 11:16:01

Directory c:\Users\NEXSOFT\Documents\Koding\React\project-kantin-frontend

Total : 94 files,  6856 codes, 93 comments, 691 blanks, all 7640 lines

summary / [details](details.md) / [diff summary](diff.md) / [diff details](diff-details.md)

## Languages
| language | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| JavaScript React | 46 | 5,140 | 18 | 422 | 5,580 |
| JavaScript | 37 | 1,421 | 52 | 204 | 1,677 |
| CSS | 6 | 150 | 0 | 27 | 177 |
| JSON | 2 | 81 | 0 | 2 | 83 |
| Markdown | 1 | 37 | 0 | 32 | 69 |
| HTML | 1 | 17 | 23 | 3 | 43 |
| XML | 1 | 10 | 0 | 1 | 11 |

## Directories
| path | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| . | 94 | 6,856 | 93 | 691 | 7,640 |
| public | 2 | 42 | 23 | 4 | 69 |
| src | 90 | 6,721 | 70 | 654 | 7,445 |
| src\app | 4 | 96 | 0 | 15 | 111 |
| src\components | 37 | 3,050 | 2 | 287 | 3,339 |
| src\components\atomic | 18 | 414 | 0 | 76 | 490 |
| src\components\atomic\BarChart | 1 | 58 | 0 | 7 | 65 |
| src\components\atomic\Card | 3 | 45 | 0 | 9 | 54 |
| src\components\atomic\CustomButton | 1 | 8 | 0 | 2 | 10 |
| src\components\atomic\DatePicker | 2 | 118 | 0 | 26 | 144 |
| src\components\atomic\Divider | 2 | 11 | 0 | 4 | 15 |
| src\components\atomic\NavItem | 2 | 41 | 0 | 6 | 47 |
| src\components\atomic\PieChart | 1 | 49 | 0 | 7 | 56 |
| src\components\atomic\TextInfo | 2 | 23 | 0 | 5 | 28 |
| src\components\molecules | 4 | 110 | 0 | 9 | 119 |
| src\components\molecules\InputField | 1 | 35 | 0 | 3 | 38 |
| src\components\molecules\PasswordField | 1 | 29 | 0 | 3 | 32 |
| src\components\molecules\StatCard | 1 | 38 | 0 | 2 | 40 |
| src\components\organism | 12 | 2,485 | 1 | 198 | 2,684 |
| src\components\organism\forms | 3 | 440 | 1 | 51 | 492 |
| src\components\organism\survey | 1 | 105 | 0 | 11 | 116 |
| src\components\organism\tables | 8 | 1,940 | 0 | 136 | 2,076 |
| src\components\templates | 3 | 41 | 1 | 4 | 46 |
| src\components\templates\NotFoundLayout | 2 | 37 | 1 | 3 | 41 |
| src\features | 20 | 766 | 11 | 90 | 867 |
| src\features\alergic | 2 | 95 | 0 | 9 | 104 |
| src\features\auth | 2 | 51 | 0 | 6 | 57 |
| src\features\dashboard | 2 | 39 | 0 | 5 | 44 |
| src\features\position | 2 | 102 | 0 | 9 | 111 |
| src\features\report | 4 | 129 | 3 | 17 | 149 |
| src\features\role | 2 | 36 | 0 | 5 | 41 |
| src\features\survey | 2 | 84 | 8 | 9 | 101 |
| src\features\user | 2 | 155 | 0 | 21 | 176 |
| src\features\voucher | 2 | 75 | 0 | 9 | 84 |
| src\pages | 18 | 2,274 | 16 | 165 | 2,455 |
| src\utils | 1 | 254 | 0 | 52 | 306 |

summary / [details](details.md) / [diff summary](diff.md) / [diff details](diff-details.md)