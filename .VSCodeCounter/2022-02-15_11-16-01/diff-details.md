# Diff Details

Date : 2022-02-15 11:16:01

Directory c:\Users\NEXSOFT\Documents\Koding\React\project-kantin-frontend

Total : 94 files,  6856 codes, 93 comments, 691 blanks, all 7640 lines

[summary](results.md) / [details](details.md) / [diff summary](diff.md) / diff details

## Files
| filename | language | code | comment | blank | total |
| :--- | :--- | ---: | ---: | ---: | ---: |
| [README.md](/README.md) | Markdown | 37 | 0 | 32 | 69 |
| [package.json](/package.json) | JSON | 56 | 0 | 1 | 57 |
| [public/index.html](/public/index.html) | HTML | 17 | 23 | 3 | 43 |
| [public/manifest.json](/public/manifest.json) | JSON | 25 | 0 | 1 | 26 |
| [src/App.js](/src/App.js) | JavaScript | 80 | 0 | 9 | 89 |
| [src/App.test.js](/src/App.test.js) | JavaScript | 9 | 0 | 2 | 11 |
| [src/ColorModeSwitcher.js](/src/ColorModeSwitcher.js) | JavaScript | 21 | 0 | 3 | 24 |
| [src/Logo.js](/src/Logo.js) | JavaScript | 14 | 0 | 5 | 19 |
| [src/app/api.js](/src/app/api.js) | JavaScript | 18 | 0 | 5 | 23 |
| [src/app/constant.js](/src/app/constant.js) | JavaScript | 17 | 0 | 2 | 19 |
| [src/app/store.js](/src/app/store.js) | JavaScript | 46 | 0 | 6 | 52 |
| [src/app/theme.js](/src/app/theme.js) | JavaScript | 15 | 0 | 2 | 17 |
| [src/components/atomic/Anchor.jsx](/src/components/atomic/Anchor.jsx) | JavaScript React | 0 | 0 | 1 | 1 |
| [src/components/atomic/BarChart/BarChart.jsx](/src/components/atomic/BarChart/BarChart.jsx) | JavaScript React | 58 | 0 | 7 | 65 |
| [src/components/atomic/Card/Card.jsx](/src/components/atomic/Card/Card.jsx) | JavaScript React | 22 | 0 | 3 | 25 |
| [src/components/atomic/Card/CardBody.jsx](/src/components/atomic/Card/CardBody.jsx) | JavaScript React | 13 | 0 | 3 | 16 |
| [src/components/atomic/Card/CardHeader.jsx](/src/components/atomic/Card/CardHeader.jsx) | JavaScript React | 10 | 0 | 3 | 13 |
| [src/components/atomic/CustomButton/CustomButton.jsx](/src/components/atomic/CustomButton/CustomButton.jsx) | JavaScript React | 8 | 0 | 2 | 10 |
| [src/components/atomic/DatePicker/DatePicker.jsx](/src/components/atomic/DatePicker/DatePicker.jsx) | JavaScript React | 20 | 0 | 5 | 25 |
| [src/components/atomic/DatePicker/date-picker.css](/src/components/atomic/DatePicker/date-picker.css) | CSS | 98 | 0 | 21 | 119 |
| [src/components/atomic/Divider/Divider.jsx](/src/components/atomic/Divider/Divider.jsx) | JavaScript React | 7 | 0 | 4 | 11 |
| [src/components/atomic/Divider/Divider.module.css](/src/components/atomic/Divider/Divider.module.css) | CSS | 4 | 0 | 0 | 4 |
| [src/components/atomic/NavItem/NavItem.jsx](/src/components/atomic/NavItem/NavItem.jsx) | JavaScript React | 13 | 0 | 2 | 15 |
| [src/components/atomic/NavItem/NavItem.module.css](/src/components/atomic/NavItem/NavItem.module.css) | CSS | 28 | 0 | 4 | 32 |
| [src/components/atomic/PasswordInput.jsx](/src/components/atomic/PasswordInput.jsx) | JavaScript React | 24 | 0 | 3 | 27 |
| [src/components/atomic/PieChart/PieChart.jsx](/src/components/atomic/PieChart/PieChart.jsx) | JavaScript React | 49 | 0 | 7 | 56 |
| [src/components/atomic/TextInfo/TextInfo.jsx](/src/components/atomic/TextInfo/TextInfo.jsx) | JavaScript React | 11 | 0 | 3 | 14 |
| [src/components/atomic/TextInfo/TextInfo.module.css](/src/components/atomic/TextInfo/TextInfo.module.css) | CSS | 12 | 0 | 2 | 14 |
| [src/components/atomic/TextInput.jsx](/src/components/atomic/TextInput.jsx) | JavaScript React | 15 | 0 | 5 | 20 |
| [src/components/atomic/index.js](/src/components/atomic/index.js) | JavaScript | 22 | 0 | 1 | 23 |
| [src/components/molecules/InputField/InputField.jsx](/src/components/molecules/InputField/InputField.jsx) | JavaScript React | 35 | 0 | 3 | 38 |
| [src/components/molecules/PasswordField/PasswordField.jsx](/src/components/molecules/PasswordField/PasswordField.jsx) | JavaScript React | 29 | 0 | 3 | 32 |
| [src/components/molecules/StatCard/StatCard.jsx](/src/components/molecules/StatCard/StatCard.jsx) | JavaScript React | 38 | 0 | 2 | 40 |
| [src/components/molecules/index.js](/src/components/molecules/index.js) | JavaScript | 8 | 0 | 1 | 9 |
| [src/components/organism/forms/ChangePasswordForm.jsx](/src/components/organism/forms/ChangePasswordForm.jsx) | JavaScript React | 137 | 0 | 18 | 155 |
| [src/components/organism/forms/SignInForm.jsx](/src/components/organism/forms/SignInForm.jsx) | JavaScript React | 139 | 0 | 16 | 155 |
| [src/components/organism/forms/VerifyPasswordForm.jsx](/src/components/organism/forms/VerifyPasswordForm.jsx) | JavaScript React | 164 | 1 | 17 | 182 |
| [src/components/organism/survey/QuestionCard.jsx](/src/components/organism/survey/QuestionCard.jsx) | JavaScript React | 105 | 0 | 11 | 116 |
| [src/components/organism/tables/AlergicTable.jsx](/src/components/organism/tables/AlergicTable.jsx) | JavaScript React | 353 | 0 | 23 | 376 |
| [src/components/organism/tables/BaseTable.jsx](/src/components/organism/tables/BaseTable.jsx) | JavaScript React | 274 | 0 | 19 | 293 |
| [src/components/organism/tables/DashboardTakeFoodTable.jsx](/src/components/organism/tables/DashboardTakeFoodTable.jsx) | JavaScript React | 70 | 0 | 5 | 75 |
| [src/components/organism/tables/EmployeeTable.jsx](/src/components/organism/tables/EmployeeTable.jsx) | JavaScript React | 249 | 0 | 11 | 260 |
| [src/components/organism/tables/EmployeeTableRow.jsx](/src/components/organism/tables/EmployeeTableRow.jsx) | JavaScript React | 40 | 0 | 3 | 43 |
| [src/components/organism/tables/PositionTable.jsx](/src/components/organism/tables/PositionTable.jsx) | JavaScript React | 358 | 0 | 26 | 384 |
| [src/components/organism/tables/TakeFoodTable.jsx](/src/components/organism/tables/TakeFoodTable.jsx) | JavaScript React | 96 | 0 | 5 | 101 |
| [src/components/organism/tables/VoucherTable.jsx](/src/components/organism/tables/VoucherTable.jsx) | JavaScript React | 500 | 0 | 44 | 544 |
| [src/components/templates/NotFoundLayout/NotFoundLayout.jsx](/src/components/templates/NotFoundLayout/NotFoundLayout.jsx) | JavaScript React | 32 | 1 | 3 | 36 |
| [src/components/templates/NotFoundLayout/NotFoundLayout.module.css](/src/components/templates/NotFoundLayout/NotFoundLayout.module.css) | CSS | 5 | 0 | 0 | 5 |
| [src/components/templates/index.js](/src/components/templates/index.js) | JavaScript | 4 | 0 | 1 | 5 |
| [src/features/alergic/alergicAPI.js](/src/features/alergic/alergicAPI.js) | JavaScript | 27 | 0 | 5 | 32 |
| [src/features/alergic/alergicSlice.js](/src/features/alergic/alergicSlice.js) | JavaScript | 68 | 0 | 4 | 72 |
| [src/features/auth/authAPI.js](/src/features/auth/authAPI.js) | JavaScript | 12 | 0 | 1 | 13 |
| [src/features/auth/authSlice.js](/src/features/auth/authSlice.js) | JavaScript | 39 | 0 | 5 | 44 |
| [src/features/dashboard/dashboardAPI.js](/src/features/dashboard/dashboardAPI.js) | JavaScript | 4 | 0 | 1 | 5 |
| [src/features/dashboard/dashboardSlice.js](/src/features/dashboard/dashboardSlice.js) | JavaScript | 35 | 0 | 4 | 39 |
| [src/features/position/positionAPI.js](/src/features/position/positionAPI.js) | JavaScript | 32 | 0 | 5 | 37 |
| [src/features/position/positionSlice.js](/src/features/position/positionSlice.js) | JavaScript | 70 | 0 | 4 | 74 |
| [src/features/report/reportAPI.js](/src/features/report/reportAPI.js) | JavaScript | 18 | 3 | 5 | 26 |
| [src/features/report/reportComplaintSlice.js](/src/features/report/reportComplaintSlice.js) | JavaScript | 39 | 0 | 4 | 43 |
| [src/features/report/reportFeedbackSlice.js](/src/features/report/reportFeedbackSlice.js) | JavaScript | 39 | 0 | 4 | 43 |
| [src/features/report/reportTakeFoodSlice.js](/src/features/report/reportTakeFoodSlice.js) | JavaScript | 33 | 0 | 4 | 37 |
| [src/features/role/roleAPI.js](/src/features/role/roleAPI.js) | JavaScript | 4 | 0 | 1 | 5 |
| [src/features/role/roleSlice.js](/src/features/role/roleSlice.js) | JavaScript | 32 | 0 | 4 | 36 |
| [src/features/survey/surveyAPI.js](/src/features/survey/surveyAPI.js) | JavaScript | 32 | 8 | 5 | 45 |
| [src/features/survey/surveySlice.js](/src/features/survey/surveySlice.js) | JavaScript | 52 | 0 | 4 | 56 |
| [src/features/user/userAPI.js](/src/features/user/userAPI.js) | JavaScript | 85 | 0 | 17 | 102 |
| [src/features/user/userSlice.js](/src/features/user/userSlice.js) | JavaScript | 70 | 0 | 4 | 74 |
| [src/features/voucher/voucherAPI.js](/src/features/voucher/voucherAPI.js) | JavaScript | 13 | 0 | 4 | 17 |
| [src/features/voucher/voucherSlice.js](/src/features/voucher/voucherSlice.js) | JavaScript | 62 | 0 | 5 | 67 |
| [src/index.js](/src/index.js) | JavaScript | 27 | 6 | 5 | 38 |
| [src/logo.svg](/src/logo.svg) | XML | 10 | 0 | 1 | 11 |
| [src/pages/AlergicPage.jsx](/src/pages/AlergicPage.jsx) | JavaScript React | 18 | 0 | 2 | 20 |
| [src/pages/CreateEmployeePage.jsx](/src/pages/CreateEmployeePage.jsx) | JavaScript React | 425 | 7 | 23 | 455 |
| [src/pages/DashboardPage.jsx](/src/pages/DashboardPage.jsx) | JavaScript React | 113 | 0 | 7 | 120 |
| [src/pages/DetailEmployeePage.jsx](/src/pages/DetailEmployeePage.jsx) | JavaScript React | 91 | 0 | 6 | 97 |
| [src/pages/EditEmployeePage.jsx](/src/pages/EditEmployeePage.jsx) | JavaScript React | 430 | 7 | 22 | 459 |
| [src/pages/EmployeePage.jsx](/src/pages/EmployeePage.jsx) | JavaScript React | 17 | 0 | 3 | 20 |
| [src/pages/HomePage.jsx](/src/pages/HomePage.jsx) | JavaScript React | 170 | 0 | 12 | 182 |
| [src/pages/LoginPages.jsx](/src/pages/LoginPages.jsx) | JavaScript React | 33 | 0 | 4 | 37 |
| [src/pages/NotFoundPage.jsx](/src/pages/NotFoundPage.jsx) | JavaScript React | 12 | 0 | 2 | 14 |
| [src/pages/PositionPage.jsx](/src/pages/PositionPage.jsx) | JavaScript React | 18 | 0 | 2 | 20 |
| [src/pages/ReportComplaintPage.jsx](/src/pages/ReportComplaintPage.jsx) | JavaScript React | 167 | 0 | 16 | 183 |
| [src/pages/ReportFeedbackPage.jsx](/src/pages/ReportFeedbackPage.jsx) | JavaScript React | 181 | 0 | 17 | 198 |
| [src/pages/ReportTakeFoodPage.jsx](/src/pages/ReportTakeFoodPage.jsx) | JavaScript React | 17 | 0 | 3 | 20 |
| [src/pages/ResetPasswordPage.jsx](/src/pages/ResetPasswordPage.jsx) | JavaScript React | 37 | 0 | 5 | 42 |
| [src/pages/SurveyPage.jsx](/src/pages/SurveyPage.jsx) | JavaScript React | 516 | 2 | 36 | 554 |
| [src/pages/TestPage.jsx](/src/pages/TestPage.jsx) | JavaScript React | 8 | 0 | 3 | 11 |
| [src/pages/VoucherPage.jsx](/src/pages/VoucherPage.jsx) | JavaScript React | 18 | 0 | 2 | 20 |
| [src/pages/test.css](/src/pages/test.css) | CSS | 3 | 0 | 0 | 3 |
| [src/reportWebVitals.js](/src/reportWebVitals.js) | JavaScript | 12 | 0 | 2 | 14 |
| [src/serviceWorker.js](/src/serviceWorker.js) | JavaScript | 98 | 31 | 13 | 142 |
| [src/setupTests.js](/src/setupTests.js) | JavaScript | 1 | 4 | 1 | 6 |
| [src/test-utils.js](/src/test-utils.js) | JavaScript | 9 | 0 | 4 | 13 |
| [src/utils/useForm.js](/src/utils/useForm.js) | JavaScript | 254 | 0 | 52 | 306 |

[summary](results.md) / [details](details.md) / [diff summary](diff.md) / diff details