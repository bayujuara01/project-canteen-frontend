exports.config = {
  tests: './tests/e2e/*_test.js',
  output: './tests/output',
  helpers: {
    Playwright: {
      url: 'http://localhost:8080',
      show: true,
      browser: 'chromium',
      timeout: 10000,
      chromium: {
        args: [
          '--disable-web-security'
        ]
      }
    }
  },
  include: {
    I: './steps_file.js'
  },
  plugins: {
    autoDelay: {
      enabled: false
    }
  },
  bootstrap: null,
  mocha: {},
  name: 'project-kantin-frontend'
}