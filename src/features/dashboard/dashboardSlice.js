import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    isError: false,
    isLoading: false,
    data: {
        countEmployee: 0,
        countCaretaker: 0,
        countTakenFood: 0
    },
}

const dashboardSlice = createSlice({
    name: 'dashboard',
    initialState,
    reducers: {
        requestDashboardAction: (state) => {
            state.isError = false;
            state.isLoading = true;
        },
        requestErrorDashboardAction: (state) => {
            state.isError = true;
            state.isLoading = false;
        },
        getStatDashboardAction: (state, { payload }) => {
            state.isError = false;
            state.isLoading = false;
            state.data = payload
        }
    }
});

export const {
    requestDashboardAction,
    requestErrorDashboardAction,
    getStatDashboardAction
} = dashboardSlice.actions;

export default dashboardSlice.reducer;