import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    isLoading: false,
    isError: false,
    error: '',
    data: [],
    dataText: [],
}

const reportFeedbackSlice = createSlice({
    name: 'reportFeedback',
    initialState,
    reducers: {
        requestReportFeedbackAction: (state) => {
            state.isLoading = true;
            state.isError = false
        },
        getAllReportFeedbackAction: (state, { payload }) => {
            state.isLoading = false;
            state.isError = false;
            state.data = payload;
        },
        getAllReportTextFeedbackActon: (state, { payload }) => {
            state.isLoading = false;
            state.isError = false;
            state.dataText = payload;
        },
        requestErrorAction: (state) => {
            state.isLoading = false;
            state.isError = true;
        }
    }
});

export const {
    requestReportFeedbackAction,
    getAllReportFeedbackAction,
    getAllReportTextFeedbackActon,
    requestErrorAction
} = reportFeedbackSlice.actions;

export default reportFeedbackSlice.reducer;