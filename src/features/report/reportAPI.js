import api from '../../app/api';

export const Report = {
    TYPE_FEEDBACK: 0,
    TYPE_COMPLAINT: 1
};

export const requestAllFeedbackReport = ({ type = Report.TYPE_FEEDBACK, date = null } = {}) => {
    return api.get(`/feedbacks/report?type=${type}${date ? `&date=${date}` : ''}`)
}

export const requestAllFeedbackReportText = ({ type = Report.TYPE_FEEDBACK, date = null } = {}) => {
    return api.get(`/feedbacks/report/text?type=${type}${date ? `&date=${date}` : ''}`)
}

/**
 * @param  {String} date = yyyy-MM-dd template date
 */
export const requestAllTakefoodReport = (date = null) => {
    return api.get(`/takefoods${date ? `?date=${date}` : ''}`);
}

export const requestAllTakefoodReportPaging = ({ page = 0, pageSize = 10, name = '', id = null, date = null } = {}) => {
    return api.get(`/takefoods/report?page=${page}&pageSize=${pageSize}${id ? `&id=${id}` : ''}${date ? `&date=${date}` : ''}${name ? `&name=${name}` : ''}`);
}

/**
 * @param {String} startDate = yyyy-MM-dd template date
 * @param {String} endDate = yyyy-MM-dd template date
 */
export const requestAllTakefoodReportForExport = ({ startDate, endDate }) => {
    return api.get(`/takefoods/report/export?start-date=${startDate}${endDate ? `&${endDate}` : ''}`)
}