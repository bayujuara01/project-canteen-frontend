import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    isLoading: false,
    isError: false,
    error: '',
    data: [],
    dataText: []
}

const reportComplaintSlice = createSlice({
    name: 'reportComplaint',
    initialState,
    reducers: {
        requestReportComplaintAction: (state) => {
            state.isLoading = true;
            state.isError = false
        },
        getAllReportComplaintAction: (state, { payload }) => {
            state.isLoading = false;
            state.isError = false;
            state.data = payload;
        },
        getAllReportTextComplaintAction: (state, { payload }) => {
            state.isLoading = false;
            state.isError = false;
            state.dataText = payload;
        },
        requestErrorAction: (state) => {
            state.isLoading = false;
            state.isError = true;
        }
    }
});

export const {
    requestReportComplaintAction,
    getAllReportComplaintAction,
    getAllReportTextComplaintAction,
    requestErrorAction
} = reportComplaintSlice.actions;

export default reportComplaintSlice.reducer;