import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    isLoading: false,
    isError: false,
    page: {},
    data: []
};

const reportTakeFoodSlice = createSlice({
    name: 'reportTakeFood',
    initialState,
    reducers: {
        requestReportTakefoodAction: (state) => {
            state.isLoading = true;
            state.isError = false;
        },
        requestErrorAction: (state) => {
            state.isLoading = false;
            state.isError = true
        },
        getAllReportTakefoodAction: (state, { payload }) => {
            state.isLoading = false;
            state.isError = false;
            state.data = payload.content;
            state.page = payload.page;
        }
    }
});

export const {
    requestErrorAction,
    requestReportTakefoodAction,
    getAllReportTakefoodAction
} = reportTakeFoodSlice.actions;

export default reportTakeFoodSlice.reducer;