import api from "../../app/api";

export const requestAllRole = () => {
    return api.get(`/roles`);
};