import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    isLoading: false,
    isError: false,
    error: '',
    roles: []
}

const roleSlice = createSlice({
    name: 'role',
    initialState,
    reducers: {
        requestRoleAction: (state) => {
            state.isLoading = true;
            state.isError = false
        },
        getAllRoleAction: (state, { payload }) => {
            state.isLoading = false;
            state.isError = false;
            state.roles = payload;
        },
        requestErrorAction: (state) => {
            state.isLoading = false;
            state.isError = true;
        }
    }
});

export const {
    requestRoleAction,
    requestErrorAction,
    getAllRoleAction,
} = roleSlice.actions;

export default roleSlice.reducer;