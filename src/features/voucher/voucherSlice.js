import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    isLoading: false,
    isError: false,
    error: '',
    vouchers: [],
    page: {},
    selected: {
        id: null
    }
}

const voucherSlice = createSlice({
    name: 'voucher',
    initialState,
    reducers: {
        requestVoucherAction: (state) => {
            state.isLoading = true;
            state.isError = false;
        },
        getAllVoucherAction: (state, { payload }) => {
            state.isLoading = false;
            state.isError = false;
            state.vouchers = payload.content;
            state.page = payload.page;
        },
        addVoucherAction: (state, { payload }) => {
            state.isLoading = false;
            state.isError = false;
            state.vouchers = [payload, ...state.vouchers]
        },
        updateSelectedVoucherAction: (state, { payload }) => {
            const updateIndex = state.vouchers.findIndex(voucher => voucher.id === payload.id);
            const tempVouchers = [...state.vouchers];
            tempVouchers[updateIndex] = { ...tempVouchers[updateIndex], ...payload };
            state.vouchers = tempVouchers;
            state.isLoading = false;
            state.isError = false;
        },
        deleteSelectedVoucherAction: (state, { payload }) => {
            const deleteIndex = state.vouchers.findIndex(voucher => voucher.id === payload);
            const tempVouchers = [...state.vouchers];
            tempVouchers.splice(deleteIndex, 1);
            state.vouchers = [...tempVouchers];
            state.isLoading = false;
            state.isError = false;

        },
        requestErrorVoucherAction: (state, { payload }) => {
            state.isLoading = false;
            state.isError = true;
            state.error = payload;
        }
    }
});

export const {
    requestVoucherAction,
    getAllVoucherAction,
    addVoucherAction,
    updateSelectedVoucherAction,
    deleteSelectedVoucherAction,
    requestErrorVoucherAction } = voucherSlice.actions;

export default voucherSlice.reducer;