import api from "../../app/api";

export const requestAllVoucher = ({ page = 0, pageSize = 10, name = '', date = null } = {}) => {
    return api.get(`/takevouchers?page=${page}&pageSize=${pageSize}&name=${name}${date ? `&date=${date}` : ''}`);
}

export const createNewVoucher = ({ idAdmin, idUser, amount, issueAt, expireAt }) => {
    return api.post('/takevouchers', { idAdmin, idUser, amount, issueAt, expireAt });
}

export const updateVoucher = (idVoucher, { idAdmin, idUser, amount, issueAt, expireAt }) => {
    return api.put(`/takevouchers/${idVoucher}`, { idAdmin, idUser, amount, issueAt, expireAt });
}

export const deleteVoucher = (idVoucher) => {
    return api.delete(`/takevouchers/${idVoucher}`);
}