import api from '../../app/api';

export const requestAllAlergic = ({ page = 0, pageSize = 10, name = '' } = {}) => {
    return api.get(`/alergics?page=${page}&pageSize=${pageSize}&name=${name}`);
}

export const requestAllAlergicWithoutPaging = () => {
    return api.get('/alergics/all');
}

export const updateAlergic = ({ idAdmin, idAlergic, nameAlergic }) => {
    return api.put(`/alergics/${idAlergic}`, {
        idAdmin,
        name: nameAlergic
    });
};

export const deleteAlergicById = ({ idAdmin, idAlergic }) => {
    return api.delete(`/alergics/${idAlergic}`, {
        data: {
            idAdmin
        }
    });
};

export const createNewAlergic = ({ idAdmin, nameAlergic }) => {
    return api.post('alergics', {
        idAdmin,
        name: nameAlergic
    });
};