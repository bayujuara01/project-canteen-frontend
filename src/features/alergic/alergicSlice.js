import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    isLoading: false,
    isError: false,
    error: '',
    alergics: [],
    page: {},
    selected: {
        id: null
    }
}

const alergicslice = createSlice({
    name: 'alergic',
    initialState,
    reducers: {
        requestAlergicAction: (state) => {
            state.isLoading = true;
            state.isError = false
        },
        getAllAlergicAction: (state, { payload }) => {
            state.isLoading = false;
            state.isError = false;
            state.alergics = payload.content;
            state.page = payload.page
        },
        getAllAlergicWithoutPagingAction: (state, { payload }) => {
            state.isLoading = false;
            state.isError = false;
            state.alergics = payload;
        },
        requestErrorAction: (state) => {
            state.isLoading = false;
            state.isError = true;
        },
        updateSelectedAlergicAction: (state, { payload }) => {
            const updateIndex = state.alergics.findIndex(alergic => alergic.id === payload.id);
            state.alergics[updateIndex] = {
                ...state.alergics[updateIndex],
                ...payload
            };
            state.isLoading = false;
            state.isError = false;
        },
        deleteSelectedAlergicAction: (state, { payload }) => {
            const deleteIndex = state.alergics.findIndex(alergic => alergic.id === payload)
            state.alergics.splice(deleteIndex, 1);
            state.isLoading = false;
            state.isError = false;
        },
        addAlergicAction: (state, { payload }) => {
            state.alergics.unshift(payload);
            state.isLoading = false;
            state.isError = false;
        }
    }
});

export const {
    requestAlergicAction,
    requestErrorAction,
    getAllAlergicAction,
    getAllAlergicWithoutPagingAction,
    updateSelectedAlergicAction,
    deleteSelectedAlergicAction,
    addAlergicAction
} = alergicslice.actions;

export default alergicslice.reducer;