import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    isLoading: false,
    isError: false,
    error: '',
    qr: '',
    page: {}
}

const qrSlice = createSlice({
    name: 'qr',
    initialState,
    reducers: {
        requestQrAction: (state) => {
            state.isLoading = true;
            state.isError = false;
        },
        getQrAction: (state, { payload }) => {
            state.isLoading = false;
            state.isError = false;
            state.qr = payload;
        },
        requestErrorAction: (state, { payload }) => {
            state.isLoading = false;
            state.isError = true;
            state.error = payload
        },
        createQrAction: (state, { payload }) => {
            state.isLoading = false;
            state.isError = false;
            state.qr = payload;
        }
    }
});

export const {
    createQrAction,
    getQrAction,
    requestErrorAction,
    requestQrAction
} = qrSlice.actions;

export default qrSlice.reducer;