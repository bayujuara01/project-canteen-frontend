import api from "../../app/api";

export const requestStaticQr = ({ idUser }) => {
    return api.get(`qr/static?idUser=${idUser}`);
};

export const createStaticQr = ({ idUser, password }) => {
    return api.post('qr/static/generate', {
        idEmployee: idUser,
        password
    });
};