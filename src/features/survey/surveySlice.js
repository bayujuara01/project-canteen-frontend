import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    isLoading: false,
    isError: false,
    questions: [],
    inputType: []
};

const surveySlice = createSlice({
    name: 'slice',
    initialState,
    reducers: {
        requestQuestionAction: (state) => {
            state.isError = false;
            state.isLoading = true;
        },
        requestQuestionErrorAction: (state) => {
            state.isError = false;
            state.isLoading = false;
        },
        getAllQuestionAction: (state, { payload }) => {
            state.questions = payload;
            state.isError = false;
            state.isLoading = false;
        },
        getAllInputTypeAction: (state, { payload }) => {
            state.inputType = payload
            state.isError = false;
            state.isLoading = false;
        },
        deleteQuestionByIdAction: (state, { payload }) => {
            const deleteIndex = state.questions.findIndex(question => question.id === payload);
            state.questions = [
                ...state.questions.slice(0, deleteIndex),
                ...state.questions.slice(deleteIndex + 1)
            ];
            state.isError = false;
            state.isLoading = false;
        },
        addQuestionAction: (state, { payload }) => {
            state.questions.push(payload)
        }
    }
});

export const {
    requestQuestionAction,
    requestQuestionErrorAction,
    getAllInputTypeAction,
    getAllQuestionAction,
    addQuestionAction,
    deleteQuestionByIdAction
} = surveySlice.actions;

export default surveySlice.reducer;