import api from '../../app/api';

export const getAllQuestion = ({ status = '' } = {}) => {
    let publish = null;

    switch (status) {
        case '1':
            publish = true;
            break;
        case '2':
            publish = false;
            break;
        default:
            publish = null
    }

    return api.get(`/questions${publish != null ? `?publish=${publish}` : ''}`);
}

export const getAllQuestionInputType = () => {
    return api.get('/questions/inputs');
}

/*
 "idAdmin": 1,
    "type": 1,
    "inputTypeId" : 2,
    "title": "Title, just identifier 5",
    "subtext": "Description, this is mostly used",
    "options": ["Hello","World","Test"]
*/
export const createNewQuestion = ({ idAdmin, type, inputTypeId, title, subtext, options, isPublish }) => {
    return api.post('/questions', {
        idAdmin,
        type,
        inputTypeId,
        title,
        subtext,
        options,
        publish: isPublish
    });
}

export const updateQuestionById = (idQuestion, { idAdmin, type, inputTypeId, title, subtext, options, isPublish }) => {

    return api.put(`/questions/${idQuestion}`, {
        idAdmin,
        type,
        inputTypeId,
        title,
        subtext,
        options,
        publish: isPublish
    });
}

export const deleteQuestionById = ({ idQuestion, idAdmin }) => {
    return api.delete('/questions', {
        data: { idQuestion, idAdmin }
    });
}