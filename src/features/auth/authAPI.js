import axios from "axios";
import { SERVER_URL } from "../../app/constant";

export const requestSignIn = ({ username, password }) => {
    return axios.post(`${SERVER_URL}/auth/login`, {
        username,
        password
    }, {
        headers: {
            'Content-Type': 'application/json'
        }
    })
}