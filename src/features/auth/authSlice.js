import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    isLoading: false,
    isError: false,
    isAuth: false,
    isExpired: true,
    errorMessage: '',
    token: '',
    user: {}
}

const authSlice = createSlice({
    name: 'auth',
    initialState,
    reducers: {
        requestSignInAction: (state) => {
            state.isLoading = true;
            state.isError = false;
        },
        signIn: (state, { payload }) => {
            state.isAuth = true;
            state.isLoading = true;
            state.isError = false;
            state.isExpired = false;
            state.token = payload.token
            state.user = payload.user
        },
        signOut: (state, { payload }) => {
            state.isLoading = false;
            state.isError = false;
            state.isAuth = false;
            state.token = '';
            state.user = {};
            state.isExpired = payload ? true : false;
        },
        requestErrorAction: (state) => {
            state.isLoading = false;
            state.isError = true;
            state.isExpired = false;
        },
        setIsExpiredAction: (state) => {
            state.isExpired = true;
        },
        setFlashErrorAction: (state, { payload }) => {
            state.isError = true;
            state.isLoading = false;
            state.errorMessage = payload;
        },
        clearFlashMessage: (state) => {
            state.isError = false;
            state.isLoading = false;
            state.errorMessage = "";
        }
    }
})

export const {
    signIn,
    signOut,
    requestSignInAction,
    requestErrorAction,
    setIsExpiredAction,
    setFlashErrorAction,
    clearFlashMessage
} = authSlice.actions;

export default authSlice.reducer;
