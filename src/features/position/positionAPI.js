import api from "../../app/api";

export const requestAllPosition = ({ page = 0, pageSize = 10, name = '' } = {}) => {
    return api.get(`/positions?page=${page}&pageSize=${pageSize}&name=${name}`);
};

export const requestAllPositionWithoutPaging = () => {
    return api.get('/positions/all')
}

export const updatePosition = ({ idAdmin, idPosition, namePosition }) => {
    return api.put('/positions', {
        idAdmin,
        position: {
            id: idPosition,
            name: namePosition
        }
    });
};

export const deletePositionById = ({ idAdmin, idPosition }) => {
    return api.delete(`/positions/${idPosition}`, {
        data: {
            idAdmin
        }
    });
};

export const createNewPosition = ({ idAdmin, namePosition }) => {
    return api.post('positions', {
        idAdmin,
        position: {
            name: namePosition
        }
    });
};