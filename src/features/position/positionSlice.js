import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    isLoading: false,
    isError: false,
    error: '',
    positions: [],
    page: {},
    selected: {
        id: null
    }
}

const positionSlice = createSlice({
    name: 'posititon',
    initialState,
    reducers: {
        requestPositionAction: (state) => {
            state.isLoading = true;
            state.isError = false
        },
        getAllPositionAction: (state, { payload }) => {
            state.isLoading = false;
            state.isError = false;
            state.positions = payload.content;
            state.page = payload.page
        },
        getAllPositionWithoutPagingAction: (state, { payload }) => {
            state.isLoading = false;
            state.isError = false;
            state.positions = payload;
        },
        requestErrorAction: (state) => {
            state.isLoading = false;
            state.isError = true;
        },
        setSelectedIdAction: (state, { payload }) => {
            state.selected.id = payload;
        },
        updateSelectedPositionAction: (state, { payload }) => {
            const updateIndex = state.positions.findIndex(position => position.id === payload.id);
            state.positions[updateIndex] = {
                ...state.positions[updateIndex],
                ...payload
            };
            state.isLoading = false;
            state.isError = false;
        },
        deleteSelectedPositionAction: (state, { payload }) => {
            const deleteIndex = state.positions.findIndex(position => position.id === payload.id)
            state.positions.splice(deleteIndex, 1);
            state.isLoading = false;
            state.isError = false;
        },
        addPositionAction: (state, { payload }) => {
            state.positions.unshift(payload);
            state.isLoading = false;
            state.isError = false;
        }
    }
});

export const {
    requestPositionAction,
    requestErrorAction,
    getAllPositionAction,
    getAllPositionWithoutPagingAction,
    setSelectedIdAction,
    updateSelectedPositionAction,
    deleteSelectedPositionAction,
    addPositionAction
} = positionSlice.actions;

export default positionSlice.reducer;