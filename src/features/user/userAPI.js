import api from '../../app/api';

export const requestAllUser = ({ page = 0, pageSize = 10, name = '' } = {}) => {
    return api.get(`/users?page=${page}&pageSize=${pageSize}&name=${name}`);
}

export const requestAllUserWithoutPagination = () => api.get('/users/all');

export const requestUserById = (id) => {
    return api.get(`/users/${id}`);
}

export const requestCheckUsersUniqueField = ({ nik = '', username = '', email = '', phone = '', userId = null } = {}) => {
    return api.get(`/users${userId ? `/${userId}/` : '/'}check?username=${username}&email=${email}&phone=${phone}&nik=${nik}`);
}

export const createNewUser = ({
    name,
    nik,
    username,
    email,
    phone,
    gender,
    position,
    alergicIds,
    image,
    idAdmin,
    role
}) => {
    const data = new FormData();
    data.append('name', name);
    data.append('nik', nik);
    data.append('username', username);
    data.append('email', email);
    data.append('phone', phone);
    data.append('gender', parseInt(gender));
    data.append('idPosition', position);
    data.append('idAdmin', idAdmin);
    data.append('idRole', role);

    if (image != null) {
        data.append('image', image)
    }


    for (const id of alergicIds) {
        data.append('alergicIds', id)
    }

    return api.post('/users', data, { headers: { 'Content-Type': 'multipart/form-data' } });
}

export const updateUserById = (idUser, {
    name,
    nik,
    username,
    email,
    phone,
    gender,
    position,
    role,
    alergicIds,
    image,
    idAdmin
}) => {
    const data = new FormData();
    data.append('name', name);
    data.append('nik', nik);
    data.append('username', username);
    data.append('email', email);
    data.append('phone', phone);
    data.append('gender', parseInt(gender))
    data.append('idRole', role);
    data.append('idPosition', position);
    data.append('idAdmin', idAdmin)
    if (image) {
        data.append('image', image)
    }

    // for (const id of alergicIds) {
    //     if (id) data.append('alergicIds', id);
    // }

    return api.put(`/users/${idUser}`, data, { headers: { 'Content-Type': 'multipart/form-data' } });
};

export const deleteUserById = ({ idEmployee, idAdmin }) => {
    return api.delete('/users', {
        data: {
            idUser: idEmployee,
            idAdmin: idAdmin
        }
    });
}

export const requestChangeUserPassword = (email) => {
    return api.post('users/resetpwd/request', { email })
}

export const checkVerifyCode = (verifyCode) => {
    return api.get(`users/resetpwd/check?verify=${verifyCode}`);
}

export const verifyChangeUserPassword = ({ verifyCode, newPassword }) => {
    return api.post(`users/resetpwd?verify=${verifyCode}`, { newPassword });
}