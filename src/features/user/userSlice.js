import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    isLoading: false,
    isError: false,
    error: '',
    search: {
        name: '',
        id: null
    },
    users: [],
    all: [],
    selected: {
        id: null,
        alergics: []
    },
    page: {}
}

const userSlice = createSlice({
    name: 'user',
    initialState,
    reducers: {
        requestUserAction: (state) => {
            state.isLoading = true;
            state.isError = false
        },
        getAllUserAction: (state, { payload }) => {
            state.isLoading = false;
            state.isError = false;
            state.users = payload.content;
            state.page = payload.page
        },
        getAllUserWithoutPagingAction: (state, { payload }) => {
            state.isLoading = false;
            state.isError = false;
            state.all = payload;
        },
        requestErrorAction: (state) => {
            state.isLoading = false;
            state.isError = true;
        },
        getByIdUserAction: (state, { payload }) => {
            state.isLoading = false;
            state.isError = false;
            state.selected = { ...state.selected, ...payload.user, alergics: payload.alergics };
        },
        deleteByUserIdAction: (state, { payload }) => {
            const deleteIndex = state.users.findIndex(user => user.id === payload);
            state.users = [
                ...state.users.slice(0, deleteIndex),
                ...state.users.slice(deleteIndex + 1)
            ];
            state.isLoading = false;
            state.isError = false;
        },
        setSelectedIdAction: (state, { payload }) => {
            state.selected.id = payload;
        }
    }
});

export const {
    requestUserAction,
    getAllUserAction,
    getAllUserWithoutPagingAction,
    requestErrorAction,
    getByIdUserAction,
    setSelectedIdAction,
    deleteByUserIdAction
} = userSlice.actions;

export default userSlice.reducer;