import { Box, Flex, Heading, Text, Button } from '@chakra-ui/react';
import style from './NotFoundLayout.module.css';

const NotFoundLayout = ({ buttonHandler }) => {
    return (
        <Flex justifyContent="center" alignItems="center" w="100%" h="100%">
            <Box textAlign="center" className={style.container} >
                <Heading
                    display="inline-block"
                    as="h2"
                    size="2xl"
                    bg="blue.500"
                    // bgGradient="linear(to-r, teal.400, teal.600)"
                    backgroundClip="text">
                    404
                </Heading>
                <Text fontSize="18px" mt={3} mb={2}>
                    Page Not Found
                </Text>
                <Text color={'gray.500'} mb={6}>
                    The page you're looking for does not seem to exist
                </Text>

                <Button
                    colorScheme="blue"
                    color="white"
                    variant="solid"
                    onClick={buttonHandler}>
                    Go to Home
                </Button>
            </Box>
        </Flex>
    );
};

export default NotFoundLayout;