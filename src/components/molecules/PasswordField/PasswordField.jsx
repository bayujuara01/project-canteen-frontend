import React from "react"
// eslint-disable-next-line no-unused-vars
import { FormControl, FormLabel, Input, FormErrorMessage } from "@chakra-ui/react"
import PasswordInput from '../../atomic/PasswordInput';


const PasswordField = ({
    id,
    label,
    name,
    onChange,
    value,
    placeholder,
    disabled,
    required,
    readOnly,
    error,
    invalid
}) => {
    return (
        <FormControl id={id}
            isDisabled={disabled}
            isRequired={required}
            isReadOnly={readOnly}
            isInvalid={invalid}>
            <FormLabel>{label}</FormLabel>
            <PasswordInput value={value} onChange={onChange} name={name} placeholder={placeholder} />
            <FormErrorMessage>{error ? error : "Password doesn't match right formation"}</FormErrorMessage>
        </FormControl>
    )
}

export default PasswordField;