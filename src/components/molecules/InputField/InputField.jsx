import React from "react"
import {
    FormControl,
    FormLabel,
    Input,
    FormErrorMessage
} from "@chakra-ui/react"


const InputField = ({
    id,
    label,
    type,
    onChange,
    value,
    name,
    placeholder,
    disabled,
    required,
    readOnly,
    error,
    invalid,
    ...rest
}) => {
    return (
        <FormControl id={id}
            isDisabled={disabled}
            isRequired={required}
            isReadOnly={readOnly}
            isInvalid={invalid}>
            <FormLabel>{label}</FormLabel>
            <Input {...rest} type={type} value={value} onChange={onChange} name={name} placeholder={placeholder} />
            <FormErrorMessage fontSize="12px">{error}</FormErrorMessage>
        </FormControl>
    )
}

export default InputField;