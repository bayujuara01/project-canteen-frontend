import React from "react";
import {
    Stat,
    StatLabel,
    Flex,
    StatNumber,
    StatHelpText
} from '@chakra-ui/react'

const StatCard = ({ title, label }) => {
    return (
        <Stat me="auto">
            <StatLabel
                fontSize="sm"
                color="gray.400"
                fontWeight="bold"
                pb=".1rem"
            >
                {title}
            </StatLabel>
            <Flex>
                <StatNumber fontSize="lg">
                    {label}
                </StatNumber>
                <StatHelpText
                    alignSelf="flex-end"
                    justifySelf="flex-end"
                    m="0px"
                    color="green.400"
                    fontWeight="bold"
                    ps="3px"
                    fontSize="md"
                >
                </StatHelpText>
            </Flex>
        </Stat>
    );
}

export default StatCard;