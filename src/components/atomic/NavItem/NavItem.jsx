import React from "react";
import { Link } from "react-router-dom";
import { Icon } from '@chakra-ui/react';
import styles from './NavItem.module.css';

const NavItem = ({ children, to, icon, active }) => {
    return (
        <Link to={to} className={`${styles.container} ${active ? styles.active : ''}`}>
            {icon && <Icon className={styles.icon} as={icon} />}
            <p className={styles.text}>{children}</p>
        </Link>
    );
}

export default NavItem;