import { Input, InputGroup, InputRightElement, IconButton } from "@chakra-ui/react"
import { FiEye, FiEyeOff } from 'react-icons/fi'
import React, { useState } from "react"

const PasswordInput = ({ onChange, value, name, placeholder }) => {
    const [show, setShow] = useState(false);

    return <>
        <InputGroup>
            <Input
                onChange={onChange}
                value={value}
                type={show ? 'text' : 'password'}
                name={name}
                placeholder={placeholder} />
            <InputRightElement>
                <IconButton
                    aria-label="show password"
                    icon={show ? <FiEyeOff /> : <FiEye />}
                    onClick={() => setShow(!show)}
                />
            </InputRightElement>
        </InputGroup>
    </>
}

export default PasswordInput;