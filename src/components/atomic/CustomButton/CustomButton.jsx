import React from "react";
import { Button } from '@chakra-ui/react'

const CustomButton = ({ type, onClick, children, ...rest }) => {
    return <>
        <Button onClick={onClick} {...rest}>{children}</Button>
    </>;
}

export default CustomButton;