import React from "react"
import { InputGroup, Input, InputRightElement } from "@chakra-ui/react"


const TextInput = ({ type, onChange, value, name, placeholder }) => {
    return <>
        <InputGroup>
            <Input
                onChange={onChange}
                value={value}
                type={type ? type : 'text'}
                name={name}
                placeholder={placeholder} />

        </InputGroup>
    </>

}

export default TextInput;