import { Stack, Text } from "@chakra-ui/react";
import style from './TextInfo.module.css';


const TextInfo = ({ label, children, element, ...rest }) => {
    return (
        <Stack className={style.container}>
            <Text className={style.label}>{label}</Text>
            {element ? children : <Text className={style.body}>{children}</Text>}
        </Stack>
    );
}

export default TextInfo;