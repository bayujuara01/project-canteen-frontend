import { Box, useStyleConfig } from "@chakra-ui/react";

const CardHeader = ({ variant, children, ...rest }) => {
    const styles = useStyleConfig("CardHeader", { variant });

    return (
        <Box __css={styles} {...rest}>
            {children}
        </Box>
    );
}

export default CardHeader;