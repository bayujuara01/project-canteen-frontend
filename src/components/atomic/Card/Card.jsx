import { Box, useStyleConfig } from "@chakra-ui/react";

const Card = ({ variant, children, ...rest }) => {
    const styles = {
        p: "22px",
        display: "flex",
        flexDirection: "column",
        width: "100%",
        position: "relative",
        minWidth: "0px",
        wordWrap: "break-word",
        backgroundClip: "border-box",
        boxShadow: "0px 5px 5px rgba(0, 0, 0, 0.15)",
        borderRadius: "5px",
    };

    return (
        <Box __css={styles} {...rest} >
            {children}
        </Box>
    );
}

export default Card;