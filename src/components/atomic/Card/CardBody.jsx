import { Box } from "@chakra-ui/react";

const CardBody = ({ variant, children, ...rest }) => {
    const styles = {
        display: "flex",
        width: "100%",
    };

    return (
        <Box __css={styles} {...rest}>
            {children}
        </Box>
    );
}

export default CardBody;