import React from "react";
import { Divider as ChakraDivider } from "@chakra-ui/react";
import style from './Divider.module.css'

const Divider = (props) => {
    return <ChakraDivider className={style.divider} {...props} />
}

export default Divider;

