import React, { useMemo } from "react";
import { Flex } from '@chakra-ui/react';
import {
    Chart as ChartJS,
    ArcElement,
    Legend,
    Tooltip,
    CategoryScale,
    LinearScale,
    BarElement,
} from "chart.js";
import { Bar } from 'react-chartjs-2';

ChartJS.register(ArcElement, Legend, Tooltip, CategoryScale, LinearScale, BarElement);

const BarChart = ({ id, labels, data, ...rest }) => {

    const backgroundColor = useMemo(() => ([
        'rgb(0, 145, 234)',
        'rgb(0, 191, 165)',
        'rgb(0, 200, 83)',
        'rgb(100, 221, 23)',
        'rgb(174, 234, 0)',
        'rgb(255, 214, 0)',
        'rgb(255, 171, 0)',
        'rgb(255, 109, 0)',
        'rgb(221, 44, 0)'
    ]), []);

    const options = useMemo(() => ({
        indexAxis: 'y',
        plugins: {
            legend: {
                display: false,

            }
        },
        scale: {
            ticks: {
                precision: 0
            }
        },
        onClick: (e, v) => { },
        responsive: true,
        maintainAspectRatio: true
    }), []);

    return (
        <Flex w={{ base: '70vw', md: '50vw', lg: '30vw' }}  {...rest}>
            <Bar
                id={id}
                options={options}
                data={{
                    labels,
                    datasets: [{
                        backgroundColor,
                        data,
                    }]
                }}
            />
        </Flex>
    );
}

export default BarChart;