import React, { useMemo } from "react";
import { Flex } from '@chakra-ui/react'
import { Chart as ChartJS, ArcElement, Tooltip, Legend } from 'chart.js';
import { Pie } from "react-chartjs-2";

ChartJS.register(ArcElement, Tooltip, Legend);

const PieChart = ({ id, labels, data, ...rest }) => {
    const backgroundColor = useMemo(() => ([
        'rgb(0, 145, 234)',
        'rgb(0, 191, 165)',
        'rgb(0, 200, 83)',
        'rgb(100, 221, 23)',
        'rgb(174, 234, 0)',
        'rgb(255, 214, 0)',
        'rgb(255, 171, 0)',
        'rgb(255, 109, 0)',
        'rgb(221, 44, 0)'
    ]), []);

    const options = useMemo(() => ({
        plugins: {
            legend: {
                display: true,
                position: 'bottom',
                labels: {
                    usePointStyle: true
                }
            }
        },
        responsive: true,
        maintainAspectRatio: true,
        onClick: (e, v) => { }
    }), []);

    return (
        <Flex w={{ base: '50vw', md: '20vw', lg: '15vw' }}  {...rest}>
            <Pie
                id={id}
                options={options}
                data={{
                    labels,
                    datasets: [{
                        backgroundColor,
                        data
                    }]
                }}

            />
        </Flex>
    );
}

export default PieChart;