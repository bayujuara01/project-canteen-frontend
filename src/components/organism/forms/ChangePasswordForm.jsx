import {
    Box,
    Stack,
    Link,
    Button,
    Alert,
    AlertIcon,
    useColorModeValue,
    useToast
} from '@chakra-ui/react';

import ReCAPTCHA from 'react-google-recaptcha';
import { PasswordField, InputField } from '../../molecules';
import { useState } from 'react';
import { useDispatch } from 'react-redux';
import { requestChangeUserPassword } from '../../../features/user/userAPI';
import jwtDecode from 'jwt-decode';
import { useSelector } from 'react-redux';
import { useNavigate } from 'react-router';


const ChangePasswordForm = () => {
    const navigate = useNavigate();
    const [user, setUser] = useState({
        email: '',
        errors: {},
        captchaValid: false
    });
    const [alert, setAlert] = useState({
        message: '',
        status: 'success',
        show: false
    });

    const [isLoading, setIsLoading] = useState(false);

    const onChangeHandler = (events) => {
        const errors = {};
        const email = events.target.value;

        if (!email.match(/^[^@\s]+@[^@\s]+\.[^@\s]+$/)) {
            errors['email'] = 'Please provide valid email';
        } else {
            delete errors.email;
        }

        setUser({
            ...user,
            [events.target.name]: email,
            errors
        });

    }

    const onHomeClickHandler = () => {
        if (typeof (window.Android) !== 'undefined' && window.Android != null) {
            window.Android.backHome('Harusnya Ke Home');
        } else {
            navigate('/');
        }
    }

    const onSubmitHandler = async (events) => {
        setIsLoading(true);
        try {
            await requestChangeUserPassword(user.email);
            setAlert({
                ...alert,
                show: true,
                status: 'success',
                message: 'Success, check your email!'
            });
            setIsLoading(false);
        } catch (err) {
            setAlert({
                ...alert,
                show: true,
                status: 'error',
                message: 'Failed, email isn\'t registered!'
            });
            setIsLoading(false);
        }
    }

    const captchaChangeHandler = (events) => {
        if (events) {
            setUser({
                ...user,
                captchaValid: true
            });
        }
    }

    const captchaExpiredHandler = (events) => {
        setUser({
            ...user,
            captchaValid: false
        });
    }

    return <>
        <Box
            rounded={'lg'}
            bg={useColorModeValue('white', 'gray.700')}
            boxShadow={'lg'}
            p={8}>
            <Stack spacing={4}>
                <Alert status={alert.status} display={alert.show ? 'inline-flex' : 'none'}>
                    <AlertIcon />
                    {alert.message}
                </Alert>
                <InputField
                    name="email"
                    label="Email"
                    placeholder="Input email"
                    value={user.email}
                    onChange={onChangeHandler}
                    invalid={user.errors.email}
                    error={user.errors.email}
                />

                <ReCAPTCHA
                    sitekey="6LfhUJgdAAAAADX5DTSMXyfGdlzEIrnVSZxQ4p11"
                    onChange={captchaChangeHandler}
                    onExpired={captchaExpiredHandler} />

                <Stack spacing={4}>

                    <Button
                        isLoading={isLoading}
                        loadingText="Submitting..."
                        bg={'blue.400'}
                        color={'white'}
                        _hover={{
                            bg: 'blue.500',
                        }}
                        onClick={onSubmitHandler}
                        isDisabled={!user.captchaValid || Object.keys(user.errors).length > 0}
                    >
                        Submit Request
                    </Button>
                    <Button onClick={onHomeClickHandler}>
                        Back
                    </Button>
                </Stack>
            </Stack>
        </Box>
    </>
}

export default ChangePasswordForm;