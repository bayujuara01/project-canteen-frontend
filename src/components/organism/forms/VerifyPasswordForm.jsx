import React, { useCallback, useEffect, useState } from "react";
import {
    Box,
    Stack,
    Link as Anchor,
    Button,
    Alert,
    AlertIcon,
    useColorModeValue,
} from '@chakra-ui/react';
import { useSearchParams, useNavigate } from "react-router-dom";
import { PasswordField } from "../../molecules";
import { verifyChangeUserPassword, checkVerifyCode } from "../../../features/user/userAPI";
import { NotFoundLayout } from '../../templates/'

const VerifyPasswordForm = ({ verified, setVerified }) => {
    const navigate = useNavigate();
    const [params] = useSearchParams();
    const verifyCode = params.get('verify');
    const [user, setUser] = useState({
        password: '',
        passwordConfirm: '',
        errors: {}
    });

    const [alert, setAlert] = useState({
        message: 'Reset password success',
        status: 'success',
        show: false
    });

    const [valid, setValid] = useState(false);
    const [isLoading, setIsLoading] = useState(false);
    const bgColor = useColorModeValue('white', 'gray.700');

    const onChangeHandler = (events) => {
        const errors = {};

        if (events.target.name === 'password' && !user.password.match(/^(?=.*\d)(?=.*[A-Z])(?=.*[a-z])([^\s]){8,}$/g)) {
            errors['password'] = 'at least 8 characters, have one or more uppercase, lowercase and number'
        } else {
            delete errors.password;
        }

        if (events.target.value !== user.password) {
            errors['passwordConfirm'] = 'Password didn\'t same';
        } else {
            delete errors.passwordConfirm;
        }

        setUser({
            ...user,
            [events.target.name]: events.target.value,
            errors
        });
    }

    const onHomeClickHandler = () => {
        if (typeof (window.Android) !== 'undefined' && window.Android != null) {
            window.Android.backHome('Harusnya Ke Home');
        } else {
            navigate('/');
        }
    }

    const onSubmitHandler = async () => {
        setAlert({ ...Alert, show: false });
        setIsLoading(true);
        try {
            await verifyChangeUserPassword({ verifyCode, newPassword: user.password });
            setAlert({
                ...alert,
                message: 'Reset password success',
                status: 'success',
                show: true
            });
        } catch (err) {
            if (err.response) {
                setAlert({
                    ...alert,
                    message: 'Failed, please contact a admin',
                    status: 'error',
                    show: true
                });
            } else {
                setAlert({
                    ...alert,
                    message: 'Internal server error, try again later',
                    status: 'error',
                    show: true
                });
            }
        } finally {
            setIsLoading(false);
        }
    }

    const fetchCheckVerifyCode = useCallback(async () => {
        try {
            await checkVerifyCode(verifyCode);
            setVerified(true);
        } catch (err) {
            setVerified(false);
        }
    }, [setVerified, verifyCode])

    useEffect(() => {
        fetchCheckVerifyCode()
        if (Object.keys(user.errors).length === 0) {
            setValid(true);
        } else {
            setValid(false);
        }
    }, [user.errors, fetchCheckVerifyCode]);

    return (<>
        {verified ?
            <Box
                rounded={'lg'}
                bg={bgColor}
                boxShadow={'lg'}
                p={8}>
                <Stack spacing={4} w="300px">
                    <Alert status={alert.status} display={alert.show ? 'inline-flex' : 'none'}>
                        <AlertIcon />
                        {alert.message}{alert.status === 'success' && <Anchor textDecoration="underline" fontWeight="bold" onClick={onHomeClickHandler}>, login</Anchor>}
                    </Alert>
                    <PasswordField
                        name="password"
                        label="New Password"
                        placeholder="Input new password"
                        value={user.password}
                        onChange={onChangeHandler}
                        invalid={user.errors.password}
                        error={user.errors.password}
                        required
                    />

                    <PasswordField
                        name="passwordConfirm"
                        label="Confirmation"
                        placeholder="Input password again"
                        value={user.passwordConfirm}
                        onChange={onChangeHandler}
                        invalid={user.errors.passwordConfirm}
                        error={user.errors.passwordConfirm}
                        required
                    />

                    <Stack spacing={4}>

                        <Button
                            isLoading={isLoading}
                            loadingText="Submitting..."
                            bg={'blue.400'}
                            color={'white'}
                            _hover={{
                                bg: 'blue.500',
                            }}
                            onClick={onSubmitHandler}
                            isDisabled={!valid}
                        >
                            Submit
                        </Button>

                        <Button
                            colorScheme="teal"
                            // bgGradient="linear(to-r, teal.400, teal.500, teal.600)"
                            color="white"
                            variant="solid"
                            onClick={onHomeClickHandler}>
                            Go to Home
                        </Button>
                    </Stack>
                </Stack>
            </Box>
            : <NotFoundLayout buttonHandler={onHomeClickHandler} />}</>);
}

export default VerifyPasswordForm;