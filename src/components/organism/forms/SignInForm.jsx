import {
    Box,
    Stack,
    Link,
    Button,
    useColorModeValue,
    useToast,
} from '@chakra-ui/react';
import React, { useEffect } from 'react';
import ReCAPTCHA from 'react-google-recaptcha';
import { CATPCHA_SITE_KEY } from '../../../app/constant';
import { PasswordField, InputField } from '../../molecules';
import { useState } from 'react';
import { useDispatch } from 'react-redux';
import { requestSignIn } from '../../../features/auth/authAPI';
import { signIn, requestSignInAction, requestErrorAction } from '../../../features/auth/authSlice';
import jwtDecode from 'jwt-decode';
import { useSelector } from 'react-redux';
import { useNavigate } from 'react-router';


const SignInForm = () => {
    const navigate = useNavigate();
    const auth = useSelector(state => state.auth);

    const toast = useToast();
    const [user, setUser] = useState({
        username: '',
        password: '',
        captchaValid: false
    })
    const dispatch = useDispatch();

    const onChangeHandler = (events) => {
        setUser({
            ...user,
            [events.target.name]: events.target.value
        });
    }

    const onSubmitHandler = (events) => {
        dispatch(requestSignInAction());
        requestSignIn({
            username: user.username,
            password: user.password
        }).then(response => {
            const token = response.data.data;
            const decodedToken = jwtDecode(token);
            const { roles } = decodedToken;

            if (roles[0].toLowerCase() === 'admin') {
                dispatch(signIn({ token: token, user: decodedToken }))
                toast({
                    title: 'Login successfully',
                    position: 'top',
                    variant: 'solid',
                    isClosable: true,
                    duration: 2500,
                    status: 'success',
                });
            } else {
                toast({
                    title: 'Username or password are wrong',
                    position: 'top',
                    variant: 'solid',
                    isClosable: true,
                    duration: 1500,
                    status: 'error'
                })
                dispatch(requestErrorAction());
            }
        }).catch(err => {
            dispatch(requestErrorAction());
            const status = err.response ? err.response.status : 500;
            toast({
                title: status < 500 ? 'Username or password are wrong' : 'Internal Server Error',
                position: 'top',
                variant: 'solid',
                isClosable: true,
                duration: 1500,
                status: 'error'
            });
        });
    }

    const captchaChangeHandler = (events) => {
        if (events) {
            setUser({
                ...user,
                captchaValid: true
            });
        }
    }

    const captchaExpiredHandler = (events) => {
        setUser({
            ...user,
            captchaValid: false
        });
    }

    useEffect(() => {
        if (process.env.NODE_ENV === 'development') {
            setUser(user => ({ ...user, captchaValid: true }));
        }
    }, []);

    return <>
        <Box
            rounded={'lg'}
            bg={useColorModeValue('white', 'gray.700')}
            boxShadow={'lg'}
            p={8}>
            <Stack spacing={4}>
                <InputField
                    name="username"
                    label="Username"
                    placeholder="Input username"
                    value={user.username}
                    onChange={onChangeHandler}
                    invalid={false}
                />

                <PasswordField
                    name="password"
                    label="Password"
                    placeholder="Input password"
                    value={user.password}
                    onChange={onChangeHandler}
                />

                {process.env.NODE_ENV !== 'development' && <ReCAPTCHA
                    sitekey={CATPCHA_SITE_KEY}
                    onChange={captchaChangeHandler}
                    onExpired={captchaExpiredHandler} />}

                <Stack spacing={10}>

                    <Link onClick={() => navigate('/resetpwd')} color={'blue.400'}>Forgot password?</Link>

                    <Button
                        isLoading={auth.isLoading}
                        loadingText="Signing..."
                        bg={'blue.400'}
                        color={'white'}
                        _hover={{
                            bg: 'blue.500',
                        }}
                        onClick={onSubmitHandler}
                        isDisabled={!user.captchaValid}
                    >
                        Sign in
                    </Button>
                </Stack>
            </Stack>
        </Box>
    </>
}

export default SignInForm;