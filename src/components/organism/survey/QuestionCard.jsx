import React from "react";
import {
    Tag,
    Flex,
    Button,
    Text,
    Checkbox,
    Stack,
    Radio,
    RadioGroup
} from '@chakra-ui/react';
import { Card, CardBody, CardHeader } from '../../atomic';
import Select from 'react-select';

const QuestionCard = ({
    id,
    title,
    description,
    type,
    isAnswered,
    isPublish,
    inputType,
    optionChoice,
    onEditClick,
    onDeleteClick }) => {

    return (
        <Card bg='white' w="100%">
            <CardHeader>
                <Flex justify="space-between" align="center" minHeight="60px" w="100%">
                    <Stack>
                        <Text fontSize="lg" fontWeight="bold">
                            {title}
                        </Text>
                        <Text fontSize="sm">{description}</Text>
                    </Stack>
                    <Stack>
                        {((typeof isPublish === 'number' || typeof isPublish === 'boolean') && isPublish) ?
                            <Tag
                                colorScheme='green'
                                variant="solid"
                                borderRadius="full">Published</Tag> :
                            <Tag
                                colorScheme='orange'
                                variant="solid"
                                borderRadius="full"><Text mx="auto">Draft</Text></Tag>
                        }


                        <Tag colorScheme={type.toLowerCase() === 'complaint' ? 'red' : 'green'} variant="outline" borderRadius="full">
                            {type}
                        </Tag>

                        <Tag
                            colorScheme='blue'
                            variant="outline"
                            borderRadius="full">
                            {inputType.toLowerCase() !== 'radio-button' ?
                                <Text mx="auto">{inputType[0].toUpperCase() + inputType.substring(1).toLowerCase()}</Text> :
                                <Text mx="auto">Radio</Text>
                            }

                        </Tag>
                    </Stack>
                </Flex>
            </CardHeader>
            <CardBody>
                <Flex
                    direction={{ sm: "column", md: "row" }}
                    align="center"
                    w="100%"

                >
                    {(typeof inputType === 'string' && inputType === 'checkbox') &&
                        <Stack>
                            {(optionChoice && optionChoice.length > 0) && optionChoice.map(option =>
                                <Checkbox isReadOnly key={option.id} value={option.id}>{option.name[0].toUpperCase()}{option.name.substring(1).toLowerCase()}</Checkbox>)}
                        </Stack>
                    }

                    {(typeof inputType === 'string' && inputType.includes('radio')) &&
                        <RadioGroup>
                            <Stack>
                                {(optionChoice && optionChoice.length > 0) && optionChoice.map(option =>
                                    <Radio isReadOnly key={option.id} value={option.id}>{option.name[0].toUpperCase()}{option.name.substring(1).toLowerCase()}</Radio>)}
                            </Stack>
                        </RadioGroup>
                    }

                    {(typeof inputType === 'string' && inputType === 'dropdown') &&
                        <Select options={optionChoice.map(option => ({
                            value: option.id.toString(),
                            label: option.name[0].toUpperCase() + option.name.substring(1).toLowerCase()
                        }))} />
                    }

                    {(typeof inputType === 'string' && inputType.includes('chip')) &&
                        <Select isMulti options={optionChoice.map(option => ({
                            value: option.id.toString(),
                            label: option.name[0].toUpperCase() + option.name.substring(1).toLowerCase()
                        }))} />
                    }

                </Flex>
            </CardBody>
            <Flex justifyContent="flex-end" mt={4}>
                {!isAnswered &&
                    <Button
                        me={4}
                        variant="solid"
                        colorScheme="blue"
                        onClick={() => onEditClick(id)}>Edit</Button>
                }

                <Button
                    variant="solid"
                    colorScheme="red"
                    onClick={() => onDeleteClick(id, title)}>Delete</Button>
            </Flex>
        </Card>
    );
}

export default QuestionCard;