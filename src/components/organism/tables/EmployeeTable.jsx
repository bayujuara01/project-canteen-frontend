import React, { useCallback, useMemo, useState } from "react";
import {
    Flex,
    Text,
    Menu,
    MenuItem,
    MenuList,
    MenuButton,
    IconButton,
    Stack,
    Button,
    Avatar,
    Modal,
    ModalOverlay,
    ModalContent,
    ModalHeader,
    ModalFooter,
    ModalBody,
    ModalCloseButton,
    useToast
} from '@chakra-ui/react';
import { useDisclosure } from "@chakra-ui/hooks";
import {
    HamburgerIcon,
    EditIcon,
    DeleteIcon,
    ExternalLinkIcon
} from '@chakra-ui/icons'
import {
    requestUserAction,
    requestErrorAction,
    getAllUserAction,
    deleteByUserIdAction,
    setSelectedIdAction
} from "../../../features/user/userSlice";
import { requestAllUser, deleteUserById } from "../../../features/user/userAPI";
import { useDispatch, useSelector } from "react-redux";
import profileImage from '../../../assets/images/profile.png';
import BaseTable from "./BaseTable";
import { useNavigate } from "react-router";
import { IMAGE_PATH } from "../../../app/constant";

const EmployeeTable = () => {
    const { user, auth } = useSelector(state => state);
    const dispatch = useDispatch();
    const navigate = useNavigate();
    const toast = useToast();
    const [deleteUser, setDeleteUser] = useState({
        isLoading: false,
        id: null,
        name: '',
        email: '',
        username: ''
    });

    const fetchData = useCallback(({ pageSize, pageIndex, search }) => {
        async function getUser() {
            dispatch(requestUserAction())
            try {
                const response = await requestAllUser({ pageSize: pageSize, page: pageIndex, name: search });
                dispatch(getAllUserAction(response.data.data));
            } catch (err) {
                dispatch(requestErrorAction());
                toast({
                    title: 'Internal Server Error',
                    position: 'top',
                    variant: 'solid',
                    isClosable: true,
                    duration: 1500,
                    status: 'error'
                });

            }
        }

        getUser();
    }, [dispatch, toast]);

    const { isOpen, onOpen, onClose } = useDisclosure();

    const columns = useMemo(
        () => [
            {
                Header: 'Name',
                accessor: 'name',
                Cell: ({ row, value }) => {
                    return (
                        <Flex align="center" py=".25rem" minWidth="100%" flexWrap="nowrap">
                            {/* <Avatar src={
                                row.original.imageUrl ? `${IMAGE_PATH}/${row.original.imageUrl}` : profileImage} w="50px" borderRadius="50px" me="18px" /> */}
                            <Avatar src={profileImage} w="50px" borderRadius="50px" me="18px" />
                            <Flex direction="column" textAlign="left">
                                <Text
                                    fontSize="md"
                                    fontWeight="bold"
                                    minWidth="100%"
                                >
                                    {value}
                                </Text>
                                <Text fontSize="sm" color="gray.400" fontWeight="normal">
                                    {row.original.email}
                                </Text>
                            </Flex>
                        </Flex>
                    );
                }
            },
            {
                Header: 'NIK',
                accessor: 'nik'
            },
            {
                Header: 'Username',
                accessor: 'username'
            },
            {
                Header: 'Position',
                accessor: 'position'
            },
            {
                Header: 'Phone',
                accessor: 'phoneNumber'
            },
            {
                Header: 'Action',
                accessor: 'id',
                Cell: ({ row, value }) => (
                    <Menu>
                        <MenuButton
                            as={IconButton}
                            aria-label='Options'
                            icon={<HamburgerIcon />}
                            variant='outline'
                        />
                        <MenuList>
                            <MenuItem icon={<ExternalLinkIcon />}
                                onClick={() => {
                                    dispatch(setSelectedIdAction(value));
                                    navigate("/employee/detail")
                                }}>
                                Detail
                            </MenuItem>
                            <MenuItem icon={<EditIcon />}
                                onClick={() => {
                                    dispatch(setSelectedIdAction(value));
                                    navigate("/employee/edit")
                                }}>
                                Edit
                            </MenuItem>
                            {value != auth.user.sub && <MenuItem icon={<DeleteIcon />} onClick={() => {
                                setDeleteUser(prevState => ({ ...row.original }))
                                onOpen();
                            }}>
                                Delete
                            </MenuItem>}

                        </MenuList>
                    </Menu>
                )
            }
        ], [auth.user, navigate, dispatch, onOpen]
    );

    const onSubmitDeleteHandler = async () => {
        setDeleteUser({ ...deleteUser, isLoading: true });
        try {
            const response = await deleteUserById({
                idEmployee: deleteUser.id,
                idAdmin: auth.user.sub
            });
            dispatch(deleteByUserIdAction(response.data.data.id));
            toast({
                title: response.data.data.name + ' successfully deleted',
                position: 'top',
                'status': 'success'
            });
        } catch (err) {
            toast({
                title: 'Error deleting position, try again',
                position: 'top',
                status: 'error'
            });
        } finally {
            onClose();
            setDeleteUser({ ...deleteUser, isLoading: false });
        }
    }

    return <>
        <BaseTable
            columns={columns}
            data={user.users}
            fetchData={fetchData}
            pageCount={user.page.totalPages}
            loading={user.isLoading}
            buttonText="Add"
            buttonHandler={() => navigate("/employee/add")}
        />
        <Modal isOpen={isOpen} onClose={onClose}>
            <ModalOverlay />
            <ModalContent>
                <ModalHeader>Delete Employee</ModalHeader>
                <ModalCloseButton />
                <ModalBody>
                    <Stack>
                        <Stack spacing={0}>
                            <Text fontSize="sm" color="gray.400" fontWeight="normal">
                                Name
                            </Text>
                            <Text
                                fontSize="md"
                                fontWeight="bold">
                                {deleteUser.name}
                            </Text>
                        </Stack>
                        <Stack spacing={0}>
                            <Text fontSize="sm" color="gray.400" fontWeight="normal">
                                Username
                            </Text>
                            <Text
                                fontSize="md"
                                fontWeight="bold">
                                {deleteUser.username}
                            </Text>
                        </Stack>
                        <Stack spacing={0}>
                            <Text fontSize="sm" color="gray.400" fontWeight="normal">
                                Email
                            </Text>
                            <Text
                                fontSize="md"
                                fontWeight="bold">
                                {deleteUser.email}
                            </Text>
                        </Stack>
                    </Stack>
                </ModalBody>

                <ModalFooter>
                    <Button mr={3} onClick={onClose}>
                        Cancel
                    </Button>
                    <Button
                        variant='solid'
                        colorScheme="red"
                        onClick={onSubmitDeleteHandler}
                        isLoading={deleteUser.isLoading}>Delete</Button>
                </ModalFooter>
            </ModalContent>
        </Modal>
    </>
}

export default EmployeeTable;