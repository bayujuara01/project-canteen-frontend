import {
    Box,
    Table,
    Thead,
    Tbody,
    Tr,
    Th,
    Td,
    Flex,
    Text,
    Spinner,
    IconButton,
    Input,
    InputGroup,
    InputLeftElement,
    Button,
    Tooltip,
    Select,
    NumberInput,
    NumberInputField,
    NumberInputStepper,
    NumberIncrementStepper,
    NumberDecrementStepper,
    InputLeftAddon
} from '@chakra-ui/react';
import {
    ArrowRightIcon,
    ArrowLeftIcon,
    ChevronRightIcon,
    ChevronLeftIcon,
    SearchIcon
} from "@chakra-ui/icons";
import { usePagination, useTable } from 'react-table';
import { useEffect, useState } from 'react';

const BaseTable = ({
    columns,
    data,
    fetchData,
    loading,
    pageCount: controlledPageCount,
    searchText,
    buttonText,
    buttonHandler,
    withDate,
    rowHeight,
    h
}) => {

    const {
        getTableProps,
        getTableBodyProps,
        headerGroups,
        prepareRow,
        page,
        canPreviousPage,
        canNextPage,
        pageOptions,
        pageCount,
        gotoPage,
        nextPage,
        previousPage,
        setPageSize,
        state: { pageIndex, pageSize }
    } = useTable({
        columns,
        data,
        initialState: { pageIndex: 0, pageSize: 10 },
        manualPagination: true,
        pageCount: controlledPageCount
    }, usePagination);

    const todayString = new Date().toISOString().slice(0, 10);
    const [date, setDate] = useState(todayString);
    const [search, setSearch] = useState('');
    const [input, setInput] = useState('');

    useEffect(() => {
        fetchData({ pageIndex, pageSize, search, date })
    }, [fetchData, pageIndex, pageSize, search, date]);

    let typingTimer;
    const doneTypingInterval = 1500;

    const onChangeDateHandler = (events) => {
        setDate(events.target.value);
    };

    const onSearchKeyUpHandler = () => {
        clearTimeout(typingTimer);
        typingTimer = setTimeout(onDoneTypingHandler, doneTypingInterval);
    };

    const onSearchKeyDownHandler = () => {
        clearTimeout(typingTimer)
    };

    const onDoneTypingHandler = () => {
        gotoPage(0);
        setSearch(input);
    };

    return <>
        <Flex justifyContent="space-between" px="2px" mb="14px">
            <InputGroup maxW="60vh">

                <InputLeftElement
                    pointerEvents='none'
                    children={<SearchIcon color='gray.300' />}
                />
                <Input
                    type='search'
                    placeholder={`${searchText ? searchText : 'Search'}`}
                    onKeyUp={onSearchKeyUpHandler}
                    onKeyDown={onSearchKeyDownHandler}
                    onChange={(event) => setInput(event.target.value)}
                    value={input} />
                <Button
                    minW="20vh"
                    ml="8px"
                    variant="solid"
                    colorScheme="blue"
                    spinnerPlacement="start"
                    isLoading={loading}
                    isDisabled={loading}
                    onClick={onDoneTypingHandler}
                    loadingText="Search"> Search</Button>
            </InputGroup>
            <Flex>
                {withDate && <InputGroup>
                    <InputLeftAddon children="Date" />
                    <Input
                        type="date"
                        value={date}
                        max={todayString}
                        onChange={onChangeDateHandler} />
                </InputGroup>}
                {(buttonText && buttonHandler) &&
                    <Button
                        ml="2"
                        variant="solid"
                        colorScheme="blue"
                        onClick={buttonHandler} >{buttonText ? buttonText : 'Button'}
                    </Button>}

            </Flex>
        </Flex>
        <Box w="100%" h={h ? h : "72vh"} overflow="auto">
            <Table variant='simple' size="sm" {...getTableProps()}>

                <Thead>
                    {headerGroups.map(headerGroup => (
                        <Tr {...headerGroup.getHeaderGroupProps()}>
                            {headerGroup.headers.map(column => (
                                <Th {...column.getHeaderProps()} textAlign="center">
                                    <Text >{column.render('Header')}</Text>
                                </Th>
                            ))}
                        </Tr>
                    ))}
                </Thead>

                <Tbody {...getTableBodyProps()} >
                    {page.length <= 0 && <Tr>
                        <Td textAlign="center" colSpan="99"><Text fontWeight="bold">Data are empty</Text></Td>
                    </Tr>}
                    {page.length > 0 && page.map(row => {
                        prepareRow(row);
                        return (
                            <Tr h={14} {...row.getRowProps()}>
                                {!loading && row.cells.map(cell => {
                                    return (
                                        <Td fontSize="md" fontWeight="bold" pb=".5rem" {...cell.getCellProps()} textAlign="center">
                                            {cell.render('Cell')}
                                        </Td>
                                    )
                                })}
                            </Tr>
                        );
                    })}
                    <Tr>
                        {loading && <>
                            <Td colSpan="100" textAlign="center">
                                <Spinner
                                    thickness='4px'
                                    speed='0.65s'
                                    emptyColor='gray.200'
                                    color='blue.500'
                                    size='xl'
                                />
                            </Td>
                        </>}
                    </Tr>

                </Tbody>
            </Table>
        </Box>
        <Flex justifyContent="space-between" mx={4} alignItems="center" display={data.length > 0 ? 'flex' : 'none'}>
            <Flex>
                <Tooltip label="First Page">
                    <IconButton
                        onClick={() => gotoPage(0)}
                        isDisabled={!canPreviousPage}
                        icon={<ArrowLeftIcon h={3} w={3} />}
                        mr={4}
                    />
                </Tooltip>
                <Tooltip label="Previous Page">
                    <IconButton
                        onClick={previousPage}
                        isDisabled={!canPreviousPage}
                        icon={<ChevronLeftIcon h={6} w={6} />}
                    />
                </Tooltip>
            </Flex>

            <Flex alignItems="center">
                <Text flexShrink="0" mr={8}>
                    Page{" "}
                    <Text fontWeight="bold" as="span">
                        {pageIndex + 1}
                    </Text>{" "}
                    of{" "}
                    <Text fontWeight="bold" as="span">
                        {pageOptions.length}
                    </Text>
                </Text>
                <Text flexShrink="0">Go to page:</Text>{" "}

                <NumberInput
                    ml={2}
                    mr={8}
                    w={28}
                    min={1}
                    max={pageOptions.length}
                    value={pageIndex + 1}
                    onChange={(value) => {
                        const page = value ? value - 1 : 0;
                        gotoPage(page);
                    }}
                    defaultValue={pageIndex + 1}
                >
                    <NumberInputField />
                    <NumberInputStepper>
                        <NumberIncrementStepper />
                        <NumberDecrementStepper />
                    </NumberInputStepper>
                </NumberInput>
                <Select
                    w={32}
                    value={pageSize}
                    onChange={(e) => {
                        setPageSize(Number(e.target.value));
                    }}
                >
                    {[5, 10, 20, 30, 40, 50].map((pageSize) => (
                        <option key={pageSize} value={pageSize}>
                            Show {pageSize}
                        </option>
                    ))}
                </Select>
            </Flex>

            <Flex>
                <Tooltip label="Next Page">
                    <IconButton
                        onClick={nextPage}
                        isDisabled={!canNextPage}
                        icon={<ChevronRightIcon h={6} w={6} />}
                    />
                </Tooltip>
                <Tooltip label="Last Page">
                    <IconButton
                        onClick={() => gotoPage(pageCount - 1)}
                        isDisabled={!canNextPage}
                        icon={<ArrowRightIcon h={3} w={3} />}
                        ml={4}
                    />
                </Tooltip>
            </Flex>
        </Flex>
    </>;
}

export default BaseTable;