import React, { useCallback, useMemo, useState } from 'react';
import {
    useToast,
    Text,
    Menu,
    MenuItem,
    MenuList,
    MenuButton,
    IconButton,
    FormControl,
    FormLabel,
    Input,
    Modal,
    ModalOverlay,
    ModalContent,
    ModalHeader,
    ModalFooter,
    ModalBody,
    ModalCloseButton,
} from '@chakra-ui/react';
import {
    HamburgerIcon,
    EditIcon,
    DeleteIcon
} from '@chakra-ui/icons'
import { useSelector, useDispatch } from 'react-redux';
import {
    requestAllAlergic,
    createNewAlergic,
    updateAlergic,
    deleteAlergicById
} from '../../../features/alergic/alergicAPI';
import {
    requestAlergicAction,
    getAllAlergicAction,
    requestErrorAction,
    addAlergicAction,
    deleteSelectedAlergicAction,
    updateSelectedAlergicAction
} from '../../../features/alergic/alergicSlice';
import BaseTable from './BaseTable';
import { CustomButton } from '../../atomic';
import { InputField } from '../../molecules';

const AlergicTable = () => {
    const { auth, alergic } = useSelector(state => state);
    const dispatch = useDispatch();
    const toast = useToast();

    const [updateModal, setUpdateModal] = useState({
        isOpen: false,
        errors: {},
        valid: false,
        alergic: '',
        id: null
    });

    const [deleteModal, setDeleteModal] = useState({
        isOpen: false,
        alergic: null,
        id: null
    });

    const [createModal, setCreateModal] = useState({
        isOpen: false
    });

    const updateModalOpen = useCallback((data) => {
        setUpdateModal({
            ...updateModal,
            isOpen: true,
            alergic: data.name,
            ...data
        });
    }, [updateModal]);

    const updateModalClose = () => {
        setUpdateModal({
            ...updateModal,
            isOpen: false
        });
    };

    const deleteModalOpen = useCallback((data) => {
        setDeleteModal({
            ...deleteModal,
            isOpen: true,
            alergic: data.name,
            ...data
        });
    }, [deleteModal]);

    const deleteModalClose = () => {
        setDeleteModal({
            ...deleteModal,
            isOpen: false
        });
    };

    const createModalOpen = () => {
        setCreateModal({
            ...createModal,
            isOpen: true
        });
    };

    const createModalClose = () => {
        setCreateModal({
            ...createModal,
            isOpen: false,
            alergic: null
        });
    };

    const onChangeNameHandler = (events) => {
        let errors = {};
        let valid = true;

        if (events.target.value.trim().length < 1) {
            errors['name'] = 'Please provide alergic name';
            valid = false;
        }

        setUpdateModal({
            ...updateModal,
            alergic: events.target.value,
            errors: errors,
            valid: valid
        });
    };

    const fetchData = useCallback(({ pageSize, pageIndex, search }) => {
        async function getUser() {
            dispatch(requestAlergicAction());
            try {
                const response = await requestAllAlergic({ pageSize: pageSize, page: pageIndex, name: search });
                dispatch(getAllAlergicAction(response.data.data));
            } catch (err) {
                toast({
                    title: 'Get allergic data error, try again',
                    status: 'error',
                    position: 'top'
                })
            }
        }
        getUser();
    }, [dispatch, toast]);

    const fetchCreate = async () => {
        dispatch(requestAlergicAction());
        try {
            const response = await createNewAlergic({
                idAdmin: auth.user.sub,
                nameAlergic: updateModal.alergic.trim()
            });
            dispatch(addAlergicAction(response.data.data));
            toast({
                title: updateModal.alergic + ' successfully created',
                position: 'top',
                'status': 'success'
            });
        } catch (err) {
            const status = err.response ? err.response.status : 500
            dispatch(requestErrorAction())
            toast({
                title: status > 400 ? 'Internal Server Error creating alergic, try again' : 'Alergic with that name, already created, please check',
                position: 'top',
                status: 'error'
            });
        }
        setUpdateModal({
            ...updateModal,
            position: '',
            valid: false
        })
        createModalClose();
    };

    const fetchUpdate = async () => {
        dispatch(requestAlergicAction());
        try {
            const response = await updateAlergic({
                idAdmin: auth.user.sub,
                idAlergic: updateModal.id,
                nameAlergic: updateModal.alergic.trim()
            });
            dispatch(updateSelectedAlergicAction(response.data.data));
            toast({
                title: 'Successfully updated',
                position: 'top',
                'status': 'success'
            });
        } catch (err) {
            dispatch(requestErrorAction())
            toast({
                title: 'Error updating alergic, try again',
                position: 'top',
                status: 'error'
            });
        }
        updateModalClose();
    };

    const fetchDelete = async () => {
        dispatch(requestAlergicAction());
        try {
            await deleteAlergicById({
                idAdmin: auth.user.sub,
                idAlergic: deleteModal.id
            });
            dispatch(deleteSelectedAlergicAction(deleteModal.id));
            toast({
                title: deleteModal.alergic + ' successfully deleted',
                position: 'top',
                'status': 'success'
            });
        } catch (err) {
            dispatch(requestErrorAction())
            toast({
                title: 'Error deleting alergic, try again',
                position: 'top',
                status: 'error'
            });
        }
        deleteModalClose();
    };

    const columns = useMemo(
        () => [
            {
                Header: 'No',
                Cell: ({ row }) => {
                    let index = row.index + (alergic.page.pageSize * alergic.page.currentPage);
                    return <Text>{index + 1}</Text>
                }
            },
            {
                Header: 'Name',
                accessor: 'name'
            },
            {
                Header: 'Amount',
                accessor: 'count'
            },
            {
                Header: 'Update Time',
                accessor: 'updateAt',
                Cell: ({ value }) => {
                    return <Text>{value ? (new Date(value)).toLocaleString() : '-'}</Text>;
                }
            },
            {
                Header: 'Update By',
                accessor: 'updateBy'
            },
            {
                Header: 'Action',
                accessor: 'id',
                Cell: ({ row, value }) => (
                    <Menu>
                        <MenuButton
                            as={IconButton}
                            aria-label='Options'
                            icon={<HamburgerIcon />}
                            variant='outline'
                        />

                        <MenuList>
                            <MenuItem icon={<EditIcon />} onClick={() => updateModalOpen(row.original)}>
                                Edit
                            </MenuItem>
                            <MenuItem icon={<DeleteIcon />} onClick={() => deleteModalOpen(row.original)}>
                                Delete
                            </MenuItem>
                        </MenuList>

                    </Menu>
                )
            }
        ], [alergic.page, deleteModalOpen, updateModalOpen]
    );

    return <>
        <BaseTable
            columns={columns}
            data={alergic.alergics}
            fetchData={fetchData}
            pageCount={alergic.page.totalPages}
            loading={alergic.isLoading}
            buttonText="Add"
            buttonHandler={() => createModalOpen()}
        />
        <Modal isOpen={createModal.isOpen} onClose={createModalClose}>
            <ModalOverlay />
            <ModalContent>
                <ModalHeader>Create New Alergic</ModalHeader>
                <ModalCloseButton />
                <ModalBody>
                    <InputField label="Name"
                        onChange={onChangeNameHandler}
                        error={updateModal.errors['name']}
                        invalid={updateModal.errors['name']}
                        placeholder='Alergic name' />
                </ModalBody>
                <ModalFooter>
                    <CustomButton mr={3} onClick={createModalClose}>
                        Cancel
                    </CustomButton>
                    <CustomButton
                        colorScheme="blue"
                        isDisabled={!updateModal.valid}
                        onClick={() => fetchCreate()}>Submit</CustomButton>
                </ModalFooter>
            </ModalContent>
        </Modal>
        <Modal isOpen={updateModal.isOpen} onClose={updateModalClose}>
            <ModalOverlay />
            <ModalContent>
                <ModalHeader>Update Alergic</ModalHeader>
                <ModalCloseButton />
                <ModalBody>
                    <InputField label="Name"
                        name="name"
                        value={updateModal.alergic}
                        onChange={onChangeNameHandler}
                        error={updateModal.errors['name']}
                        invalid={updateModal.errors['name']}
                        placeholder='Allergic name' />
                </ModalBody>
                <ModalFooter>
                    <CustomButton mr={3} onClick={updateModalClose}>
                        Cancel
                    </CustomButton>
                    <CustomButton
                        colorScheme="blue"
                        isDisabled={!updateModal.valid}
                        onClick={() => fetchUpdate()}>Update</CustomButton>
                </ModalFooter>
            </ModalContent>
        </Modal>
        <Modal isOpen={deleteModal.isOpen} onClose={deleteModalClose}>
            <ModalOverlay />
            <ModalContent>
                <ModalHeader>Delete Alergic</ModalHeader>
                <ModalCloseButton />
                <ModalBody>
                    <FormControl>
                        <FormLabel>Id</FormLabel>
                        <Input value={deleteModal.id} readOnly />
                    </FormControl>

                    <InputField label="Name"
                        value={deleteModal.alergic}
                        readOnly
                        placeholder='Alergic name' />
                </ModalBody>
                <ModalFooter>
                    <CustomButton mr={3} onClick={deleteModalClose}>
                        Cancel
                    </CustomButton>
                    <CustomButton
                        colorScheme="red"
                        onClick={() => fetchDelete()}>Delete</CustomButton>
                </ModalFooter>
            </ModalContent>
        </Modal>
    </>;
}

export default AlergicTable;