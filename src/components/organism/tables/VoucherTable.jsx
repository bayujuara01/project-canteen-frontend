import React, { useCallback, useMemo, useState } from 'react';
import {
    useToast,
    Text,
    Menu,
    MenuItem,
    MenuList,
    MenuButton,
    IconButton,
    Input,
    FormControl,
    FormLabel,
    Modal,
    ModalOverlay,
    ModalContent,
    ModalHeader,
    ModalFooter,
    ModalBody,
    ModalCloseButton,
    Stack,
} from '@chakra-ui/react';
import {
    HamburgerIcon,
    EditIcon,
    DeleteIcon
} from '@chakra-ui/icons'
import { useSelector, useDispatch } from 'react-redux';
import {
    requestAllUserWithoutPagination
} from '../../../features/user/userAPI';
import {
    requestUserAction,
    requestErrorAction as requestErrorUserAction,
    getAllUserWithoutPagingAction
} from '../../../features/user/userSlice';
import {
    requestAllVoucher,
    createNewVoucher,
    deleteVoucher,
    updateVoucher
} from '../../../features/voucher/voucherAPI'
import {
    requestVoucherAction,
    getAllVoucherAction,
    addVoucherAction,
    updateSelectedVoucherAction,
    deleteSelectedVoucherAction,
    requestErrorVoucherAction
} from '../../../features/voucher/voucherSlice';
import BaseTable from './BaseTable';
import { InputField } from '../../molecules';
import { CustomButton } from '../../atomic';
import Select from 'react-select';
import useForm, { ValidationType } from '../../../utils/useForm';
import { useEffect } from 'react';

const VoucherTable = () => {
    const { auth, voucher, user } = useSelector(state => state);
    const dispatch = useDispatch();
    const toast = useToast();

    const [createModal, setCreateModal] = useState({
        isOpen: false,
        id: null,
        identifier: null
    });

    const [deleteModal, setDeleteModal] = useState({
        isOpen: false
    });

    const [updateModal, setUpdateModal] = useState({
        isOpen: false
    });

    const todayString = new Date().toISOString().substring(0, 10);

    const formCreateVoucherInput = useMemo(() => ([
        {
            name: 'amount',
            validation: {
                min: 1,
                max: 100,
                isNumber: true,
                isRequired: true,
            }
        },
        {
            name: 'user',
            type: ValidationType.SELECT,
            validation: {
                isRequired: true
            }
        },
        {
            name: 'issueAt',
            validation: {
                isRequired: true,
                errorMessage: 'Voucher date issue can\'t be empty'
            }
        },
        {
            name: 'expireAt',
            validation: {
                isRequired: false,
                errorMessage: 'Voucher date expire can\'t be empty'
            }
        }
    ]), []);

    const { formState, handler, errorHandler, checkHandler, resetHandler } = useForm(formCreateVoucherInput);

    const createModalOpen = () => {
        resetHandler();
        setCreateModal({
            ...createModal,
            isOpen: true
        });
    };

    const createModalClose = () => {
        setCreateModal({
            ...createModal,
            isOpen: false,
        });
    };

    const deleteModalOpen = (data) => {
        setDeleteModal({
            ...data,
            isOpen: true,
        });
    }

    const deleteModalClose = () => {
        setDeleteModal({
            isOpen: false
        });
    }

    const updateModalOpen = useCallback((data) => {

        const issueDate = new Date(data.issueAt);
        const issueMonth = issueDate.getMonth() + 1;
        const issueDateString = `${issueDate.getFullYear()}-${issueMonth >= 10 ? issueMonth : `0${issueMonth}`}-${issueDate.getDate()}`;

        handler('user', { value: data.issueId, label: data.issueFor });
        handler('amount', data.usageAmount);
        handler('issueAt', issueDateString);
        handler('expireAt', issueDateString);

        setUpdateModal({
            ...data,
            isOpen: true
        });
    }, [handler]);

    const updateModalClose = () => {
        setUpdateModal({
            isOpen: false
        });
    }

    const fetchUserAllData = useCallback(async () => {
        dispatch(requestUserAction());
        try {
            const response = await requestAllUserWithoutPagination();
            dispatch(getAllUserWithoutPagingAction(response.data.data));
        } catch (err) {
            dispatch(requestErrorUserAction());
        }
    }, [dispatch]);

    const fetchData = useCallback(({ pageSize, pageIndex, search }) => {
        async function getVoucher() {
            dispatch(requestVoucherAction());
            try {
                const response = await requestAllVoucher({ page: pageIndex, pageSize, name: search });
                dispatch(getAllVoucherAction(response.data.data));
            } catch (err) {
                dispatch(requestErrorVoucherAction());
                let message = '';

                if (err.response) {
                    message = err.response.error ? err.response.error : err.response.message
                }

                toast({
                    title: 'Get vouchers data error, try again',
                    description: message,
                    status: 'error',
                    position: 'top'
                });
            }
        }

        getVoucher();
    }, [dispatch, toast]);

    const fetchCreate = async () => {
        let success = false;
        dispatch(requestVoucherAction());
        try {
            const response = await createNewVoucher({
                idAdmin: auth.user.sub,
                idUser: formState['user'].value.value,
                amount: formState['amount'].value,
                issueAt: formState['issueAt'].value,
                expireAt: formState['expireAt'].value,
            });

            success = true;
            dispatch(addVoucherAction(response.data.data));
            toast({
                title: 'New voucher for ' + formState['user'].value.label + ' successfully created',
                position: 'top',
                'status': 'success'
            });
        } catch (err) {
            let message = 'Internal Server Error'
            if (err.response) {
                message = err.response.error ? err.response.error : err.response.message;
            }

            toast({
                title: message,
                position: 'top',
                variant: 'solid',
                isClosable: true,
                duration: 1500,
                status: 'error'
            });
            dispatch(requestErrorVoucherAction(message));
        } finally {
            return success;
        }

    }

    const fetchUpdate = async () => {
        let success = false;
        dispatch(requestVoucherAction());
        try {
            const response = await updateVoucher(updateModal.id, {
                idAdmin: auth.user.sub,
                idUser: formState['user'].value.value,
                amount: formState['amount'].value,
                issueAt: formState['issueAt'].value,
                expireAt: formState['expireAt'].value
            });
            dispatch(updateSelectedVoucherAction(response.data.data));
            dispatch(requestErrorVoucherAction());
            success = true;
            toast({
                title: 'Voucher with identifier ' + updateModal.identifier + ' successfully updated!',
                position: 'top',
                'status': 'success'
            });
        } catch (err) {
            let message = 'Internal Server Error'
            if (err.response) {
                message = err.response.error ? err.response.error : err.response.message;
            }

            toast({
                title: message,
                position: 'top',
                variant: 'solid',
                isClosable: true,
                duration: 1500,
                status: 'error'
            });
            dispatch(requestErrorVoucherAction(message));
        } finally {
            return success;
        }
    }

    const fetchDelete = async () => {
        let success = false;
        dispatch(requestVoucherAction());
        try {
            const response = await deleteVoucher(deleteModal.id);
            dispatch(deleteSelectedVoucherAction(response.data.data.id));
            toast({
                title: 'Voucher with identifier ' + deleteModal.identifier + ' successfully deleted!',
                position: 'top',
                'status': 'success'
            });
        } catch (err) {
            let message = 'Internal Server Error'
            if (err.response) {
                message = err.response.error ? err.response.error : err.response.message;
            }

            toast({
                title: message,
                position: 'top',
                variant: 'solid',
                isClosable: true,
                duration: 1500,
                status: 'error'
            });
            dispatch(requestErrorVoucherAction(message));
        } finally {
            deleteModalClose();
            return success;
        }
    }

    const checkDateValidation = () => {
        let valid = true;
        const issueAt = new Date(formState['issueAt'].value).getTime();
        const expireAt = new Date(formState['expireAt'].value).getTime();
        const today = new Date(todayString).getTime();
        const todayLocal = new Date(todayString).toLocaleDateString();

        if (!formState['issueAt'].value || !formState['expireAt'].value) {
            valid = false;
            errorHandler('expireAt', 'Voucher date expire can\'t be empty');
            errorHandler('issueAt', 'Voucher date issue can\'t be empty')
        }

        if (expireAt < issueAt) {
            valid = false;
            errorHandler('expireAt', 'Expiration date must be more than issue date');
        }

        if (expireAt < today) {
            valid = false;
            errorHandler('expireAt', `Expiration date must be more than today (${todayLocal})`);
        }

        if (issueAt < today) {
            valid = false;
            errorHandler('issueAt', `Expiration date must be more than today (${todayLocal})`);
        }


        return valid;
    }

    const onSubmitHandler = async (events) => {
        events.preventDefault();
        const isDateValidated = checkDateValidation();
        checkHandler();

        if (formState.isFormValid && isDateValidated) {
            if (createModal.isOpen) {
                fetchCreate().then(isSuccess => {
                    createModalClose();
                });
            } else {
                fetchUpdate().then(isSuccess => {
                    updateModalClose();
                })
            }
        } else {
            toast({
                title: "Inputs it is not correct",
                description: "Please check the field, and enter with correct value",
                status: 'error',
                position: 'top'
            });
        }
    }

    const columns = useMemo(() => [
        {
            Header: 'No',
            Cell: ({ row }) => {
                return <Text>{row.index + 1}</Text>
            }
        },
        {
            Header: 'Identifier',
            accessor: 'identifier'
        },
        {
            Header: 'Issue Date',
            accessor: 'issueAt',
            Cell: ({ value }) => {
                return <Text>{value ? (new Date(value)).toLocaleDateString() : '-'}</Text>;
            }
        },
        {
            Header: 'Issue For',
            accessor: 'issueFor'
        },
        {
            Header: 'Usage/Amount',
            Cell: ({ row }) => {
                return <Text>{row.original.countUsage}/{row.original.usageAmount}</Text>
            }
        },
        {
            Header: 'Update By',
            accessor: 'createBy',
            Cell: ({ row, value }) => {
                return <Text>{row.original.updateBy ? row.original.updateBy : value}</Text>
            }
        },
        {
            Header: 'Action',
            accessor: 'id',
            Cell: ({ row, value }) => (
                <Menu>
                    <MenuButton
                        as={IconButton}
                        aria-label='Options'
                        icon={<HamburgerIcon />}
                        variant='outline'
                    />

                    <MenuList>
                        {row.original.countUsage === 0 &&
                            <MenuItem icon={<EditIcon />} onClick={() => updateModalOpen(row.original)}>
                                Edit
                            </MenuItem>}

                        <MenuItem icon={<DeleteIcon />} onClick={() => deleteModalOpen(row.original)}>
                            Delete
                        </MenuItem>
                    </MenuList>

                </Menu>
            )
        }
    ], [updateModalOpen]);

    useEffect(() => {
        fetchUserAllData();
    }, [fetchUserAllData]);

    return <>
        <BaseTable
            columns={columns}
            data={voucher.vouchers}
            fetchData={fetchData}
            pageCount={voucher.page.totalPages}
            loading={voucher.isLoading}
            buttonText='Add'
            buttonHandler={createModalOpen}
        />
        <Modal isOpen={createModal.isOpen} onClose={createModalClose}>
            <ModalOverlay />
            <ModalContent>
                <ModalHeader>Create Guest Voucher</ModalHeader>
                <ModalCloseButton />
                <ModalBody>
                    <Stack>
                        <FormControl isRequired>
                            <FormLabel>Issue For</FormLabel>
                            <Select
                                styles={{ control: base => ({ ...base, borderColor: formState["user"].hasError ? 'red' : '' }) }}
                                value={{ ...formState["user"].value }}
                                options={user.all.map(
                                    usr => ({ value: usr.id, label: usr.name }))}
                                onChange={(e) => handler("user", e)} />
                            {formState["user"].hasError && <Text fontSize="12px" color="red" mt={2}>{formState["user"].error}</Text>}
                        </FormControl>
                        <InputField
                            name="amount"
                            label="Usage Amount"
                            value={formState["amount"].value}
                            type="number"
                            onChange={(e) => handler(e.target.name, e.target.value)}
                            invalid={formState["amount"].hasError}
                            error={formState["amount"].error}
                            placeholder="How many times voucher can be used"
                            required
                        />
                        <InputField
                            name="issueAt"
                            label="Issue At"
                            value={formState["issueAt"].value}
                            type="date"
                            min={todayString}
                            onChange={(e) => {
                                handler(e.target.name, e.target.value);
                                handler("expireAt", e.target.value)
                            }}
                            invalid={formState["issueAt"].hasError}
                            error={formState["issueAt"].error}
                            required />
                    </Stack>
                </ModalBody>
                <ModalFooter>
                    <CustomButton mr={3} onClick={createModalClose}>
                        Cancel
                    </CustomButton>
                    <CustomButton
                        colorScheme="blue"
                        isDisabled={voucher.isLoading}
                        isLoading={voucher.isLoading}
                        onClick={onSubmitHandler}>Submit</CustomButton>
                </ModalFooter>
            </ModalContent>
        </Modal>
        <Modal isOpen={updateModal.isOpen} onClose={updateModalClose}>
            <ModalOverlay />
            <ModalContent>
                <ModalHeader>Update Voucher</ModalHeader>
                <ModalCloseButton />
                <ModalBody>
                    <Stack>
                        <FormControl isRequired>
                            <FormLabel>Issue For</FormLabel>
                            <Select
                                styles={{ control: base => ({ ...base, borderColor: formState["user"].hasError ? 'red' : '' }) }}
                                value={{ ...formState["user"].value }}
                                options={user.all.map(
                                    usr => ({ value: usr.id, label: usr.name }))}
                                onChange={(e) => handler("user", e)} />
                            {formState["user"].hasError && <Text fontSize="12px" color="red" mt={2}>{formState["user"].error}</Text>}
                        </FormControl>
                        <InputField
                            name="amount"
                            label="Usage Amount"
                            value={formState["amount"].value}
                            type="number"
                            onChange={(e) => handler(e.target.name, e.target.value)}
                            invalid={formState["amount"].hasError}
                            error={formState["amount"].error}
                            placeholder="How many times voucher can be used"
                            required
                        />
                        <InputField
                            name="issueAt"
                            label="Issue At"
                            value={formState["issueAt"].value}
                            type="date"
                            min={todayString}
                            onChange={(e) => {
                                handler(e.target.name, e.target.value);
                                handler("expireAt", e.target.value)
                            }}
                            invalid={formState["issueAt"].hasError}
                            error={formState["issueAt"].error}
                            required />
                    </Stack>
                </ModalBody>
                <ModalFooter>
                    <CustomButton mr={3} onClick={updateModalClose}>
                        Cancel
                    </CustomButton>
                    <CustomButton
                        colorScheme="blue"
                        isDisabled={voucher.isLoading}
                        isLoading={voucher.isLoading}
                        onClick={onSubmitHandler}>Submit</CustomButton>
                </ModalFooter>
            </ModalContent>
        </Modal>
        <Modal isOpen={deleteModal.isOpen} onClose={deleteModalClose}>
            <ModalOverlay />
            <ModalContent>
                <ModalHeader>Delete Voucher</ModalHeader>
                <ModalCloseButton />
                <ModalBody>
                    <FormControl>
                        <FormLabel>Id</FormLabel>
                        <Input value={deleteModal.id} readOnly />
                    </FormControl>

                    <InputField label="Identifier"
                        value={deleteModal.identifier}
                        readOnly />
                </ModalBody>
                <ModalFooter>
                    <CustomButton mr={3} onClick={deleteModalClose}>
                        Cancel
                    </CustomButton>
                    <CustomButton
                        colorScheme="red"
                        onClick={() => fetchDelete()}>Delete</CustomButton>
                </ModalFooter>
            </ModalContent>
        </Modal>
    </>;
}

export default VoucherTable;