import React, { useCallback, useMemo } from 'react';
import { Flex, Text, useToast } from '@chakra-ui/react';
import { useDispatch, useSelector } from 'react-redux';
import {
    requestReportTakefoodAction,
    requestErrorAction,
    getAllReportTakefoodAction
} from '../../../features/report/reportTakeFoodSlice';
import {
    requestAllTakefoodReportPaging
} from '../../../features/report/reportAPI';
import BaseTable from './BaseTable';

const TakeFoodTable = ({ exports, setExports, exporter }) => {
    const reportTakeFood = useSelector(state => state.reportTakeFood);
    const dispatch = useDispatch();
    const toast = useToast();

    const fetchData = useCallback(async ({ pageSize, pageIndex, search, date }) => {
        dispatch(requestReportTakefoodAction());
        try {
            let response = null;
            if (search.match(/\d+/g)) {
                response = await requestAllTakefoodReportPaging({ pageSize, page: pageIndex, id: search, date });
            } else {
                response = await requestAllTakefoodReportPaging({ pageSize, page: pageIndex, name: search, date });
            }

            setExports({ ...exports, startDate: date });
            dispatch(getAllReportTakefoodAction(response.data.data));
        } catch (err) {
            toast({
                title: 'Get take food data error, try again',
                status: 'error',
                position: 'top'
            })
        } finally {
            dispatch(requestErrorAction());
        }
    }, [dispatch, toast]);

    const columns = useMemo(() => [
        {
            Header: 'No',
            Cell: ({ row }) => {
                let index = row.index + (reportTakeFood.page.pageSize * reportTakeFood.page.currentPage);
                return <Text>{index + 1}</Text>
            }
        },
        {
            Header: 'Time',
            accessor: 'createAt',
            Cell: ({ value }) => {
                return <Text>{(new Date(value)).toLocaleString()}</Text>
            }
        },
        {
            Header: 'Name',
            accessor: 'employeeName',
            Cell: ({ row, value }) => {
                return <Flex direction="column" textAlign="center">
                    <Text
                        fontSize="md"
                        fontWeight="bold"
                        minWidth="100%"
                    >
                        {row.original.guestName ? row.original.guestName : value}
                    </Text>
                    <Text fontSize="sm" color="gray.400" fontWeight="normal">
                        {row.original.guestName ? value : ''}
                    </Text>
                </Flex>
            }
        },
        {
            Header: 'Email',
            accessor: 'employeeEmail',
            Cell: ({ row, value }) => {
                return <Text>{row.original.guestName ? '-' : value}</Text>
            }
        },
        {
            Header: 'Position',
            accessor: 'employeePosition',
            Cell: ({ row, value }) => {
                return <Text>{row.original.guestName ? '-' : value}</Text>
            }
        }
    ], [reportTakeFood.page]);

    return (<>
        <BaseTable
            columns={columns}
            data={reportTakeFood.data}
            fetchData={fetchData}
            pageCount={reportTakeFood.page.totalPages}
            loading={reportTakeFood.isLoading}
            buttonText="Export"
            buttonHandler={() => exporter()}
            withDate />
    </>);
}

export default TakeFoodTable;