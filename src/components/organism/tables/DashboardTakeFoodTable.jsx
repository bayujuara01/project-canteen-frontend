import React, { useCallback, useMemo } from 'react';
import { Text, useToast } from '@chakra-ui/react';
import { useDispatch, useSelector } from 'react-redux';
import {
    requestReportTakefoodAction,
    requestErrorAction,
    getAllReportTakefoodAction
} from '../../../features/report/reportTakeFoodSlice';
import { requestAllTakefoodReportPaging } from '../../../features/report/reportAPI';
import BaseTable from './BaseTable';

const DashboardTakeFoodTable = () => {
    const reportTakeFood = useSelector(state => state.reportTakeFood);
    const dispatch = useDispatch();
    const toast = useToast();

    const fetchData = useCallback(async ({ pageSize, pageIndex, search, date }) => {
        dispatch(requestReportTakefoodAction());
        try {
            let response = null;
            if (search.match(/\d+/g)) {
                response = await requestAllTakefoodReportPaging({ pageSize, page: pageIndex, id: search, date });
            } else {
                response = await requestAllTakefoodReportPaging({ pageSize, page: pageIndex, name: search, date });
            }
            dispatch(getAllReportTakefoodAction(response.data.data));
        } catch (err) {
            toast({
                title: 'Get take food data error, try again',
                status: 'error',
                position: 'top'
            })
        } finally {
            dispatch(requestErrorAction());
        }
    }, [dispatch, toast]);

    const columns = useMemo(() => [
        {
            Header: 'ID',
            accessor: 'employeeId'
        },
        {
            Header: 'Time',
            accessor: 'createAt',
            Cell: ({ value }) => {
                return <Text>{(new Date(value)).toLocaleString()}</Text>
            }
        },
        {
            Header: 'Name',
            accessor: 'employeeName'
        },
        {
            Header: 'Email',
            accessor: 'employeeEmail'
        },
        {
            Header: 'Position',
            accessor: 'employeePosition'
        }
    ], []);

    return (<>
        <BaseTable
            columns={columns}
            data={reportTakeFood.data}
            fetchData={fetchData}
            pageCount={reportTakeFood.page.totalPages}
            loading={reportTakeFood.isLoading}
            h="55vh" />
    </>);
}

export default DashboardTakeFoodTable;