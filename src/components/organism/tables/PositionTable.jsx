import React, { useCallback, useMemo, useState } from 'react';
import {
    useToast,
    Text,
    Menu,
    MenuItem,
    MenuList,
    MenuButton,
    IconButton,
    FormControl,
    FormLabel,
    Input,
    Modal,
    ModalOverlay,
    ModalContent,
    ModalHeader,
    ModalFooter,
    ModalBody,
    ModalCloseButton,
} from '@chakra-ui/react';
import {
    HamburgerIcon,
    EditIcon,
    DeleteIcon
} from '@chakra-ui/icons'
import { useSelector, useDispatch } from 'react-redux';
import {
    requestAllPosition,
    updatePosition,
    deletePositionById,
    createNewPosition
} from '../../../features/position/positionAPI';
import {
    requestPositionAction,
    getAllPositionAction,
    requestErrorAction,
    updateSelectedPositionAction,
    deleteSelectedPositionAction,
    addPositionAction
} from '../../../features/position/positionSlice';
import BaseTable from './BaseTable';
import { CustomButton } from '../../atomic';
import { InputField } from '../../molecules';

const PositionTable = () => {
    const { auth, position } = useSelector(state => state);
    const dispatch = useDispatch();
    const toast = useToast();

    const [updateModal, setUpdateModal] = useState({
        isOpen: false,
        errors: {},
        valid: false,
        position: '',
        id: null
    });

    const [deleteModal, setDeleteModal] = useState({
        isOpen: false,
        position: null,
        id: null
    });

    const [createModal, setCreateModal] = useState({
        isOpen: false
    });

    const resetUpdateModal = useCallback(() => {
        setUpdateModal({
            isOpen: false,
            errors: {},
            valid: false,
            position: '',
            id: null
        })
    }, []);

    const updateModalOpen = useCallback((data) => {
        setUpdateModal({
            ...updateModal,
            isOpen: true,
            ...data
        });
    }, [updateModal]);

    const updateModalClose = useCallback(() => {
        setUpdateModal({
            ...updateModal,
            isOpen: false
        });
    }, [updateModal]);

    const deleteModalOpen = useCallback((data) => {
        setDeleteModal({
            ...deleteModal,
            isOpen: true,
            ...data
        });
    }, [deleteModal]);

    const deleteModalClose = useCallback(() => {
        setDeleteModal({
            ...deleteModal,
            isOpen: false
        });
    }, [deleteModal]);

    const createModalOpen = () => {
        resetUpdateModal();
        setCreateModal({
            ...createModal,
            isOpen: true
        });
    };

    const createModalClose = () => {
        setCreateModal({
            ...createModal,
            isOpen: false,
            position: null
        });
    };

    const onChangeNameHandler = (events) => {
        let errors = {};
        let valid = true;

        if (events.target.value.trim().length < 1) {
            errors['name'] = 'Please provide position name';
            valid = false;
        }

        setUpdateModal({
            ...updateModal,
            position: events.target.value,
            errors: errors,
            valid: valid
        });
    };

    const fetchData = useCallback(({ pageSize, pageIndex, search }) => {
        async function getUser() {
            dispatch(requestPositionAction());
            try {
                const response = await requestAllPosition({ pageSize: pageSize, page: pageIndex, name: search });
                dispatch(getAllPositionAction(response.data.data));
            } catch (err) {
                toast({
                    title: 'Get position data error, try again',
                    status: 'error',
                    position: 'top'
                })
            }
        }
        getUser();
    }, [dispatch, toast]);

    const fetchUpdate = useCallback(async () => {
        dispatch(requestPositionAction());
        try {
            const response = await updatePosition({
                idAdmin: auth.user.sub,
                idPosition: updateModal.id,
                namePosition: updateModal.position.trim()
            });
            dispatch(updateSelectedPositionAction(response.data.data));
            toast({
                title: 'Successfully updated',
                position: 'top',
                'status': 'success'
            });
        } catch (err) {
            dispatch(requestErrorAction())
            toast({
                title: 'Error updating position, try again',
                position: 'top',
                status: 'error'
            });
        }
        updateModalClose();
    }, [dispatch, auth.user, toast, updateModal, updateModalClose]);

    const fetchDelete = useCallback(async () => {
        dispatch(requestPositionAction());

        try {
            await deletePositionById({
                idAdmin: auth.user.sub,
                idPosition: deleteModal.id
            });
            dispatch(deleteSelectedPositionAction(deleteModal.id));
            toast({
                title: deleteModal.position + ' successfully deleted',
                position: 'top',
                'status': 'success'
            });
        } catch (err) {
            dispatch(requestErrorAction())

            toast({
                title: 'Error deleting position, try again',
                position: 'top',
                status: 'error'
            });
        }
        deleteModalClose();
    }, [dispatch, deleteModal, auth.user, toast, deleteModalClose]);

    const fetchCreate = async () => {
        dispatch(requestPositionAction());
        try {
            const response = await createNewPosition({
                idAdmin: auth.user.sub,
                namePosition: updateModal.position.trim()
            });
            dispatch(addPositionAction(response.data.data));
            toast({
                title: updateModal.position + ' successfully created',
                position: 'top',
                'status': 'success'
            });
        } catch (err) {
            const status = err.response ? err.response.status : 500
            dispatch(requestErrorAction())
            toast({
                title: status > 400 ? 'Internal Server Error creating position, try again' : 'Position with that name, already created, please check',
                position: 'top',
                status: 'error'
            });
        }
        setUpdateModal({
            ...updateModal,
            position: '',
            valid: false
        })
        createModalClose();
    }

    const columns = useMemo(
        () => [
            {
                Header: 'No',
                Cell: ({ row }) => {
                    let index = row.index + (position.page.pageSize * position.page.currentPage);
                    return <Text>{index + 1}</Text>
                }
            },
            {
                Header: 'Postition',
                accessor: 'position'
            },
            {
                Header: 'Update Time',
                accessor: 'updateAt',
                Cell: ({ value }) => {
                    return <Text>{value ? (new Date(value)).toLocaleString() : '-'}</Text>;
                }
            },
            {
                Header: 'Update By',
                accessor: 'updateBy'
            },
            {
                Header: 'Action',
                accessor: 'id',
                Cell: ({ row, value }) => (
                    <Menu>
                        {row.original.position.toLowerCase() !== 'office girl' && <>
                            <MenuButton
                                as={IconButton}
                                aria-label='Options'
                                icon={<HamburgerIcon />}
                                variant='outline'
                            />

                            <MenuList>
                                <MenuItem icon={<EditIcon />} onClick={() => updateModalOpen(row.original)}>
                                    Edit
                                </MenuItem>
                                <MenuItem icon={<DeleteIcon />} onClick={() => deleteModalOpen(row.original)}>
                                    Delete
                                </MenuItem>
                            </MenuList>
                        </>
                        }
                    </Menu>
                )
            }
        ], [position.page, deleteModalOpen, updateModalOpen]
    );

    return <>
        <BaseTable
            columns={columns}
            data={position.positions}
            fetchData={fetchData}
            pageCount={position.page.totalPages}
            loading={position.isLoading}
            buttonText="Add"
            buttonHandler={() => createModalOpen()}
        />
        <Modal isOpen={createModal.isOpen} onClose={createModalClose}>
            <ModalOverlay />
            <ModalContent>
                <ModalHeader>Create New Position</ModalHeader>
                <ModalCloseButton />
                <ModalBody>
                    <InputField label="Name"
                        value={updateModal.position}
                        onChange={onChangeNameHandler}
                        error={updateModal.errors['name']}
                        invalid={updateModal.errors['name']}
                        placeholder='Position name' />
                </ModalBody>
                <ModalFooter>
                    <CustomButton mr={3} onClick={createModalClose}>
                        Cancel
                    </CustomButton>
                    <CustomButton
                        colorScheme="blue"
                        isDisabled={!updateModal.valid}
                        onClick={() => fetchCreate()}>Submit</CustomButton>
                </ModalFooter>
            </ModalContent>
        </Modal>
        <Modal isOpen={updateModal.isOpen} onClose={updateModalClose}>
            <ModalOverlay />
            <ModalContent>
                <ModalHeader>Update Position</ModalHeader>
                <ModalCloseButton />
                <ModalBody>

                    <InputField label="Name"
                        name="name"
                        value={updateModal.position}
                        onChange={onChangeNameHandler}
                        error={updateModal.errors['name']}
                        invalid={updateModal.errors['name']}
                        placeholder='Position name' />
                </ModalBody>
                <ModalFooter>
                    <CustomButton mr={3} onClick={updateModalClose}>
                        Cancel
                    </CustomButton>
                    <CustomButton
                        colorScheme="blue"
                        isDisabled={!updateModal.valid}
                        onClick={() => fetchUpdate()}>Update</CustomButton>
                </ModalFooter>
            </ModalContent>
        </Modal>
        <Modal isOpen={deleteModal.isOpen} onClose={deleteModalClose}>
            <ModalOverlay />
            <ModalContent>
                <ModalHeader>Delete Position</ModalHeader>
                <ModalCloseButton />
                <ModalBody>
                    <FormControl>
                        <FormLabel>Id</FormLabel>
                        <Input value={deleteModal.id} readOnly />
                    </FormControl>

                    <InputField label="Name"
                        value={deleteModal.position}
                        readOnly
                        placeholder='Position name' />
                </ModalBody>
                <ModalFooter>
                    <CustomButton mr={3} onClick={deleteModalClose}>
                        Cancel
                    </CustomButton>
                    <CustomButton
                        colorScheme="red"
                        onClick={() => fetchDelete()}>Delete</CustomButton>
                </ModalFooter>
            </ModalContent>
        </Modal>
    </>;
}

export default PositionTable;