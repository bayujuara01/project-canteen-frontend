import { useEffect, useReducer } from "react";

const USE_FORM = "USE_FORM_CHANGE";
const USE_FORM_ON_ERROR = "USE_FORM_ERROR";
const USE_FORM_UPDATE_STATE = "USE_FORM_UPDATE_STATE";
const USE_FORM_CHECK_ALL = "USE_FORM_CHECK_ALL";

export const ValidationType = {
    TEXT: 'text',
    SELECT: 'select',
    MULTI_SELECT: 'multi-select',
    RADIO: 'radio',
    SINGLE_IMAGE: 'single-image'
};

const createInitialState = (inputs) => {
    let state = { isFormValid: false };
    for (const input of inputs) {
        state = {
            ...state,
            [input.name]: {
                type: input.type ? input.type : ValidationType.TEXT,
                value: input.defaultValue ? input.defaultValue : "",
                touched: false,
                hasError: false,
                error: "",
                validation: {
                    ...input.validation
                }
            }
        }
    }

    return state;
}

const createInputValidation = (inputs) => {
    let validation = {}

    for (let input of inputs) {
        validation = {
            ...validation,
            [input.name]: { ...input.validation }
        }
    }

    return validation;
}


const typeTextValidation = (name, value, validation) => {
    let hasError = false;
    let error = ''
    const { min, max, isNumber, isRequired, pattern, errorMessage } = validation;

    if (isNumber && typeof value === 'number') {
        value = value.toString();
    }

    if (isRequired && value.trim() === '') {
        hasError = true;
        error = `${name} cannot be empty`;
    } else if (max != null && min != null && (value.trim().length > max || value.trim().length < min)) {
        hasError = true;
        error = `${name} must between ${min} - ${max} characters`;
    } else if (isNumber && value.match(/\d+/g) && min !== null && max != null) {
        const intValue = parseInt(value);
        if (intValue < min) {
            hasError = true;
            error = `${name} must be equal or greather than ${min}`
        }

        if (intValue > max) {
            hasError = true;
            error = `${name} must be equal or less than ${max}`
        }
    } else if (pattern != null && !value.match(pattern)) {
        hasError = true;
        error = errorMessage;
    }

    if (hasError && errorMessage && pattern == null) {
        error = errorMessage;
    }

    return { hasError, error };
}

const typeSelectValidation = (name, value, validation) => {
    let hasError = false;
    let error = ''
    const { isRequired } = validation;

    if (isRequired && !value) {
        hasError = true;
        error = `Please select one of ${name} list`;
    }

    return { hasError, error };
}

const typeMultiSelectValidation = (name, value, validation) => {
    let hasError = false;
    let error = ''
    const { isRequired, max, min } = validation;

    if (isRequired && !value) {
        hasError = true;
        error = `Please select of ${name} list`;
    } else if (max != null && min != null && (value.length > max || value.length < min)) {
        hasError = true;
        error = `select ${name} only between ${min} - ${max} item`;
    }

    return { hasError, error };
}

const typeRadioValidation = (name, value, validation) => {
    let hasError = false;
    let error = ''
    const { isRequired } = validation;

    if (isRequired && !value) {
        hasError = true;
        error = `Please select one of ${name} radio button`;
    }

    return { hasError, error }
}

const typeSingleImageValidation = (name, value, validation) => {
    let hasError = false;
    let error = ''
    const { isRequired } = validation;

    if (isRequired && (!value.file || !value.image)) {
        hasError = true;
        error = `Please select one ${name} file`;
    }

    return { hasError, error }
}

const inputValidation = (type = ValidationType.TEXT, name, value, validation) => {
    let validationType = type.trim().toLowerCase();
    switch (validationType) {
        case ValidationType.SELECT:
            return typeSelectValidation(name, value, validation);
        case ValidationType.MULTI_SELECT:
            return typeMultiSelectValidation(name, value, validation);
        case ValidationType.RADIO:
            return typeRadioValidation(name, value, validation);
        case ValidationType.SINGLE_IMAGE:
            return typeSingleImageValidation(name, value, validation);
        default:
            return typeTextValidation(name, value, validation);
    }
}

const onSetCustomErrorAction = (name, errorMessage, dispatch, formState) => {
    let isFormValid = false;

    dispatch({
        type: USE_FORM_ON_ERROR,
        data: {
            name,
            hasError: true,
            error: errorMessage,
            isFormValid
        }
    });
}


const onInputChangeAction = (name, value, validations, dispatch, formState) => {
    let isFormValid = true;
    const { hasError, error } = inputValidation(formState[name].type, name, value, validations[name]);

    for (const key in formState) {
        const input = formState[key];

        if (key === name && hasError) {
            isFormValid = false;
            break;
        } else if (key !== name && input.hasError) {
            isFormValid = false;
            break;
        }
    }

    dispatch({
        type: USE_FORM,
        data: {
            name,
            value,
            hasError,
            error,
            touched: false,
            isFormValid
        }
    });
};

const submitCheckForm = (dispatch, formState) => {
    let isFormValid = true;
    let newState = { ...formState };

    for (const key in formState) {
        const input = formState[key];

        if (typeof input !== 'object') continue;

        const { hasError, error } = inputValidation(input.type, key, input.value, input.validation);
        if (hasError) {
            isFormValid = false;
        }
        newState = {
            ...newState,
            [key]: { ...input, hasError, error },
            isFormValid
        };
    }

    dispatch({ type: USE_FORM_CHECK_ALL, all: newState });
    return isFormValid;
}



const formReducer = (state, action) => {

    switch (action.type) {
        case USE_FORM: {
            const { name, value, hasError, error, touched, isFormValid } = action.data;
            return {
                ...state,
                [name]: { ...state[name], value, hasError, error, touched },
                isFormValid
            };
        }
        case USE_FORM_ON_ERROR: {
            const { name, hasError, error } = action.data;
            return {
                isFormValid: false,
                ...state,
                [name]: { ...state[name], hasError, error }
            };
        }
        case USE_FORM_UPDATE_STATE:
            return {
                ...state,
                ...action.all
            };
        case USE_FORM_CHECK_ALL:
            return {
                ...action.all
            };
        default: return state;
    }
}

const useForm = (inputs) => {

    const initialState = createInitialState(inputs);
    const validations = createInputValidation(inputs);
    const [formState, dispatch] = useReducer(formReducer, initialState);
    const handler = (name, value) => onInputChangeAction(name, value, validations, dispatch, formState);
    const errorHandler = (name, errorMessage) => onSetCustomErrorAction(name, errorMessage, dispatch, formState);
    const checkHandler = () => submitCheckForm(dispatch, formState);
    const resetHandler = () => dispatch({ type: USE_FORM_UPDATE_STATE, all: createInitialState(inputs) });
    // const resetHandler = () => { };
    useEffect(() => {
        dispatch({ type: USE_FORM_UPDATE_STATE, all: createInitialState(inputs) });
    }, [inputs, dispatch]);

    return { formState, handler, errorHandler, checkHandler, resetHandler };
}


export default useForm;