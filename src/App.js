import React from 'react';
import '@fontsource/open-sans/700.css';
import '@fontsource/raleway/400.css';
import { Routes, Route } from 'react-router-dom';
import { ChakraProvider } from '@chakra-ui/react';
import theme from './app/theme';
import { useSelector } from 'react-redux';
import LoginPages from './pages/LoginPages';
import HomePage from './pages/HomePage';
import NotFoundPage from './pages/NotFoundPage';
import EmployeePage from './pages/EmployeePage';
import DashboardPage from './pages/DashboardPage';
import ResetPasswordPage from './pages/ResetPasswordPage';
import CreateEmployeePage from './pages/CreateEmployeePage';
import DetailEmployeePage from './pages/DetailEmployeePage';
import PositionPage from './pages/PositionPage';
import AlergicPage from './pages/AlergicPage';
import ReportFeedbackPage from './pages/ReportFeedbackPage';
import ReportComplaintPage from './pages/ReportComplaintPage';
import StaticQRPage from './pages/StaticQRPage';
import { NotFoundLayout } from './components/templates';
import { useNavigate } from 'react-router';
import SurveyPage from './pages/SurveyPage';
import ReportTakeFoodPage from './pages/ReportTakeFoodPage';
import EditEmployeePage from './pages/EditEmployeePage';
import VoucherPage from './pages/VoucherPage';

function App() {

  const auth = useSelector(state => state.auth);
  const navigate = useNavigate();

  return (
    <ChakraProvider theme={theme}>
      <Routes>
        {(auth.isAuth) ? <>
          <Route path="/" element={<HomePage />}>
            <Route index element={<DashboardPage />} />
            <Route path="employee" element={<EmployeePage />} />
            <Route path="employee/add" element={<CreateEmployeePage />} />
            <Route path="employee/edit" element={<EditEmployeePage />} />
            <Route path="employee/detail" element={<DetailEmployeePage />} />
            <Route path="position" element={<PositionPage />} />
            <Route path="alergic" element={<AlergicPage />} />
            <Route path="report/feedback" element={<ReportFeedbackPage />} />
            <Route path="report/complaint" element={<ReportComplaintPage />} />
            <Route path="report/takefood" element={<ReportTakeFoodPage />} />
            <Route path="voucher" element={<VoucherPage />} />
            <Route path="survey" element={<SurveyPage />} />
            <Route path="qr" element={<StaticQRPage />} />
            <Route path="*" element={<NotFoundLayout buttonHandler={() => navigate('/')} />} />
          </Route>
        </> : <>
          <Route path="/" element={<LoginPages />} />
        </>}
        <Route path="/resetpwd" element={<ResetPasswordPage />} />
        {auth.isExpired ? <>
          <Route path="*" element={<LoginPages />} />
        </> : <>
          <Route path="*" element={<NotFoundPage />} />
        </>}
      </Routes>
    </ChakraProvider>
  );
}

export default App;
