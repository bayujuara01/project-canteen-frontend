import { configureStore, combineReducers } from "@reduxjs/toolkit";
import authReducer from '../features/auth/authSlice';
import userReducer from '../features/user/userSlice';
import alergicReducer from "../features/alergic/alergicSlice";
import positionReducer from "../features/position/positionSlice";
import surveyReducer from "../features/survey/surveySlice";
import reportFeedbackReducer from "../features/report/reportFeedbackSlice";
import reportComplaintReducer from '../features/report/reportComplaintSlice';
import reportTakeFoodReducer from "../features/report/reportTakeFoodSlice";
import dashboardReducer from "../features/dashboard/dashboardSlice";
import roleReducer from "../features/role/roleSlice";
import voucherReducer from "../features/voucher/voucherSlice";
import qrReducer from "../features/qr/qrSlice";

import {
    persistReducer,
    persistStore,
    FLUSH, REHYDRATE, PAUSE, PERSIST, PURGE, REGISTER
} from "redux-persist";
import storage from "redux-persist/lib/storage";

const persistConfig = {
    key: 'root',
    whitelist: ['auth', 'user'],
    storage
}

const rootReducer = combineReducers({
    auth: authReducer,
    user: userReducer,
    alergic: alergicReducer,
    position: positionReducer,
    survey: surveyReducer,
    reportFeedback: reportFeedbackReducer,
    reportComplaint: reportComplaintReducer,
    reportTakeFood: reportTakeFoodReducer,
    dashboard: dashboardReducer,
    role: roleReducer,
    voucher: voucherReducer,
    qr: qrReducer
});

const persistedReducer = persistReducer(persistConfig, rootReducer);

export const store = configureStore({
    reducer: persistedReducer,
    middleware: getDefaultMiddleware => getDefaultMiddleware({
        serializableCheck: {
            ignoredActions: [FLUSH, REHYDRATE, PAUSE, PERSIST, PURGE, REGISTER]
        }
    })
});

export const persistor = persistStore(store);