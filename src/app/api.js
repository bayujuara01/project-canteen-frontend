import axios from 'axios';
import { SERVER_URL, TOKEN_PREFIX } from "./constant";
import { signOut } from '../features/auth/authSlice';

let store;

const api = axios.create({
    baseURL: SERVER_URL,
    headers: {
        'Content-Type': 'application/json'
    },
    timeout: 3000
});

api.interceptors.request.use(config => {
    config.headers.authorization = `${TOKEN_PREFIX} ${store.getState().auth.token}`;
    return config;
});

api.interceptors.response.use(response => {
    return response;
}, error => {
    if (error.response.status === 401 && error.response) {
        let expiredError = error;

        if (error.response.data.error_code === 'AUTH-231') {

            expiredError = new Error();
            expiredError.response = {
                ...error.response,
                data: {
                    ...error.response.data,
                    message: 'Session already expired, login again'
                }
            }

            store.dispatch(signOut(true));
        }

        return Promise.reject(expiredError);
    } else {
        return Promise.reject(error);
    }
})

export const injectStore = _store => {
    store = _store;
}

export default api;