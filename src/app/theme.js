import { extendTheme } from "@chakra-ui/react";

const theme = extendTheme({
    styles: {
        global: {
            'html': {
                overflow: 'hidden'
            }
        }
    },
    fonts: {
        heading: 'Open Sans',
        body: 'Raleway'
    }
});

export default theme;