import React from "react";
import {
    Box,
    Flex
} from '@chakra-ui/react';
import VoucherTable from "../components/organism/tables/VoucherTable";

const VoucherPage = () => {
    return <>
        <Box w='100%'>
            <Flex direction="column" pt={{ base: "120px", md: "24px" }} px="8px">
                <VoucherTable />
            </Flex>
        </Box>
    </>;
}

export default VoucherPage;