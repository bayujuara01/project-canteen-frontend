import {
    Flex,
    Stack,
    Text,
    Image,
    useColorModeValue
} from '@chakra-ui/react';
import React from "react";
import SignInForm from '../components/organism/forms/SignInForm';
import CantnLogo from '../assets/images/cantn_logo_tr.png';

const LoginPages = () => {

    return (
        <Flex
            minH={'100vh'}
            align={'center'}
            justify={'center'}
            bg={useColorModeValue('gray.50', 'gray.800')}>
            <Stack spacing={8} mx={'auto'} maxW={'lg'} py={12} px={6}>
                <Stack align={'center'}>
                    <Image src={CantnLogo} h="10vh" />
                    <Text fontSize={'xl'} color={'gray.600'}>
                        Welcome Abroad 💻
                    </Text>
                </Stack>
                <SignInForm />
            </Stack>
        </Flex>

    );
}

export default LoginPages;