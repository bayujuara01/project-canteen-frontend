import React, { useCallback, useState } from 'react';
import * as Xlsx from 'xlsx';
import * as FileSaver from 'file-saver';
import {
    Box,
    Flex
} from '@chakra-ui/react';
import TakeFoodTable from '../components/organism/tables/TakeFoodTable';
import { requestAllTakefoodReportForExport } from '../features/report/reportAPI';

const ReportTakeFoodPage = () => {
    const now = new Date();

    const [exports, setExports] = useState({
        startDate: `${now.getFullYear()}-${now.getMonth()}-${now.getDate()}`,
        endDate: ''
    });

    /* Test Export */
    const fileType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
    const fileExt = '.xlsx';

    const exportToCSV = (csvData, fileName) => {
        const ws = Xlsx.utils.json_to_sheet(csvData);
        const wb = { Sheets: { 'data': ws }, SheetNames: ['data'] }
        const excelBuffer = Xlsx.write(wb, { bookType: 'xlsx', type: 'array' });
        const data = new Blob([excelBuffer], { type: fileType });
        FileSaver.saveAs(data, fileName + fileExt);
    }
    /* End Test Export */

    const fetchDataExport = useCallback(async () => {
        try {
            const response = await requestAllTakefoodReportForExport({
                startDate: exports.startDate,
                endDate: exports.endDate
            });

            const newArr = response.data.data.map((value, index) => {
                const createTime = new Date(value.createAt);
                return ({
                    'No': index + 1,
                    'Id': value.id,
                    'NIK': value.nik ? value.nik : '',
                    'Name': value.employeeName,
                    'Guest Name': value.guestName ? value.guestName : '-',
                    'Time': createTime.toLocaleTimeString()
                });
            });
            const fileName = `REPORT_FOOD_CANTN_${exports.startDate}-${exports.endDate ? exports.endDate : ''}`;
            exportToCSV(newArr, fileName);
        } catch (err) {
            alert("Error")
        }
    }, [exports]);

    return <>
        <Box w='100%'>
            <Flex direction="column" pt={{ base: "120px", md: "16px" }} px="8px">
                <TakeFoodTable
                    exports={exports}
                    setExports={setExports}
                    exporter={fetchDataExport} />
            </Flex>
        </Box>
    </>
}

export default ReportTakeFoodPage;