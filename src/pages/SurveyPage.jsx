import React, { useCallback, useEffect, useState } from 'react';
import {
    Box,
    Flex,
    Text,
    useToast,
    Button,
    Modal,
    ModalOverlay,
    ModalContent,
    ModalHeader,
    ModalFooter,
    ModalBody,
    ModalCloseButton,
    FormControl,
    FormLabel,
    Input,
    Radio,
    RadioGroup,
    Stack,
    Spinner,
    Switch,
    Select as Dropdown,
    useDisclosure
} from '@chakra-ui/react';
import {
    AddIcon,
    DeleteIcon
} from '@chakra-ui/icons'
import { Card, CardBody } from '../components/atomic';
import QuestionCard from '../components/organism/survey/QuestionCard';
import { InputField } from '../components/molecules';
import {
    getAllQuestion,
    getAllQuestionInputType,
    createNewQuestion,
    updateQuestionById,
    deleteQuestionById
} from '../features/survey/surveyAPI';
import {
    requestQuestionAction,
    requestQuestionErrorAction,
    getAllInputTypeAction,
    getAllQuestionAction,
    deleteQuestionByIdAction
} from '../features/survey/surveySlice';
import { useSelector, useDispatch } from 'react-redux';

const SurveyPage = () => {
    const { survey, auth } = useSelector(state => state);
    const dispatch = useDispatch();
    const toast = useToast();
    const { isOpen, onOpen, onClose } = useDisclosure();
    const {
        isOpen: isDeleteOpen,
        onOpen: onDeleteOpen,
        onClose: onDeleteClose } = useDisclosure();
    const {
        isOpen: isEditOpen,
        onOpen: onEditOpen,
        onClose: onEditClose } = useDisclosure();
    const [questionDelete, setQuestionDelete] = useState({
        id: null,
        title: ''
    });

    const [publish, setPublish] = useState({
        status: ''
    });

    const [question, setQuestion] = useState({
        id: null,
        type: '0',
        inputType: 1,
        modal: 'md',
        options: [],
        title: '',
        subtext: ''
    });

    const [isPublish, setIsPublish] = useState(false);
    const [option, setOption] = useState('');

    const resetQuestion = useCallback(() => {
        setQuestion({
            id: null,
            type: '0',
            inputType: 1,
            modal: 'md',
            options: [],
            title: '',
            subtext: ''
        });
    }, []);

    const handleInputTypeSelector = (events) => {
        const selectedIndex = events.target.selectedIndex;
        const modalType = events.target[selectedIndex].innerText.toLowerCase().includes('text') ? 'md' : 'xxl'
        setQuestion({
            ...question,
            inputType: events.target.value,
            modal: modalType,
            options: []
        });
    }

    const handleInputTextChange = (events) => {
        setQuestion({
            ...question,
            [events.target.name]: events.target.value
        })
    }

    const handleOptionTextChange = (events) => {
        setOption(prevState => events.target.value)
    }

    const handleAddOption = () => {
        setQuestion({
            ...question,
            options: [...question.options, option]
        });
        setOption('');
    }

    const handleDeleteOption = (index) => {
        setQuestion(prevState => ({
            ...prevState,
            options: prevState.options.filter((option, idx) => idx !== index)
        }));
    }

    const onAddNewQuestionClick = () => {
        setQuestion({
            type: '0',
            inputType: 1,
            modal: 'md',
            options: [],
            title: '',
            subtext: ''
        });
        onOpen();
    }

    const onEditClick = (questionId) => {
        const quest = survey.questions.find(q => q.id === questionId);
        setQuestion({
            ...question,
            ...quest,
            id: quest.id,
            inputType: quest.inputType.id,
            type: quest.type.toString(),
            modal: quest.inputType.id === 1 ? 'md' : 'xxl',
            options: [...quest.options.map(opt => opt.name)]
        })
        setIsPublish((v => quest.publish));
        onEditOpen();
    }

    const onDeleteClick = (questionId, title) => {
        setQuestionDelete({
            ...questionDelete,
            id: questionId,
            title
        });
        onDeleteOpen();
    }

    const fetchDelete = async () => {
        try {
            const response = await deleteQuestionById({
                idQuestion: questionDelete.id,
                idAdmin: auth.user.sub
            });
            dispatch(deleteQuestionByIdAction(questionDelete.id));
            toast({
                title: `Question successfully deleted`,
                description: `Question ${response.data.data.title} successfully deleted`,
                position: 'top',
                'status': 'success'
            });
        } catch (err) {
            toast({
                title: 'Error deleting question, try again',
                position: 'top',
                status: 'error'
            });
        } finally {
            onDeleteClose();
        }
    }

    const fetchUpdate = async (events) => {
        events.preventDefault();

        try {
            await updateQuestionById(question.id, {
                idAdmin: auth.user.sub,
                inputTypeId: question.inputType,
                type: parseInt(question.type),
                options: question.options,
                title: question.title,
                subtext: question.subtext,
                isPublish
            });
            toast({
                title: 'question for survey successfully updated',
                position: 'top',
                status: 'success'
            });
            window.location.reload();
        } catch (err) {
            let errorDesc = '';

            if (err.response) {
                const errors = err.response.data.error;
                for (const key in errors) {
                    errorDesc += `- ${key} ${errors[key]}\n`;
                }
            }

            toast({
                title: 'Error when updating question for survey',
                position: 'top',
                description: errorDesc,
                status: 'error'
            });
        } finally {
            resetQuestion();
            onEditClose();
        }
    }

    const fetchCreate = async (events) => {
        events.preventDefault();

        try {
            await createNewQuestion({
                idAdmin: auth.user.sub,
                inputTypeId: question.inputType,
                type: parseInt(question.type),
                options: question.options,
                title: question.title,
                subtext: question.subtext,
                isPublish
            });
            toast({
                title: 'New question for survey successfully created',
                position: 'top',
                status: 'success'
            });
        } catch (err) {
            let errorDesc = '';

            if (err.response) {
                const errors = err.response.data.detail;
                for (const key in errors) {
                    errorDesc += `- ${key} ${errors[key]}\n`;
                }
            }

            toast({
                title: 'Error when creating question for survey',
                position: 'top',
                description: errorDesc,
                status: 'error'
            });
        } finally {
            resetQuestion();
            onClose();
        }
    }

    const fetchData = useCallback(async () => {
        dispatch(requestQuestionAction());
        try {
            const responseInputType = await getAllQuestionInputType();
            const responseQuestion = await getAllQuestion({ status: publish.status });

            dispatch(getAllInputTypeAction(responseInputType.data.data));
            dispatch(getAllQuestionAction(responseQuestion.data.data));
        } catch (err) {
            dispatch(requestQuestionErrorAction());
            toast({
                title: 'Error when fethching data, please reload this page',
                status: 'error',
                position: 'top'
            })
        }
    }, [dispatch, toast, publish]);

    useEffect(() => {
        fetchData();
    }, [fetchData, question]);

    return (

        <Box w='100%'>
            <Flex justifyContent="end" mt={4}>
                <Dropdown
                    w="140px"
                    defaultValue=''
                    placeholder='All'
                    onChange={(e) => setPublish({ ...publish, status: e.target.value })}>
                    <option value='1'>Published</option>
                    <option value='2'>Draft</option>
                </Dropdown>
                <Button ml="2" variant="solid" onClick={onAddNewQuestionClick} colorScheme="blue">Add</Button>
            </Flex>

            <Flex direction="column">

                {survey.questions.map((question, index) => {
                    return (
                        <Box my="10px" key={index}>
                            <QuestionCard
                                id={question.id}
                                key={question.id}
                                description={question.subtext}
                                type={question.type ? 'Complaint' : 'Feedback'}
                                inputType={question.inputType.typeName}
                                optionChoice={question.options}
                                title={question.title}
                                isPublish={question.publish}
                                isAnswered={question.answered}
                                onDeleteClick={onDeleteClick}
                                onEditClick={onEditClick}
                            />
                        </Box>
                    );
                })}

                {survey.isLoading &&
                    <Box my="10px">
                        <Card>
                            <CardBody>
                                <Flex w="full" justifyContent="center">
                                    <Spinner
                                        thickness='4px'
                                        speed='0.65s'
                                        emptyColor='gray.200'
                                        color='blue.500'
                                        size='xl'
                                    />
                                </Flex>
                            </CardBody>
                        </Card>
                    </Box>}
            </Flex>
            <Modal
                isOpen={isOpen}
                onClose={onClose}
                size={question.modal}
            >
                <ModalOverlay />
                <ModalContent w={question.modal === 'xxl' ? '800px' : null}>
                    <form method='post' onSubmit={fetchCreate}>
                        <ModalHeader>Create Question</ModalHeader>
                        <ModalCloseButton />
                        <ModalBody pb={6}>
                            <Flex>
                                <Stack w={question.modal === 'xxl' ? '50%' : '100%'}>
                                    <InputField
                                        label="Title"
                                        required
                                        name="title"
                                        value={question.title}
                                        onChange={handleInputTextChange}
                                    />
                                    <Stack direction="row" spacing="8">
                                        <FormControl isRequired>
                                            <FormLabel>Type</FormLabel>
                                            <RadioGroup value={question.type} onChange={(value) => setQuestion({ ...question, type: value })} >
                                                <Stack direction='row'>
                                                    <Radio value='0'>Feedback</Radio>
                                                    <Radio value='1'>Complaint</Radio>
                                                </Stack>
                                            </RadioGroup>
                                        </FormControl>
                                        <FormControl isRequired>
                                            <FormLabel htmlFor="publish-toggle">Publish</FormLabel>
                                            <Switch
                                                id="publish-toggle"
                                                isChecked={isPublish}
                                                value={isPublish}
                                                onChange={(e) => setIsPublish(v => !isPublish)}
                                            /> {isPublish ? ' Yes' : ' No'}
                                        </FormControl>
                                    </Stack>
                                    <InputField
                                        label="Description"
                                        required
                                        name="subtext"
                                        value={question.subtext}
                                        onChange={handleInputTextChange}
                                    />
                                    <FormControl mt="10px" isRequired>
                                        <FormLabel>Input Type</FormLabel>
                                        <Dropdown placeholder='Select option' value={question.inputType} onChange={handleInputTypeSelector} >
                                            {survey.inputType.map((type, index) => {
                                                return <option key={index} name={type.name} value={type.id}>{type.typeName}</option>
                                            })}
                                        </Dropdown>
                                    </FormControl>
                                </Stack>
                                {question.modal === 'xxl' && <Stack w="50%" ml="20px">
                                    <Text>Option Choices</Text>
                                    {question.options.map((option, index) =>
                                        <Flex key={index}>
                                            <Input type="text" w="60%" readOnly value={option} />
                                            <Button
                                                ml="10px"
                                                leftIcon={<DeleteIcon />}
                                                variant="outline"
                                                onClick={() => handleDeleteOption(index)}
                                                colorScheme="red">Delete</Button>
                                        </Flex>
                                    )}
                                    <Flex>
                                        <Input type="text" w="60%" value={option} onChange={handleOptionTextChange} />
                                        <Button
                                            ml="10px"
                                            leftIcon={<AddIcon />}
                                            variant="outline"
                                            onClick={handleAddOption}
                                            colorScheme="blue">Option</Button>
                                    </Flex>
                                </Stack>}

                            </Flex>

                        </ModalBody>
                        <ModalFooter>
                            <Button onClick={onClose}>Cancel</Button>
                            <Button type="submit" colorScheme='blue' mr={3} >
                                Save
                            </Button>
                        </ModalFooter>
                    </form>
                </ModalContent>
            </Modal>

            <Modal
                isOpen={isEditOpen}
                onClose={onEditClose}
                size={question.modal}
            >
                <ModalOverlay />
                <ModalContent w={question.modal === 'xxl' ? '800px' : null}>
                    <form method='post' onSubmit={fetchUpdate}>
                        <ModalHeader>Edit Question</ModalHeader>
                        <ModalCloseButton />
                        <ModalBody pb={6}>
                            <Flex>
                                <Stack w={question.modal === 'xxl' ? '50%' : '100%'}>
                                    <InputField
                                        label="Title"
                                        required
                                        name="title"
                                        value={question.title}
                                        onChange={handleInputTextChange}
                                    />
                                    <Stack direction="row" spacing="8">
                                        <FormControl isRequired>
                                            <FormLabel>Type</FormLabel>
                                            <RadioGroup value={question.type} onChange={(value) => setQuestion({ ...question, type: value })} >
                                                <Stack direction='row'>
                                                    <Radio value='0'>Feedback</Radio>
                                                    <Radio value='1'>Complaint</Radio>
                                                </Stack>
                                            </RadioGroup>
                                        </FormControl>
                                        <FormControl isRequired>
                                            <FormLabel htmlFor="publish-toggle">Publish</FormLabel>
                                            <Switch
                                                id="publish-toggle"
                                                isChecked={isPublish}
                                                value={isPublish}
                                                onChange={(e) => setIsPublish(v => !isPublish)}
                                            /> {isPublish ? ' Yes' : ' No'}
                                        </FormControl>
                                    </Stack>
                                    <InputField
                                        label="Description"
                                        required
                                        name="subtext"
                                        value={question.subtext}
                                        onChange={handleInputTextChange}
                                    />
                                    <FormControl mt="10px" isRequired>
                                        <FormLabel>Input Type</FormLabel>
                                        <Dropdown placeholder='Select option' value={question.inputType} onChange={handleInputTypeSelector} >
                                            {survey.inputType.map((type, index) => {
                                                return <option key={index} name={type.name} value={type.id}>{type.typeName}</option>
                                            })}
                                        </Dropdown>
                                    </FormControl>
                                </Stack>
                                {question.modal === 'xxl' && <Stack w="50%" ml="20px">
                                    <Text>Option Choices</Text>
                                    {question.options.map((option, index) =>
                                        <Flex key={index}>
                                            <Input type="text" w="60%" readOnly value={option} />
                                            <Button
                                                ml="10px"
                                                leftIcon={<DeleteIcon />}
                                                variant="outline"
                                                onClick={() => handleDeleteOption(index)}
                                                colorScheme="red">Delete</Button>
                                        </Flex>
                                    )}
                                    <Flex>
                                        <Input type="text" w="60%" value={option} onChange={handleOptionTextChange} />
                                        <Button
                                            ml="10px"
                                            leftIcon={<AddIcon />}
                                            variant="outline"
                                            onClick={handleAddOption}
                                            colorScheme="blue">Option</Button>
                                    </Flex>
                                </Stack>}

                            </Flex>

                        </ModalBody>
                        <ModalFooter>
                            <Button onClick={onEditClose}>Cancel</Button>
                            <Button type="submit" colorScheme='blue' mr={3} >
                                Save
                            </Button>
                        </ModalFooter>
                    </form>
                </ModalContent>
            </Modal>

            <Modal isOpen={isDeleteOpen} onClose={onDeleteClose}>
                <ModalOverlay />
                <ModalContent>
                    <ModalHeader>Delete Question</ModalHeader>
                    <ModalCloseButton />
                    <ModalBody>
                        <Stack>
                            <Stack spacing={0}>
                                <Text fontSize="sm" color="gray.400" fontWeight="normal">
                                    Id
                                </Text>
                                <Text
                                    fontSize="md"
                                    fontWeight="bold">
                                    {questionDelete.id}
                                </Text>
                            </Stack>
                            <Stack spacing={0}>
                                <Text fontSize="sm" color="gray.400" fontWeight="normal">
                                    Title
                                </Text>
                                <Text
                                    fontSize="md"
                                    fontWeight="bold">
                                    {questionDelete.title}
                                </Text>
                            </Stack>
                        </Stack>
                    </ModalBody>

                    <ModalFooter>
                        <Button
                            mr={3} onClick={onDeleteClose}>
                            Cancel
                        </Button>
                        <Button
                            variant='solid'
                            colorScheme="red"
                            onClick={() => fetchDelete()}>
                            Delete
                        </Button>
                    </ModalFooter>
                </ModalContent>
            </Modal>
        </Box >
    );
}

export default SurveyPage;