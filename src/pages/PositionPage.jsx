import React from "react";
import {
    Box,
    Flex
} from '@chakra-ui/react';
import PositionTable from "../components/organism/tables/PositionTable";

const PositionPage = () => {
    return <>
        <Box w='100%'>
            <Flex direction="column" pt={{ base: "120px", md: "24px" }} px="8px">
                <PositionTable />
            </Flex>
        </Box>
    </>;
}

export default PositionPage;