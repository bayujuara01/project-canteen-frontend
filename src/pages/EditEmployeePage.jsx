import React, { useCallback, useEffect, useMemo, useRef, useState } from "react";
import {
    Box,
    Button,
    FormControl,
    FormLabel,
    Radio,
    RadioGroup,
    Flex,
    Input,
    InputGroup,
    InputLeftElement,
    InputRightElement,
    Icon,
    IconButton,
    Text,
    Stack,
    Grid,
    Image,
    useToast,
} from "@chakra-ui/react";
import Select from 'react-select';
import { FiFile, FiMoreHorizontal } from "react-icons/fi";
import { useSelector, useDispatch } from "react-redux";
import { requestAllPositionWithoutPaging } from '../features/position/positionAPI';
import { requestAllAlergicWithoutPaging } from '../features/alergic/alergicAPI';
import { requestAllRole } from '../features/role/roleAPI';
import { updateUserById, requestCheckUsersUniqueField } from "../features/user/userAPI";
import {
    requestPositionAction,
    getAllPositionWithoutPagingAction,
    requestErrorAction as requestErrorPositionAction
} from '../features/position/positionSlice';
import {
    requestAlergicAction,
    getAllAlergicWithoutPagingAction,
    requestErrorAction as requestErrorAlergicAction
} from '../features/alergic/alergicSlice';
import {
    requestRoleAction,
    getAllRoleAction,
    requestErrorAction as requestErrorRolesAction
} from '../features/role/roleSlice';
import useForm, { ValidationType } from "../utils/useForm";
import {
    requestUserAction,
    getByIdUserAction,
    requestErrorAction as requestErrorUserDetailAction
} from '../features/user/userSlice';
import { requestUserById } from '../features/user/userAPI';
import { IMAGE_PATH } from '../app/constant';
import { InputField } from "../components/molecules";
import profileImage from '../assets/images/profile.png'

const EditEmployeePage = () => {
    const dispatch = useDispatch();
    const toast = useToast();
    const { position, alergic, auth, user, role } = useSelector(state => state);
    const [loading, setLoading] = useState(false);

    const fileRef = useRef(null);

    const formUserEditInput = useMemo(() => ([
        {
            name: 'name',
            defaultValue: user.selected.name,
            validation: {
                min: 0,
                max: 60,
                isRequired: true,
                pattern: /^[\w ]+$/,
                errorMessage: 'please input without number and symbols'
            }
        },
        {
            name: 'nik',
            defaultValue: user.selected.nik,
            validation: {
                min: 6,
                max: 20,
                isRequired: true,
                pattern: /^[0-9]*$/,
                errorMessage: 'please input only number'
            }
        },
        {
            name: 'username',
            defaultValue: user.selected.username,
            validation: {
                min: 6,
                max: 60,
                isRequired: true,
            }
        }, {
            name: 'email',
            defaultValue: user.selected.email,
            validation: {
                isRequired: true,
                pattern: /^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$/,
                errorMessage: 'Please provide valid email'
            }
        },
        {
            name: 'phone',
            defaultValue: user.selected.phoneNumber,
            validation: {
                isRequired: true,
                pattern: /^(0|\+62)\d{5,18}$/,
                errorMessage: 'Starts with 0/+62 and between 5 - 18 characters'
            }
        },
        {
            name: 'position',
            type: ValidationType.SELECT,
            defaultValue: { value: user.selected.positionId, label: user.selected.position },
            validation: {
                isRequired: true
            }
        },
        {
            name: 'role',
            type: ValidationType.SELECT,
            defaultValue: { value: user.selected.roleId, label: user.selected.roleName },
            validation: {
                isRequired: true
            }
        },
        {
            name: 'alergic',
            type: ValidationType.MULTI_SELECT,
            defaultValue: [...user.selected.alergics.map(alg => ({ value: alg.id, label: alg.name }))],
            validation: {
                isRequired: true
            }
        },
        {
            name: 'gender',
            type: ValidationType.RADIO,
            defaultValue: user.selected.gender,
            validation: {
                isRequired: true
            }
        },
        {
            name: 'profile',
            type: ValidationType.SINGLE_IMAGE,
        },
    ]), [user.selected]);

    const { formState, handler, errorHandler, checkHandler } = useForm(formUserEditInput);

    const onImageFileChangeHandler = (events) => {
        if (events.target.files) {
            handler("profile", {
                image: URL.createObjectURL(events.target.files[0]),
                file: events.target.files[0]
            })
        }
    }

    const getPosition = useCallback(async () => {
        requestPositionAction();
        try {
            const response = await requestAllPositionWithoutPaging();
            dispatch(getAllPositionWithoutPagingAction(response.data.data));
        } catch (err) {
            dispatch(requestErrorPositionAction());
        }
    }, [dispatch]);

    const getRole = useCallback(async () => {
        requestRoleAction();
        try {
            const response = await requestAllRole();
            dispatch(getAllRoleAction(response.data.data));
        } catch (err) {
            dispatch(requestErrorRolesAction());
        }
    }, [dispatch]);

    const getAlergic = useCallback(async () => {
        requestAlergicAction();
        try {
            const response = await requestAllAlergicWithoutPaging();
            dispatch(getAllAlergicWithoutPagingAction(response.data.data));
        } catch (err) {
            dispatch(requestErrorAlergicAction());
        }
    }, [dispatch]);

    const getDetailUser = useCallback(async () => {
        dispatch(requestUserAction());
        try {
            const response = await requestUserById(user.selected.id)
            dispatch(getByIdUserAction(response.data.data));
        } catch (err) {
            toast({
                title: 'Error fetching data, please try again',
                status: 'error',
                position: 'top'
            });
            dispatch(requestErrorUserDetailAction());
        }
    }, [dispatch, user.selected.id, toast]);

    const checkUniqueField = async () => {
        let valid = true;
        try {
            const response = await requestCheckUsersUniqueField({
                username: formState["username"].value,
                email: formState["email"].value,
                phone: formState["phone"].value,
                nik: formState["nik"].value,
                userId: user.selected.id
            });

            const fieldData = response.data.data;
            for (let key in fieldData) {
                if (fieldData[key]) {
                    errorHandler(key, `${key} already in use, take another`);
                    valid = false;
                }
            }
        } catch (err) {
            valid = false;
        }

        return valid;
    }

    const onSubmitHandler = async (events) => {
        let uniqueValid = false;
        events.preventDefault();

        if (checkHandler()) {
            uniqueValid = await checkUniqueField();
        }

        if (formState.isFormValid && uniqueValid) {
            setLoading(true);
            try {
                const response = await updateUserById(user.selected.id, {
                    name: formState["name"].value,
                    username: formState["username"].value,
                    email: formState["email"].value,
                    phone: formState["phone"].value,
                    position: formState["position"].value.value,
                    gender: formState["gender"].value,
                    role: formState["role"].value.value,
                    nik: formState["nik"].value,
                    image: formState["profile"].value ? formState["profile"].value.file : '',
                    alergicIds: formState["alergic"].value ? [
                        formState["alergic"].value.value
                    ] : [],
                    idAdmin: auth.user.sub
                });
                toast({
                    title: response.data.message,
                    status: 'success',
                    position: 'top'
                });
            } catch (err) {
                if (err.response) {
                    toast({
                        title: err.response.data.error,
                        description: err.response.data.message,
                        status: 'error',
                        position: 'top'
                    });
                } else {
                    toast({
                        title: "Internal site error",
                        description: `Please try again later`,
                        status: 'error',
                        position: 'top'
                    });
                }
            } finally {
                setLoading(false);
            }
        } else {
            toast({
                title: "Inputs it is not correct",
                description: "Please check the field, and enter with correct value",
                status: 'error',
                position: 'top'
            });
        }
    };

    useEffect(() => {
        getPosition();
        getRole();
        getAlergic();
        getDetailUser();

    }, [getPosition, getAlergic, getRole, getDetailUser]);

    return (
        <Box>
            <Box mb={4} mt={4} backgroundColor="white">
                <Grid>
                    <Stack spacing={2} ml={3}>
                        <Flex justifyContent={{ base: 'center', md: 'flex-start' }}>
                            <Image
                                height="120px"
                                width="120px"
                                borderRadius={100}
                                objectFit="cover"
                                src={formState["profile"].value ? formState["profile"].value.image : user.selected.imageUrl ? `${IMAGE_PATH}/${user.selected.imageUrl}` : profileImage}
                            />
                        </Flex>
                        <Grid templateColumns={{ base: "repeat(1,1fr)", md: "repeat(2,1fr)", lg: "repeat(3,1fr)" }} gap={6}>
                            <Stack spacing={2}>
                                {/* <InputGroup> */}
                                <InputGroup display="none">
                                    <InputLeftElement
                                        pointerEvents="none"
                                        children={<Icon as={FiFile} />}
                                    />
                                    <InputRightElement
                                        children={<IconButton
                                            icon={<FiMoreHorizontal />}
                                            onClick={() => { fileRef.current.click() }}
                                        />} />
                                    <input
                                        ref={fileRef}
                                        onChange={(events) => onImageFileChangeHandler(events)}
                                        type='file'
                                        style={{ display: 'none' }}
                                        accept="image/png, image/jpg, image/jpeg" />
                                    <Input
                                        defaultValue={formState["profile"].value ? formState["profile"].value.file.name : ''}
                                        placeholder={"Browser image file ..."}
                                        onClick={() => { fileRef.current.click() }}
                                    />
                                </InputGroup>
                            </Stack>
                        </Grid>
                    </Stack>
                </Grid>
            </Box>
            <Box mb={4} backgroundColor="white">
                <Grid columnGap={0} templateColumns={{ base: "repeat(1,1fr)", md: "repeat(2,1fr)", lg: "repeat(3,1fr)" }}>
                    <Stack>
                        <Box px={3} py={5}
                        >
                            <Stack spacing={2}>
                                <InputField
                                    name="nik"
                                    label="NIK"
                                    value={formState["nik"].value}
                                    onChange={(e) => handler(e.target.name, e.target.value)}
                                    invalid={formState["nik"].hasError}
                                    error={formState["nik"].error}
                                    required
                                />

                                <InputField
                                    name="email"
                                    label="Email"
                                    value={formState["email"].value}
                                    onChange={(e) => handler(e.target.name, e.target.value)}
                                    invalid={formState["email"].hasError}
                                    error={formState["email"].error}
                                    required
                                />

                                {/* <InputField
                                    name="name"
                                    label="Name"
                                    value={formState["name"].value}
                                    onChange={(e) => handler(e.target.name, e.target.value)}
                                    invalid={formState["name"].hasError}
                                    error={formState["name"].error}
                                    required
                                /> */}

                                {/* <InputField
                                    name="username"
                                    label="Username"
                                    value={formState["username"].value}
                                    onChange={(e) => handler(e.target.name, e.target.value)}
                                    invalid={formState["username"].hasError}
                                    error={formState["username"].error}
                                    required
                                /> */}

                                <Stack spacing={2}>
                                    <FormControl isRequired>
                                        <FormLabel>Position</FormLabel>
                                        <Select
                                            id="react-select-create-employee-pos"
                                            name="position"
                                            styles={{ control: base => ({ ...base, borderColor: formState["position"].hasError ? 'red' : '' }) }}
                                            value={{ ...formState["position"].value }}
                                            menuPlacement="auto"
                                            options={position.positions.map(
                                                pos => ({ value: pos.id, label: pos.position }))}
                                            onChange={(e) => {
                                                handler("position", e);
                                                const lab = e.label.toLowerCase();
                                                if (lab.includes("office") && lab.includes("girl")) {
                                                    const caretaker = role.roles.find(rol => rol.name.toLowerCase() === 'caretaker');

                                                    if (typeof caretaker != 'undefined') {
                                                        handler("role", { value: caretaker.id, label: caretaker.name })
                                                    }
                                                }
                                            }} />
                                        {formState["position"].hasError && <Text fontSize="12px" color="red" mt={2}>{formState["position"].error}</Text>}
                                    </FormControl>
                                </Stack>

                                {/* <InputField
                                    name="email"
                                    label="Email"
                                    value={formState["email"].value}
                                    onChange={(e) => handler(e.target.name, e.target.value)}
                                    invalid={formState["email"].hasError}
                                    error={formState["email"].error}
                                    required
                                /> */}
                            </Stack>
                        </Box>
                    </Stack>
                    <Box>
                        <Box px={3} py={5}>
                            <Stack spacing={2}>
                                <InputField
                                    name="name"
                                    label="Name"
                                    value={formState["name"].value}
                                    onChange={(e) => handler(e.target.name, e.target.value)}
                                    invalid={formState["name"].hasError}
                                    error={formState["name"].error}
                                    required
                                />
                                {/* <InputField
                                    name="phone"
                                    label="Phone Number"
                                    value={formState["phone"].value}
                                    onChange={(e) => handler(e.target.name, e.target.value)}
                                    invalid={formState["phone"].hasError}
                                    error={formState["phone"].error}
                                    required
                                /> */}

                                <Stack spacing={2}>
                                    <InputField
                                        name="phone"
                                        label="Phone Number"
                                        value={formState["phone"].value}
                                        onChange={(e) => handler(e.target.name, e.target.value)}
                                        invalid={formState["phone"].hasError}
                                        error={formState["phone"].error}
                                        required
                                    />
                                </Stack>

                                {/* <Stack spacing={2}>
                                    <FormControl isRequired>
                                        <FormLabel>Gender</FormLabel>
                                        <RadioGroup
                                            value={formState['gender'].value}
                                            onChange={value => handler("gender", value)} >
                                            <Stack direction='row' py={2}>
                                                <Radio value='0'>Male</Radio>
                                                <Radio value='1'>Female</Radio>
                                            </Stack>
                                        </RadioGroup>
                                        {formState["gender"].hasError && <Text fontSize="12px" color="red" mt={2}>{formState["gender"].error}</Text>}
                                    </FormControl>
                                </Stack> */}

                                <Stack spacing={2}>
                                    <FormControl isRequired>
                                        <FormLabel>Role</FormLabel>
                                        <Select
                                            name="role"
                                            styles={{ control: base => ({ ...base, borderColor: formState["role"].hasError ? 'red' : '' }) }}
                                            value={{ ...formState["role"].value }}
                                            options={role.roles.map(
                                                rol => ({ value: rol.id, label: rol.name }))}
                                            onChange={(e) => handler("role", e)} />
                                        {formState["role"].hasError && <Text fontSize="12px" color="red" mt={2}>{formState["role"].error}</Text>}
                                    </FormControl>
                                </Stack>
                                {/* <Stack spacing={2}>
                                    <FormControl>
                                        <FormLabel>Allergic</FormLabel>
                                        <Select isMulti={true}
                                            value={[...formState['alergic'].value]}
                                            options={alergic.alergics.map(alg => ({
                                                value: alg.id, label: alg.name[0].toUpperCase() + alg.name.substring(1).toLowerCase()
                                            }))}
                                            onChange={(e) => handler("alergic", e)} />
                                    </FormControl>
                                </Stack> */}
                            </Stack>
                        </Box>
                    </Box>
                    <Box display="block">
                        <Box px={3} py={5}>
                            <Stack spacing={2}>
                                <InputField
                                    name="username"
                                    label="Username"
                                    value={formState["username"].value}
                                    onChange={(e) => handler(e.target.name, e.target.value)}
                                    invalid={formState["username"].hasError}
                                    error={formState["username"].error}
                                    required
                                />

                                <FormControl isRequired>
                                    <FormLabel>Gender</FormLabel>
                                    <RadioGroup
                                        value={formState['gender'].value}
                                        onChange={value => handler("gender", value)} >
                                        <Stack direction='row' py={2}>
                                            <Radio value='0'>Male</Radio>
                                            <Radio value='1'>Female</Radio>
                                        </Stack>
                                    </RadioGroup>
                                    {formState["gender"].hasError && <Text fontSize="12px" color="red" mt={2}>{formState["gender"].error}</Text>}
                                </FormControl>

                                <Stack spacing={2}>
                                    <FormControl>
                                        <FormLabel>Allergic</FormLabel>
                                        <Select isMulti={false}
                                            menuPlacement="auto"
                                            isClearable={true}
                                            placeholder="Select allergics..."
                                            value={formState['alergic'].value}
                                            options={alergic.alergics.map(alg => ({
                                                value: alg.id, label: alg.name[0].toUpperCase() + alg.name.substring(1).toLowerCase()
                                            }))}
                                            onChange={(e) => { handler("alergic", e); }} />
                                    </FormControl>
                                </Stack>

                                <Stack spacing={3}>
                                    <Box h={6} />
                                    <Button onClick={onSubmitHandler} isLoading={loading} isDisabled={loading || !formState.isFormValid}>Submit</Button>
                                </Stack>
                            </Stack>
                        </Box>
                    </Box>
                </Grid>
            </Box>
        </Box>
    );
}

export default EditEmployeePage;