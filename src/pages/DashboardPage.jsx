import {
    Box,
    Flex,
    SimpleGrid,
    useToast
} from "@chakra-ui/react";
import React, { useCallback, useEffect } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import {
    Card,
    CardBody
} from "../components/atomic";
import { StatCard } from '../components/molecules';
import {
    getStatDashboardAction,
    requestDashboardAction,
    requestErrorDashboardAction
} from '../features/dashboard/dashboardSlice';
import {
    requestStat
} from '../features/dashboard/dashboardAPI';
import DashboardTakeFoodTable from "../components/organism/tables/DashboardTakeFoodTable";

const DashboardPage = ({
    isLoading,
    data,
    requestDashboard,
    requestErrorDashboard,
    getStatDashboard
}) => {
    const toast = useToast();
    const now = new Date();
    const fetchStatDashboard = useCallback(async () => {
        requestDashboard();
        try {
            const response = await requestStat();
            getStatDashboard(response.data.data);
        } catch (err) {
            requestErrorDashboard();
            let message = 'Internal Server Error'
            if (err.response) {
                message = err.response.message ? err.response.message : err.response.error
            }

            toast({
                title: message,
                position: 'top',
                variant: 'solid',
                isClosable: true,
                duration: 1500,
                status: 'error'
            });
        }
    }, [getStatDashboard, requestDashboard, requestErrorDashboard, toast]);

    useEffect(() => {
        fetchStatDashboard();
    }, [fetchStatDashboard]);

    return <Box h="100%">
        <Flex flexDirection="column">
            <SimpleGrid columns={{ sm: 1, md: 2, xl: 4 }} gap="4" my="16px">
                <Card>
                    <CardBody>
                        <StatCard
                            title="Employee"
                            label={data.countEmployee} />
                    </CardBody>
                </Card>
                <Card>
                    <CardBody>
                        <StatCard
                            title="Caretaker"
                            label={data.countCaretaker} />
                    </CardBody>
                </Card>
                <Card>
                    <CardBody>
                        <StatCard
                            title="Food Taken"
                            label={data.countTakenFood} />
                    </CardBody>
                </Card>
                <Card>
                    <CardBody>
                        <StatCard
                            title={now.toLocaleDateString("en-US", { weekday: 'long' })}
                            label={now.toLocaleDateString("en-US", { year: 'numeric', month: 'long', day: 'numeric' })} />
                    </CardBody>
                </Card>
            </SimpleGrid>
            <DashboardTakeFoodTable />
        </Flex>
    </Box>
}

const mapStateToProps = (state) => {
    const { data, isLoading } = state.dashboard;
    return {
        data,
        isLoading
    }
};

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        requestDashboard: requestDashboardAction,
        requestErrorDashboard: requestErrorDashboardAction,
        getStatDashboard: getStatDashboardAction
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(DashboardPage);