import React, { useCallback, useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { useDispatch } from "react-redux";
import {
    Report,
    requestAllFeedbackReport,
    requestAllFeedbackReportText
} from '../features/report/reportAPI';
import {
    requestReportComplaintAction,
    requestErrorAction,
    getAllReportComplaintAction,
    getAllReportTextComplaintAction
} from '../features/report/reportComplaintSlice';

import {
    Box,
    Flex,
    Text,
    Stack,
    SimpleGrid,
    Input,
    InputGroup,
    InputLeftAddon,
    useToast
} from '@chakra-ui/react';
import {
    Card,
    CardHeader,
    CardBody,
    PieChart,
    BarChart,
    Divider,
} from "../components/atomic";

import { Chart as ChartJS, ArcElement, Tooltip, Legend } from 'chart.js';

ChartJS.register(ArcElement, Tooltip, Legend);

const ReportComplaintPage = () => {
    const reportComplaint = useSelector(state => state.reportComplaint);
    const dispatch = useDispatch();
    const toast = useToast();

    const todayString = new Date().toISOString().slice(0, 10);
    const [date, setDate] = useState(todayString);


    const fetchQuestionReportOptionData = useCallback(async () => {
        dispatch(requestReportComplaintAction());
        try {
            const response = await requestAllFeedbackReport({ type: Report.TYPE_COMPLAINT, date });
            dispatch(getAllReportComplaintAction(response.data.data));
        } catch (err) {
            let message = 'Internal Server Error'
            if (err.response) {
                message = err.response.message ? err.response.message : err.response.error
            }

            toast({
                title: message,
                position: 'top',
                variant: 'solid',
                isClosable: true,
                duration: 1500,
                status: 'error'
            });

            dispatch(requestErrorAction());
        }
    }, [dispatch, date, toast]);

    const fetchQuestionReportTextData = useCallback(async () => {
        dispatch(requestReportComplaintAction());
        try {
            const response = await requestAllFeedbackReportText({ type: Report.TYPE_COMPLAINT, date });
            dispatch(getAllReportTextComplaintAction(response.data.data));
        } catch (err) {
            dispatch(requestErrorAction());
        }
    }, [dispatch, date]);

    useEffect(() => {
        fetchQuestionReportOptionData();
        fetchQuestionReportTextData();
    }, [fetchQuestionReportOptionData, fetchQuestionReportTextData]);

    const onDateChange = (events) => {
        if (events.target.value) {
            setDate(prevState => {
                return events.target.value
            })
        }
    }

    return <>
        <Box w='100%' mb="4">
            <Flex my="4" justifyContent="end">
                <Box w={{ md: '30vw', lg: '20vw' }}>
                    <InputGroup>
                        <InputLeftAddon children="Date" />
                        <Input type="date" onChange={onDateChange} value={date} max={todayString} />
                    </InputGroup>
                </Box>

            </Flex>
            <SimpleGrid columns={{ base: 1, md: 1, lg: 2 }}
                rowGap="4"
                columnGap="4">
                {reportComplaint.data.map((value, index) => {
                    return <Box key={index} >
                        <Card h={{ md: '50vh', lg: '50vh' }}>
                            <CardHeader pb="4">
                                <Stack>
                                    <Text fontSize="lg" fontWeight="bold">
                                        {value.title}
                                    </Text>
                                    <Text fontSize="sm">{value.subtext}</Text>
                                    <Divider />
                                </Stack>
                            </CardHeader>
                            <CardBody>
                                {(value.options && (
                                    value.inputType.typeName.includes('radio') ||
                                    value.inputType.typeName.includes('dropdown')
                                )) &&
                                    <PieChart
                                        id={value.id}
                                        data={value.options.map(option => option.count)}
                                        labels={value.options.map(option => option.name)}
                                    />
                                }

                                {(value.options && (
                                    value.inputType.typeName.includes('checkbox') ||
                                    value.inputType.typeName.includes('chip')
                                )) &&
                                    <BarChart
                                        id={value.id}
                                        data={value.options.map(option => option.count)}
                                        labels={value.options.map(option => option.name)}
                                    />
                                }
                            </CardBody>
                        </Card>
                    </Box>
                })}
                {reportComplaint.dataText.map((value, index) => {
                    return (
                        <Box key={index}>
                            <Card h={{ md: '50vh', lg: '50vh' }} >
                                <CardHeader pb="4">
                                    <Stack>
                                        <Text fontSize="lg" fontWeight="bold">
                                            {value.title}
                                        </Text>
                                        <Text fontSize="sm">{value.subtext}</Text>
                                        <Divider />
                                    </Stack>
                                </CardHeader>
                                <CardBody overflowX="auto">
                                    <Stack>
                                        {value.answer.map((item, index) => {
                                            return (
                                                <Text key={index}>{index + 1}. {item.text}</Text>
                                            );
                                        })}
                                    </Stack>
                                </CardBody>
                            </Card>
                        </Box>
                    );
                })}
            </SimpleGrid>
        </Box >
    </>
}

export default ReportComplaintPage;