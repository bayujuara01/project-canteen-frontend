import {
    Box,
    Flex,
    Text,
    Image,
    Stack,
    Button,
    useToast,
    Tag
} from '@chakra-ui/react'
import React, { useCallback, useEffect } from "react";
import { Card, CardBody, CardHeader, TextInfo } from '../components/atomic';
import profileImage from '../assets/images/profile.png'
import { useSelector, useDispatch } from 'react-redux';
import {
    requestUserAction,
    getByIdUserAction,
    requestErrorAction
} from '../features/user/userSlice';
import { requestUserById } from '../features/user/userAPI';
import { IMAGE_PATH } from '../app/constant';
import { useNavigate } from 'react-router-dom';

const DetailEmployeePage = () => {
    const user = useSelector(state => state.user);
    const navigate = useNavigate();
    const dispatch = useDispatch();
    const toast = useToast();

    const getDetailUser = useCallback(async () => {
        dispatch(requestUserAction());
        try {
            const response = await requestUserById(user.selected.id)
            dispatch(getByIdUserAction(response.data.data));
        } catch (err) {
            toast({
                title: 'Error fetching data, please try again',
                status: 'error',
                position: 'top'
            });
            dispatch(requestErrorAction());
        }
    }, [dispatch, user.selected.id, toast]);


    useEffect(() => {
        getDetailUser();
    }, [getDetailUser]);

    return <>
        <Box w='100%'>
            <Card >
                <CardHeader p="6px 0px 22px 0px">
                    <CardBody>
                        {user.isLoading && <Flex alignItems="center" justifyContent="center">Loading</Flex>}
                        <Flex flexDirection="column" w="full">
                            <Flex alignItems="start" justifyContent="space-between">
                                <Flex>
                                    <Image alt={`${user.selected.name}'s photo`}
                                        src={profileImage}
                                        objectFit="cover" boxSize="180px" borderRadius="5px" />
                                    {/* <Image alt={`${user.selected.name}'s photo`}
                                        src={user.selected.imageUrl ? `${IMAGE_PATH}/${user.selected.imageUrl}` : profileImage}
                                        objectFit="cover" boxSize="180px" borderRadius="5px" /> */}
                                    <Stack ml="24px">
                                        <Text fontSize="36px" fontFamily="Open Sans">{user.selected.name}</Text>
                                        <Text fontSize="18px">{user.selected.position}</Text>
                                        <Text fontSize="18px">NIK {user.selected.nik}</Text>
                                    </Stack>
                                </Flex>
                                <Button justifySelf="start" onClick={() => navigate("/employee/edit")}>Edit Profile</Button>
                            </Flex>
                            <Text fontSize="lg" fontWeight="bold" mt="40px">Account Information</Text>
                            <Flex>
                                <Flex direction="column" mr="10vw">
                                    <TextInfo label="Username">{user.selected.username}</TextInfo>
                                    <TextInfo label="Email">{user.selected.email}</TextInfo>
                                    <TextInfo label="Phone Number">{user.selected.phoneNumber}</TextInfo>
                                </Flex>
                                <Flex direction="column">
                                    <TextInfo label="Gender">{user.selected.gender == 0 ? 'Male' : 'Female'}</TextInfo>
                                    <TextInfo
                                        label="Allergic"
                                        element>
                                        <Flex>
                                            {user.selected.alergics.map(alergic => {
                                                return <Tag mx="2px" colorScheme="blue" variant="solid">{alergic.name}</Tag>
                                            })}
                                        </Flex>
                                    </TextInfo>
                                </Flex>
                            </Flex>
                        </Flex>
                    </CardBody>
                </CardHeader>
            </Card>
        </Box>
    </>
}

export default DetailEmployeePage;