import {
    Box,
    useColorModeValue,
    Text,
    Flex,
    IconButton,
    Menu,
    MenuButton,
    MenuList,
    MenuItem,
    Drawer,
    DrawerContent,
    DrawerOverlay,
    Avatar,
    Button,
    Image,
    Divider,
    Breadcrumb,
    BreadcrumbItem,
    BreadcrumbLink,
    useDisclosure
} from '@chakra-ui/react';
import {
    FiHome,
    FiUser,
    FiMenu
} from 'react-icons/fi';
import {
    RiSurveyLine,
    RiFeedbackFill,
    RiFeedbackLine,
    RiTableLine,
    RiTicket2Line,
    RiQrCodeLine
} from 'react-icons/ri';
import {
    FaUserTie,
    FaAllergies,
} from 'react-icons/fa'
import { Link, Outlet } from 'react-router-dom';
import { NavItem } from '../components/atomic';
import { signOut } from '../features/auth/authSlice';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate, useLocation } from 'react-router-dom';
import { IMAGE_PATH } from '../app/constant';
import { setSelectedIdAction } from '../features/user/userSlice';
import profilePlaceholder from '../assets/images/profile.png'
import CantnLogo from '../assets/images/cantn_logo_tr.png';

const HomePage = () => {
    const dispatch = useDispatch();
    const navigate = useNavigate();
    const auth = useSelector(state => state.auth);
    const location = useLocation();

    const path = location.pathname.replace(/(^\/+|\/+$)/g, '');
    const paths = path.split('/');

    const sidebar = useDisclosure();

    const onSignOutHandler = () => {
        dispatch(signOut());
        navigate("/");
    }

    const onProfileHandler = () => {
        dispatch(setSelectedIdAction(auth.user.sub));
        navigate("/employee/edit")
    }


    const SidebarContent = ({ children, ...props }) => <>
        <Box
            pos="fixed"
            top="0"
            left="0"
            zIndex="sticky"
            w="60"
            h="full"
            transition=".25s ease"
            bg={useColorModeValue('white', 'gray.900')}
            borderRight="1px"
            borderRightColor={useColorModeValue('gray.200', 'gray.700')}
            {...props}>
            <Flex h="20" alignItems="center" mx="8" justifyContent="center">
                <Image src={CantnLogo} h="8vh" />
            </Flex>
            {children}
        </Box>
    </>;

    return <>
        <Box
            bg={useColorModeValue('white')}
            overflow="hidden"
            height='100vh'
        >
            <SidebarContent display={{ base: 'none', md: 'unset' }} overflowY="auto" >
                <NavItem to="/" icon={FiHome}>Dashboard</NavItem>
                <NavItem to="/employee" icon={FiUser}>Employee</NavItem>
                <NavItem to="/qr" icon={RiQrCodeLine}>QR</NavItem>
                <NavItem to="/survey" icon={RiSurveyLine}>Survey</NavItem>
                <NavItem to="/report/feedback" icon={RiFeedbackLine}>Report Feedback</NavItem>
                <NavItem to="/report/complaint" icon={RiFeedbackFill}>Report Complaint</NavItem>
                <NavItem to="/report/takefood" icon={RiTableLine}>Report Take Food</NavItem>
                <Divider orientation="horizontal" />
                <NavItem to="/position" icon={FaUserTie}>Position</NavItem>
                <NavItem to="/alergic" icon={FaAllergies}>Allergic</NavItem>
                <NavItem to="/voucher" icon={RiTicket2Line}>Guest Voucher</NavItem>
            </SidebarContent>
            <Drawer
                isOpen={sidebar.isOpen}
                onClose={sidebar.onClose}
                placement="left"
            >
                <DrawerOverlay />
                <DrawerContent>
                    {/* <SidebarContent w="full" borderRight="none" /> */}
                </DrawerContent>
            </Drawer>
            <Flex
                ml={{ base: 0, md: 60 }}
                px={{ base: 4, md: 4 }}
                h="14"
                alignItems="center"
                bg={useColorModeValue('white', 'gray.900')}
                borderBottomWidth="1px"
                borderBottomColor={useColorModeValue('gray.200', 'gray.700')}
                justifyContent={{ base: 'space-between', md: 'space-between' }}
            >
                <IconButton
                    aria-label="Menu"
                    display={{ base: "inline-flex", md: "none" }}
                    onClick={sidebar.onOpen}
                    icon={<FiMenu />}
                    size="sm"
                />

                <Breadcrumb separator='-' display={{ base: 'none', md: 'inline-flex' }}>
                    <BreadcrumbItem fontSize="md" fontWeight="bold">
                        <BreadcrumbLink as={Link} to="/" >Dashboard</BreadcrumbLink>
                    </BreadcrumbItem>
                    {paths[0] && paths.map((p, index) => {
                        return (
                            <BreadcrumbItem key={index}>
                                <Text

                                    fontSize="md"
                                    fontWeight="bold">{`${p[0].toUpperCase()}${p.substring(1).toLowerCase()}`}</Text>
                            </BreadcrumbItem>
                        );
                    })}

                </Breadcrumb>
                <Flex alignItems={'center'}>
                    <Text mr="12px">{auth.user.name}</Text>
                    <Menu>
                        <MenuButton
                            as={Button}
                            rounded={'full'}
                            variant={'link'}
                            cursor={'pointer'}
                            minW={0}>
                            <Avatar
                                size={'sm'}
                                src={auth.user.image ? `${IMAGE_PATH}/${auth.user.image}` : profilePlaceholder}
                            />
                        </MenuButton>
                        <MenuList>
                            <MenuItem onClick={onProfileHandler}>Profile</MenuItem>
                            <MenuItem onClick={onSignOutHandler}>Sign Out</MenuItem>
                        </MenuList>
                    </Menu>
                </Flex>
            </Flex>
            <Box
                h="90vh"
                overflow="auto"
                ml={{ base: 0, md: 60 }}
                px={{ base: 4, md: 4 }}>
                <Outlet />
            </Box>
        </Box>

    </>
}

export default HomePage;