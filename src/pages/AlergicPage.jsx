import React from "react";
import {
    Box,
    Flex
} from '@chakra-ui/react';
import AlergicTable from "../components/organism/tables/AlergicTable";

const AlergicPage = () => {
    return <>
        <Box w='100%'>
            <Flex direction="column" pt={{ base: "120px", md: "24px" }} px="8px">
                <AlergicTable />
            </Flex>
        </Box>
    </>;
}

export default AlergicPage;