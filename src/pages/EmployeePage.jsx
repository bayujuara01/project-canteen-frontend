import {
    Box,
    Flex
} from '@chakra-ui/react';
import EmployeeTable from "../components/organism/tables/EmployeeTable";

const EmployeePage = () => {

    return <>
        <Box w='100%'>
            <Flex direction="column" pt={{ base: "120px", md: "16px" }} px="8px">
                <EmployeeTable />
            </Flex>
        </Box>
    </>
}

export default EmployeePage;