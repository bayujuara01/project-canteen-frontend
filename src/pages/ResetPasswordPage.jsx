import React, { useState } from "react";
import {
    Flex,
    Stack,
    Text,
    Image,
    useColorModeValue,
} from '@chakra-ui/react';
import { useSearchParams } from "react-router-dom";
import ChangePasswordForm from "../components/organism/forms/ChangePasswordForm";
import VerifyPasswordForm from '../components/organism/forms/VerifyPasswordForm'
import CantnLogo from '../assets/images/cantn_logo_tr.png';

const ResetPasswordPage = () => {
    const [params] = useSearchParams();
    const verify = params.get('verify');
    const [verified, setVerified] = useState(false);

    return (
        <Flex
            minH={'100vh'}
            align={'center'}
            justify={'center'}
            bg={useColorModeValue('gray.50', 'gray.800')}>
            <Stack spacing={8} mx={'auto'} maxW={'lg'} py={12} px={6}>
                {(!verified || verify) &&
                    <Stack align={'center'}>
                        <Image src={CantnLogo} h="10vh" />
                        <Text fontSize={'xl'} color={'gray.600'}>
                            {verify ? 'Reset' : 'Change'} Password 🔑
                        </Text>
                    </Stack>}

                {verify ? <VerifyPasswordForm verified={verified} setVerified={setVerified} /> : <ChangePasswordForm />}

            </Stack>
        </Flex>
    );
}

export default ResetPasswordPage;