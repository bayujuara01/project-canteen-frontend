import React, { useCallback, useEffect, useState, useRef } from 'react';
import {
    Box,
    Button,
    Flex,
    Stack,
    Image,
    Text,
    useToast,
    ModalOverlay,
    Modal,
    ModalContent,
    ModalHeader,
    ModalBody,
    ModalCloseButton,
    ModalFooter,
    useDisclosure
} from '@chakra-ui/react';
import QRCode from 'react-qr-code';
import { useReactToPrint } from 'react-to-print';
import StaticQRLogo from '../assets/images/qr_code.png';
import { CustomButton } from '../components/atomic'
import { InputField } from '../components/molecules';
import { useSelector, useDispatch } from 'react-redux';
import { createQrAction, getQrAction, requestErrorAction, requestQrAction } from '../features/qr/qrSlice';
import { createStaticQr, requestStaticQr } from '../features/qr/qrAPI';

import CantnLogo from '../assets/images/cantn_logo_tr.png'

const QRTemplatePrint = React.forwardRef(({ qr = '', createAt = null } = {}, ref) => {
    return (
        <Box
            ref={ref}
            w="100%"
        >
            <Flex
                alignItems="center"
                direction="column">
                <Image
                    mt="50px"
                    src={CantnLogo}
                    w="35%" />
                <Box mt="60px" mb="8">
                    <QRCode
                        title={qr ? qr : ''}
                        value={qr ? qr : ''}
                        size={400}
                    />
                </Box>
                <Text fontSize="2xl">{qr}</Text>
                {createAt && <Text fontSize="2xl">{new Date(createAt).toLocaleString()}</Text>}
                <Text mt="50px" fontSize="6xl" fontWeight="bold">SCAN HERE</Text>
            </Flex>
        </Box>
    );
})

const StaticQRPage = () => {

    const componentRef = useRef();
    const handlePrint = useReactToPrint({
        content: () => componentRef.current
    });

    const { auth, qr } = useSelector(state => state);
    const dispatch = useDispatch();
    const toast = useToast();

    const { isOpen: generateModalIsOpen,
        onClose: closeGenerateModal,
        onOpen: openGenerateModal } = useDisclosure();

    const [password, setPassword] = useState('');


    const handleGenerateStaticQR = async (e) => {
        dispatch(requestQrAction());
        try {
            const response = await createStaticQr({
                idUser: auth.user.sub,
                password
            });
            dispatch(createQrAction(response.data.data));
            toast({
                title: 'QR Code successfully changed',
                description: response.data.data.generatedCode,
                status: 'success',
                position: 'top'
            });
        } catch (err) {
            if (err.response && (err.response.status >= 400 && err.response.status < 500)) {
                toast({
                    title: 'Error when creating Static QR code',
                    description: err.response.data.message,
                    status: 'error',
                    position: 'top'
                });
            } else {
                toast({
                    title: 'Internal server error when creating Static QR code',
                    status: 'error',
                    position: 'top'
                });
            }
            dispatch(requestErrorAction());
        } finally {
            closeGenerateModal();
            setPassword(v => '');
        }
    }

    const fetchData = useCallback(async () => {
        dispatch(requestQrAction());
        try {
            const response = await requestStaticQr({ idUser: auth.user.sub });
            dispatch(getQrAction(response.data.data))
        } catch (err) {
            if (err.response && err.response.status === 404) {
                toast({
                    title: 'Static QR code not found',
                    description: 'Please generate a QR code via generate button',
                    status: 'warning',
                    position: 'top'
                })
            } else {
                toast({
                    title: 'Error when fetching Static QR code',
                    description: err.response.data.message,
                    status: 'error',
                    position: 'top'
                })
            }
            dispatch(requestErrorAction());
        }
    }, [auth.user.sub, toast, dispatch]);

    useEffect(() => {
        fetchData();
    }, [fetchData]);

    return (<Box w='100%'>
        <Box display="none">
            <QRTemplatePrint ref={componentRef} qr={qr.qr.generatedCode} createAt={qr.qr.createAt} />
        </Box>
        <Flex justifyContent="end" mt={4}>
            {qr.qr ? <Stack direction="row">
                <Button variant="solid" onClick={handlePrint} colorScheme="blue">Print</Button>
                <Button variant="solid" onClick={() => openGenerateModal()} colorScheme="blue">Generate</Button>
            </Stack> : <Stack direction="row">

                <Button variant="solid" onClick={() => openGenerateModal()} colorScheme="blue">Generate</Button>
            </Stack>}
        </Flex>

        <Flex justifyContent={{ base: "center", md: "start" }} mt="4">
            <Box
                w="300px"
                h="300px"
                border="5px"
                borderStyle="dashed"
                borderColor="gray.500">
                <Flex
                    w="100%"
                    h="100%"
                    direction="column"
                    justifyContent="center"
                    alignItems="center">

                    {qr.qr ? <>
                        <QRCode
                            title={qr.qr.generatedCode}
                            value={qr.qr.generatedCode}
                        />
                    </> : <>
                        <Image
                            w="150px"
                            h="150px"
                            opacity="0.5"
                            src={StaticQRLogo} />
                        <Text mt="3" opacity="0.8">Please generate QR code</Text>
                    </>}
                </Flex>
            </Box>
        </Flex>
        <Flex mt="4">
            <Text>Created at {new Date(qr.qr.createAt).toLocaleString()}</Text>
        </Flex>
        <Modal isOpen={generateModalIsOpen} onClose={closeGenerateModal}>
            <ModalOverlay />
            <ModalContent>
                <ModalHeader>Change QR Code</ModalHeader>
                <ModalCloseButton />
                <ModalBody>

                    <InputField label="Password"
                        name="name"
                        type="password"
                        value={password}
                        onChange={(e) => setPassword(v => e.target.value)}
                        placeholder='Password' />
                </ModalBody>
                <ModalFooter>
                    <CustomButton mr={3} onClick={closeGenerateModal}>
                        Cancel
                    </CustomButton>
                    <CustomButton
                        colorScheme="blue"
                        isLoading={qr.isLoading}
                        onClick={handleGenerateStaticQR}>Save</CustomButton>
                </ModalFooter>
            </ModalContent>
        </Modal>
    </Box>);
}

export default StaticQRPage;