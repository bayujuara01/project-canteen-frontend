import { Box } from '@chakra-ui/react';
import { NotFoundLayout } from '../components/templates';
import { useNavigate } from 'react-router';
const NotFoundPage = () => {
    const navigate = useNavigate();

    return (
        <Box w="100vw" h="100vh">
            <NotFoundLayout buttonHandler={() => navigate("/")} />
        </Box>
    );
}

export default NotFoundPage;