/* eslint-disable no-undef */

const SERVER_URL = 'http://127.0.0.1:3080/api';

Feature('Employee');

Scenario('Add Employee Should Error When Input Not Valid', { timeout: 60 }, ({ I }) => {
    I.login('admin_cantn', 'Admincantn12345');
    I.waitForText('Dashboard');
    I.see('Employee');
    I.amOnPage('/employee');
    I.click('Add');

    /**
     * 1. Field name test
     *  A Less than 6 [name must between 6 - 60 characters]
     *  B Not empty [name cannot be empty]
     *  C More than 60 [name must between 6 - 60 characters]
     *  D No Symbols [please input without number and symbols]
     *  E No Error
     */
    // 1.A
    I.fillField('Name', 'bayu');
    I.see('name must between 6 - 60 characters');
    I.clearField('Name');

    // 1.B
    I.fillField('Name', 'L');
    I.pressKey('Backspace');
    I.see('name cannot be empty');

    // 1.C
    I.fillField('Name', 'ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo')
    I.see('name must between 6 - 60 characters');
    I.clearField('Name');

    // 1.D
    I.fillField('Name', 'bayuju@*@');
    I.see('please input without number and symbols');
    I.clearField('Name');

    // 1.E
    I.fillField('Name', 'Bayu Seno Ariefyanto');
    I.dontSee('name must between 6 - 60 characters');
    I.dontSee('name cannot be empty');

    /**
     * 2. Field username test
     *  A Less than 6 [username must between 6 - 60 characters]
     *  B Not empty [username cannot be empty]
     *  C More than 60 [username must between 6 - 60 characters]
     *  D No Symbols [please input without symbols & space]
     *  E No error
     */
    // 2.A
    I.fillField('Username', 'bayu');
    I.see('username must between 6 - 60 characters');
    I.clearField('Username');

    // 2.B
    I.fillField('Username', 'L');
    I.pressKey('Backspace');
    I.see('username cannot be empty');

    // 2.C
    I.fillField('Username', 'ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo')
    I.see('username must between 6 - 60 characters');
    I.clearField('Username');

    // 2.D
    I.fillField('Username', 'bayujuar@01');
    I.see('please input without symbols & space');
    I.clearField('Username');
    I.fillField('Username', 'bayujuara 01');
    I.see('please input without symbols & space');
    I.clearField('Username');

    // 2.E
    I.fillField('Username', 'bayujuara01');
    I.dontSee('username must between 6 - 60 characters');
    I.dontSee('please input without sybols & space');
    I.dontSee('username cannot be empty');

    /**
     * 3. Field email test
     *  A Less than 6 [email must between 6 - 60 characters]
     *  B Not empty [email cannot be empty]
     *  C More than 60 [email must between 6 - 60 characters]
     *  D Invalid email [Please provide valid email]
     *  E No error
     */
    // 3.A
    I.fillField('Email', 'bayu');
    I.see('email must between 6 - 60 characters');
    I.clearField('Email');

    // 3.B
    I.fillField('Email', 'L');
    I.pressKey('Backspace');
    I.see('email cannot be empty');

    // 3.C
    I.fillField('Email', 'ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo')
    I.see('email must between 6 - 60 characters');
    I.clearField('Email');

    // 3.D
    I.fillField('Email', 'ariefyantobayu.com');
    I.see('Please provide valid email');
    I.clearField('Email');

    // 3.E
    I.fillField('Email', 'ariefyantobayu@gmail.com');
    I.dontSee('email must between 6 - 60 characters');
    I.dontSee('Please provide valid email');
    I.dontSee('email cannot be empty');
    I.clearField('Email');

    I.fillField('Email', 'bayu.ariefyanto@nexsoft.co.id');
    I.dontSee('email must between 6 - 60 characters');
    I.dontSee('Please provide valid email');
    I.dontSee('email cannot be empty');

    /**
     * 3. Field phone number test
     *  A Less than 5 [Starts with 0/+62 and between 5 - 18 characters]
     *  B Not empty [phone cannot be empty]
     *  C More than 18 [Starts with 0/+62 and between 5 - 18 characters]
     *  D Invalid phone [Starts with 0/+62 and between 5 - 18 characters]
     *  E No error
     */
    // 3.A
    I.fillField('Phone Number', '089');
    I.see('Starts with 0/+62 and between 5 - 18 characters');
    I.clearField('Phone Number');

    // 3.B
    I.fillField('Phone Number', '0');
    I.pressKey('Backspace');
    I.see('phone cannot be empty');

    // 3.C
    I.fillField('Phone Number', '08985934850048394829034820980')
    I.see('Starts with 0/+62 and between 5 - 18 characters');
    I.clearField('Phone Number');

    // 3.D
    I.fillField('Phone Number', '12345678910');
    I.see('Starts with 0/+62 and between 5 - 18 characters');
    I.clearField('Phone Number');

    // 3.E
    I.fillField('Phone Number', '08953044309');
    I.dontSee('Starts with 0/+62 and between 5 - 18 characters');
    I.dontSee('phone cannot be empty');
    I.clearField('Phone Number');
    I.fillField('Phone Number', '+628953044309');
    I.dontSee('Starts with 0/+62 and between 5 - 18 characters');
    I.dontSee('phone cannot be empty');
});

Scenario('Add Employee Should Error When Unique Input Error', { timeout: 60 }, async ({ I }) => {

    I.login('admin_cantn', 'Admincantn12345');
    I.waitForText('Dashboard');
    I.see('Employee');
    I.amOnPage('/employee');
    I.click('Add');
    I.fillField('Name', 'Bayu Seno Ariefyanto');
    I.fillField('Username', 'bayujuara01');
    I.fillField('Email', 'ariefyantobayu@gmail.com');
    I.fillField('Phone Number', '089530470916');
    I.click('Submit');
    I.see('Please select one of gender radio button');
    I.click('Male');
    I.dontSee('Please select one of gender radio button');
    I.pressKey('Tab');
    I.pressKey('Tab');
    I.pressKey('ArrowDown');
    I.pressKey('Enter');
    I.see('Director');
    I.pressKey('Tab');
    I.pressKey('ArrowDown');
    I.pressKey('Enter');
    I.see('ADMIN');
    await I.mockRoute('**/users/**', route => route.fulfill({
        status: 200,
        contentType: 'application/json',
        body: JSON.stringify({
            status: 200,
            message: "-",
            data: {
                phone: true,
                email: true,
                username: true
            }
        })
    }));
    I.click('Submit');
    I.wait(2);
    I.see('Inputs it is not correct');
    I.see('phone already in use, take another');
    I.see('username already in use, take another');
    I.see('email already in use, take another');
    I.stopMockingRoute('**/users/**');

    I.clearField('Phone');
    I.fillField('Phone', '089520470917');
    await I.mockRoute('**/users/**', route => route.fulfill({
        status: 200,
        contentType: 'application/json',
        body: JSON.stringify({
            status: 200,
            message: "-",
            data: {
                phone: false,
                email: true,
                username: true
            }
        })
    }));
    I.click('Submit');
    I.wait(2);
    I.see('Inputs it is not correct');
    I.dontSee('phone already in use, take another');
    I.see('username already in use, take another');
    I.see('email already in use, take another');
    I.stopMockingRoute('**/users/**');

    I.clearField('Username');
    I.fillField('Username', 'bayujuara02');
    await I.mockRoute('**/users/**', route => route.fulfill({
        status: 200,
        contentType: 'application/json',
        body: JSON.stringify({
            status: 200,
            message: "-",
            data: {
                phone: false,
                email: true,
                username: false
            }
        })
    }));
    I.click('Submit');
    I.wait(2);
    I.see('Inputs it is not correct');
    I.dontSee('phone already in use, take another');
    I.dontSee('username already in use, take another');
    I.see('email already in use, take another');
    I.stopMockingRoute('**/users/**');

    I.clearField('Email');
    I.fillField('Email', 'bayu.ariefyanto@nexsoft.co.id');
    await I.mockRoute('**/users/**', route => route.fulfill({
        status: 200,
        contentType: 'application/json',
        body: JSON.stringify({
            status: 200,
            message: "-",
            data: {
                phone: false,
                email: false,
                username: false
            }
        })
    }));
    await I.mockRoute('**/users', route => route.fulfill({
        status: 201,
        contentType: 'application/json',
        body: JSON.stringify({
            status: 200,
            message: "-",
            data: {
                id: 15,
                name: "Bayu Seno Ariefyanto",
                username: "bayujuara01",
                email: "bayu.ariefyanto@nexsoft.co.id",
                phoneNumber: "089530470916",
                gender: "MALE",
                status: false,
                activationToken: null,
                imageUrl: "",
                position: {
                    id: 1,
                    name: "Director",
                    createAt: 1646074083461,
                    updateAt: null,
                    deleteAt: null
                },
                role: {
                    id: 1,
                    name: "ADMIN"
                },
                alergicUserList: [],
                createAt: 1646644600972,
                updateAt: null,
                deleteAt: null
            }
        })
    }));
    I.click('Submit');
    I.wait(2);
    I.dontSee('Inputs it is not correct');
});
