/* eslint-disable no-undef */
Feature('Login');

Scenario('When Login Should Error Because Wrong Password', ({ I }) => {
    I.login('bayujuara01', 'Bayujuara01');
    I.waitForText('Username or password are wrong', 3);
});

Scenario('When Login Success Then Logout', ({ I }) => {
    I.login('admin_cantn', 'Admincantn12345');
    I.waitForText('Dashboard', 3);
    I.see('Dashboard');
});
