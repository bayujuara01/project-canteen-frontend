/* eslint-disable no-undef */
// in this file you can append custom step methods to 'I' object

module.exports = function () {
  return actor({

    // Define custom steps here, use 'this' to access default methods of I.
    // It is recommended to place a general 'login' function here.
    login: function (username, password) {
      this.amOnPage('/');
      this.see('Welcome');
      this.fillField('Username', username);
      this.fillField('Password', password);
      this.click('Sign in');

    }

  });
}
